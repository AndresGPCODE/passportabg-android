package com.softernium.passportABG;

import android.os.Build;
import android.os.Bundle;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.softernium.passportABG.utils.NukeSSLCerts;

public class webPolizaContratos extends AppCompatActivity {
    private WebView web;
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new NukeSSLCerts().nuke(this);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_web_poliza_contratos);
        Bundle datos = this.getIntent().getExtras();
        getSupportActionBar().setTitle(datos.getString("TipoDoc", "Poliza y contrato"));
        web = (WebView)findViewById(R.id.web);
        WebSettings settings = web.getSettings();
        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        settings.setDomStorageEnabled(true);
        settings.setJavaScriptEnabled(true);
        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        settings.setBuiltInZoomControls(true);
//        web.setWebViewClient(new WebViewClient() {
//            @Override
//            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error){
//                switch (error.getPrimaryError()) {
//                    case SslError.SSL_UNTRUSTED:
//                        Log.d("","SslError : The certificate authority is not trusted.");
//                        break;
//                    case SslError.SSL_EXPIRED:
//                        Log.d("","SslError : The certificate has expired.");
//                        break;
//                    case SslError.SSL_IDMISMATCH:
//                        Log.d("","The certificate Hostname mismatch.");
//                        break;
//                    case SslError.SSL_NOTYETVALID:
//                        Log.d("","The certificate is not yet valid.");
//                        break;
//                }
//                handler.proceed();
//            }
//        });
        web.setWebViewClient(new WebViewClient());
        String ruta = datos.getString("url", "http://170.80.28.214/abg/abgsite");
        //Log.i("RUTA",ruta);
        web.loadUrl("https://docs.google.com/gview?embedded=true&url=" + ruta);

    }
}
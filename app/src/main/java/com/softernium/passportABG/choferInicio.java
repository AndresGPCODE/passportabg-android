package com.softernium.passportABG;


import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.softernium.passportABG.utils.NukeSSLCerts;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class choferInicio extends Fragment {

    private ImageView fotoCliente;
    private TextView nombreCliente;
    private TextView lugarEntrega;
    private TextView fechaEntrega;
    private TextView peticionesEntrega;


    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private ConstantesGlobales util = new ConstantesGlobales();

    public choferInicio() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_chofer_inicio, container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        new NukeSSLCerts().nuke(this.getContext());

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fotoCliente = getActivity().findViewById(R.id.fotoCliente);
        //fotoCliente.setImageResource(R.drawable.smokin);
        nombreCliente = getActivity().findViewById(R.id.nombreClienteText);
        lugarEntrega = getActivity().findViewById(R.id.direccionClienteText);
        fechaEntrega = getActivity().findViewById(R.id.fechaEntregaText);
        peticionesEntrega = getActivity().findViewById(R.id.peticionesText);
        pref = choferInicio.this.getActivity().getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        editor = pref.edit();
        datosCliente();
    }

    @Override
    public void onStart() {
        super.onStart();
        //Resumen de la vista de inicio
        datosCliente();
        //Toast.makeText(choferInicio.this.getContext(),"inicio",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResume() {
        super.onResume();
        datosCliente();
    }

    public void datosCliente(){
        // Peticion Post
        final ConstantesGlobales direcciones = new ConstantesGlobales();
        RequestQueue peticion = Volley.newRequestQueue(choferInicio.this.getContext());
        String url = direcciones.getApi() +""+ direcciones.getCho();
        final String data = "{"+
                " \"idChofer\": \""+pref.getInt("nIDCliente", 0)+"\", " +
                " \"option\": 1" +
                "}";
        final Map<String, String>[] datos = new Map[]{new HashMap<String, String>()};
        StringRequest stringRegquest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //timeOut(1000);
                try {
                    JSONObject json = new JSONObject( response );
                      //alertDialog("Respuesta","Laitud: "+json.getDouble("Latitud")+", Longitud: "+json.getDouble("Longitud"),false);
                      if(json.getBoolean("status")){
                          String[] fecha = json.getString("FechaHora").split("T");
                          String dateString = "03/26/2012 11:49:00 AM";
                          dateString = fecha[0]+ " " + fecha[1];
                          SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                          Date convertedDate = new Date();

                          try {
                              convertedDate = dateFormat.parse(dateString);
                          } catch (ParseException e) {
                              // TODO Auto-generated catch block
                              e.printStackTrace();
                          }

                          System.out.println(convertedDate);
                          nombreCliente.setText(json.getString("Nombre"));
                          peticionesEntrega.setText(json.getString("Notas"));
                          lugarEntrega.setText(json.getString("CalleSol")+", "+json.getString("NoExteriorSol"));
                          String[] fechaEntregaArr = json.getString("FechaHora").split("T");
                          String[] fechaSeparada = fechaEntregaArr[0].split("-");
                          String[] horaSeparada = fechaEntregaArr[1].split(":");
                          String fechaEstruncturada = fechaSeparada[2]+"-"+fechaSeparada[1]+"-"+fechaSeparada[0];
                          String ampm;
                          //alertDialog("Hora pasada",""+ horaSeparada[0],false);
                          if(Integer.parseInt(horaSeparada[0]) > 12){
                              ampm = " P.M.";
                              //horaSeparada[0] = ""+ (Integer.parseInt(horaSeparada[0]) - 12);
                          }else{
                              ampm = " A.M.";
                          }
                          String horaEstruncturada = "";
                          fechaEntrega.setText(fechaEstruncturada + " " + hourFormatter12(Integer.parseInt(horaSeparada[0]),Integer.parseInt(horaSeparada[1])));

                          if("".equals(json.getString("Foto")) || " ".equals(json.getString("Foto")) || "null".equals(json.getString("Foto")) || "NULL".equals(json.getString("Foto"))){
                              fotoCliente.setImageResource(R.drawable.abg_drive_1);
                          } else {
                              String[] img = json.getString("Foto").split(",");
                              fotoCliente.setImageBitmap(util.stringtoImage(img[1]));
                          }

                          editor.putString("imgFotoCliente",json.getString("Foto"));
                          editor.putString("direccionEntrega",json.getString("CalleSol")+""+json.getString("NoExteriorSol"));
                          editor.putString("nombreCliente",json.getString("Nombre"));
                          editor.putString("idVehiculoEntrega",json.getString("nIDAutomovil"));
                          editor.putString("nIDClienteEntrega",json.getString("nIDCliente"));
                          editor.putString("latitudEntrega", ""+json.getDouble("Latitud"));
                          editor.putString("longitudEntrega", ""+json.getDouble("Longitud"));
                          editor.putInt("nIDSolicitud", json.getInt("nIDSolicitud"));
                          editor.putString("celularCliente",json.getString("Celular"));
                          editor.putString("Estatus",json.getString("Estatus"));
                      }else{
                          nombreCliente.setText("Cliente");
                          peticionesEntrega.setText("No tienes entregas por realizar.");
                          lugarEntrega.setText("");
                          fechaEntrega.setText("");
                          fotoCliente.setImageResource(R.drawable.abg_portada);
                          editor.putString("imgFotoCliente","");
                          editor.putString("direccionEntrega","");
                          editor.putString("nombreCliente","");
                          editor.putString("idVehiculoEntrega","");
                          editor.putString("nIDClienteEntrega","");
                          editor.putString("latitudEntrega", "0.0");
                          editor.putString("longitudEntrega", "0.0");
                          editor.putInt("nIDSolicitud", 0);
                          editor.putString("celularCliente","0");
                          editor.putString("Estatus","NA");
                      }
                      editor.apply();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Error de respuesta","Error");
                error.printStackTrace();
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        int socketTimeout = 8000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRegquest.setRetryPolicy(policy);
        peticion.add(stringRegquest);
    }

    public String hourFormatter12(int hora, int min){
        //alertDialog("Hora pasada","hora: "+hora+" - "+min,false);
        String fecha = "", minuto = "";
        //Calculo del min
        if(min < 10){
            minuto = "0"+min;
        } else {
            minuto = ""+min;
        }
        //Calculo de la hora
        if(hora < 12){
            if(hora < 10){
                fecha = "0"+hora+":"+minuto+":00 A.M.";
            }else{
                fecha = ""+hora+":"+minuto+":00 A.M.";
            }
        } else if(hora == 12){
            fecha = ""+hora+":"+minuto+":00 P.M.";
        } else if(hora > 12){
            hora = hora - 12;
            if(hora < 10){
                fecha = "0"+hora+":"+minuto+":00 P.M.";
            }else{
                fecha = ""+hora+":"+minuto+":00 P.M.";
            }
        }
        return fecha;
    }
    private void alertDialog(String titulo, String msg, boolean dismiss) {
        AlertDialog.Builder builder = new AlertDialog.Builder(choferInicio.this.getContext());
        builder.setTitle(titulo);
        builder.setMessage(msg);
        builder.setPositiveButton("Aceptar", null);
        if ( dismiss){
            builder.setNegativeButton("Cancelar", null);
        }
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}

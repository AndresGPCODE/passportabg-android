package com.softernium.passportABG;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.softernium.passportABG.utils.NukeSSLCerts;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;

public class detalleVehiculoList extends AppCompatActivity {

    private Button agendarBtn;
    private TextView modelo;
    private TextView marca;
    private TextView colorTxtView;
    private TextView puertas;
    private AsyncTask<String, Void, Bitmap> imagenDetalle;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private JSONArray ImagenesAutomovil;
    private ImageSwitcher ImgSlider;
    private int[] galeria = { R.drawable.abg_drive_1, R.drawable.abg_portada, R.drawable.audi_portada };
    private int posicion;
    private static final int DURACION = 3000;
    private Timer timer = null;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new NukeSSLCerts().nuke(this);

        setContentView(R.layout.activity_detalle_vehiculo_list);
        getSupportActionBar().setTitle("Detalles del Vehículo");
        pref = detalleVehiculoList.this.getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        editor = pref.edit();
        Bundle datos = this.getIntent().getExtras();
        String modeloText = datos.getString("modeloTextPass", "De fabrica");
        final String idVehiculo = datos.getString("idTextPass", "0");
        String marcaText = datos.getString("marcaTextPass", "De fabrica");
        String imagen = pref.getString("ImagenDetalleList","a,a");
        String colorText = datos.getString("colorTextPass", "De fabrica");
        String noPuertasText = datos.getString("noPuertasTextPass", "Estandar");
        final String[] img = imagen.split(",");

        final ConstantesGlobales util = new ConstantesGlobales();
        ImgSlider = findViewById(R.id.simpleImageSwitcher);
        ImgSlider.setFactory(new ViewSwitcher.ViewFactory() {
            public View makeView() {
            // TODO Auto-generated method stub
            // Create a new ImageView and set it's properties
                NetworkImageView imageView = new NetworkImageView(getApplicationContext());
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

                return imageView;
            }
        });

        Animation fadeIn = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        Animation fadeOut = AnimationUtils.loadAnimation(this, R.anim.fade_out);
        ImgSlider.setInAnimation(fadeIn);
        ImgSlider.setOutAnimation(fadeOut);

//        timer = new Timer();
//        timer.scheduleAtFixedRate(new TimerTask(){
//            public void run(){
//                runOnUiThread(new Runnable(){
//                    public void run(){
//                        if(ImagenesAutomovil != null && ImagenesAutomovil.length() > 0){
//                            try {
//                                posicion++;
//                                if (posicion == ImagenesAutomovil.length()){
//                                    posicion = 0;
//                                }
//                                imagenDetalle = new DownloadImageTask((ImageView) findViewById(R.id.imageVehiculoListDetalle))
//                                        .execute(util.getUrlImagenesAutomoviles() + ImagenesAutomovil.getJSONObject(posicion).getString("Imagen"));
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    }
//                });
//            }
//        }, 0, DURACION);


        agendarBtn = findViewById(R.id.solicitarBtn);
        modelo = findViewById(R.id.modeloVehiculoDetalleList);
        marca = findViewById(R.id.marcaVehiculoDetalleList);
        colorTxtView = findViewById(R.id.colorVehiculoDetalleList);
        puertas = findViewById(R.id.puertasVehiculoDetalleList);

        findViewById(R.id.imageVehiculoListDetalle).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(ImagenesAutomovil != null && ImagenesAutomovil.length() > 0){
                    try {
                        posicion++;
                        if (ImagenesAutomovil.length() == 1) {
                            Toast.makeText(detalleVehiculoList.this, "Solo hay una imagen disponible.", Toast.LENGTH_SHORT).show();
                        }
                        if (posicion == ImagenesAutomovil.length()){
                            posicion = 0;
                        }
                        imagenDetalle = new DownloadImageTask((ImageView) findViewById(R.id.imageVehiculoListDetalle))
                                .execute(util.getUrlImagenesAutomoviles() + ImagenesAutomovil.getJSONObject(posicion).getString("Imagen"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else{
                    Toast.makeText(detalleVehiculoList.this, "No hay imagenes disponibles.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        imagenDetalle = new DownloadImageTask((ImageView) findViewById(R.id.imageVehiculoListDetalle))
                .execute("https://www.abgpassport.com/Imagenes/Automoviles/NOIMAGE.png");


        modelo.setText(modeloText);
        marca.setText(marcaText);
        colorTxtView.setText(colorText);
        puertas.setText(noPuertasText);



        agendarBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            if((Boolean) pref.getBoolean("statusCambio",false)){
                alertDialog();
            }
            else{
                Intent intent = new Intent(detalleVehiculoList.this, solicitud.class);
                intent.putExtra("idVehiculo", idVehiculo);
                editor.putString("idVehiculoParaSolicitud", idVehiculo);
                editor.putString("imagenVehiculoCambio", img[1]);
                editor.putString("FormatterFechaPosibleCambio","DD/MM/YYYY");
                editor.putString("FormatterFechaPosibleCambio","00:00:00");
                editor.apply();
                startActivity(intent);
            }
            }
        });
        this.obtenerImagenesAutomovil(idVehiculo);
    }

    private void alertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(detalleVehiculoList.this);
        builder.setTitle("ADVERTENCIA!!");
        builder.setMessage("Por políticas solo puedes tener un vehículo reservado, si deseas otro debe cancelar el anterior.");
        builder.setPositiveButton("Entendido", null);
        builder.setNegativeButton("Cancelar cambio", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                confirmarCancelacion();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void confirmarCancelacion() {
        AlertDialog.Builder builder = new AlertDialog.Builder(detalleVehiculoList.this);
        builder.setTitle("PRECAUCION!!");
        builder.setMessage("Estas seguro que desea cancelar su reserva anterior?");
        builder.setPositiveButton("Si, lo estoy.", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                cancelarSolicitud();
            }
        });
        builder.setNegativeButton("No, cerrar", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void cancelarSolicitud(){

        // Peticion Post
        final ConstantesGlobales direcciones = new ConstantesGlobales();
        final RequestQueue peticion = Volley.newRequestQueue(detalleVehiculoList.this);
        String url = direcciones.getApi() +""+ direcciones.getCam();
        final String data = "{"+
                " \"lastIdSolicitud\": \""+ pref.getInt("lastIdCambio", 0) + "\", " +
                " \"option\": 3" +
                "}";
        //Toast.makeText(this,data,Toast.LENGTH_LONG).show();
        final Map<String, String>[] datos = new Map[]{new HashMap<String, String>()};
        StringRequest stringRegquest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    //alertDialog("",response.toString(),false);
                    JSONArray json = new JSONArray( response );
                    if(json.getJSONObject(0).getBoolean("status")){
                        editor.putString("FechaHora",""+" ");
                        editor.putString("HoraPosibleCambio",""+"");
                        editor.putString("Notas",""+"");
                        editor.putString("ubicacionCambio","");
                        editor.putBoolean("statusCambio",false);
                        editor.putInt("lastIdCambio",0);
                        editor.apply();
                        Toast.makeText(detalleVehiculoList.this, "Solicitud realizada, ya puedes agendar.", Toast.LENGTH_LONG).show();
                        //Intent intent = new Intent(detalleVehiculoList.this, MainActivity.class);
                        //startActivity(intent);
                    } else {
                        //alertDialog("Alerta!","Problemas al eliminar la solicitud la solicitud",false);
                        Toast.makeText(detalleVehiculoList.this, "Problemas al eliminar la solicitud la solicitud, intentalo nuevamente", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Error de respuesta","Error");
                error.printStackTrace();
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        peticion.add(stringRegquest);
    }

    public void obtenerImagenesAutomovil(String nIDAutomovil){

        // Peticion Post
        final ConstantesGlobales direcciones = new ConstantesGlobales();
        final RequestQueue peticion = Volley.newRequestQueue(detalleVehiculoList.this);
        String url = direcciones.getApi() +""+ direcciones.getCam();
        final String data = "{"+
                " \"nIDAutomovil\": \""+ nIDAutomovil + "\", " +
                " \"option\": 5" +
                "}";
        final Map<String, String>[] datos = new Map[]{new HashMap<String, String>()};
        StringRequest stringRegquest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray json = new JSONArray( response );
                    if(json.getJSONObject(0).getBoolean("status")){
//                        for (int i = 0; i < json.length(); i++){}
                        ImagenesAutomovil = json;
                        imagenDetalle = new DownloadImageTask((ImageView) findViewById(R.id.imageVehiculoListDetalle))
                                .execute(direcciones.getUrlImagenesAutomoviles() + ImagenesAutomovil.getJSONObject(0).getString("Imagen"));
                    } else {
                        //alertDialog("Alerta!","Problemas al eliminar la solicitud la solicitud",false);
                        Log.i("ListaImagenes", "Sin resultados" );
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Error de respuesta","Error");
                error.printStackTrace();
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        peticion.add(stringRegquest);
    }

}// fin de la clase

//public class MyImageSwitcher extends ImageSwitcher {
//
//    public MyImageSwitcher(Context context) {
//        super(context);
//    }
//
//    public MyImageSwitcher(Context context, AttributeSet attrs) {
//        super(context, attrs);
//    }
//
//    public void setImageUrl(String url) {
//        NetworkImageView image = (NetworkImageView) this.getNextView();
//        image.setImageUrl(url, AppController.getInstance().getImageLoader());
//        showNext();
//    }
//}

class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

    ImageView bmImage;

    public DownloadImageTask(ImageView bmImage) {
        this.bmImage = bmImage;

    }

    protected Bitmap doInBackground(String... urls) {
        String urldisplay = urls[0];
        Bitmap mIcon11 = null;

        try {
            InputStream in = new java.net.URL(urldisplay).openStream();

            mIcon11 = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
        return mIcon11;
    }

    protected void onPostExecute(Bitmap result) {
        bmImage.setImageBitmap(result);
    }

}

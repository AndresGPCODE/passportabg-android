package com.softernium.passportABG;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.Locale;

public class detallesCuenta extends BaseAdapter {

        private Context context;
        private int layout;
        private JSONArray list;

        public detallesCuenta(Context context, int layout, JSONArray list){
                this.context = context;
                this.layout = layout;
                this.list = list;
        }

        @Override
        public int getCount() {
                return list.length();
        }

        @Override
        public Object getItem(int i) {
                try {
                        return this.list.getJSONObject(i);
                } catch (JSONException e) {
                        e.printStackTrace();
                        return null;
                }
        }

        @Override
        public long getItemId(int i) {
                return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
                View v = view;
                LayoutInflater inflate = LayoutInflater.from(this.context);
                v = inflate.inflate(R.layout.list_cuenta, null, false);

                JSONObject currentItem = (JSONObject) getItem(i);

                TextView item = (TextView) v.findViewById(R.id.itemCuenta);
                TextView itemValor = (TextView) v.findViewById(R.id.itemValor);

                ConstantesGlobales util = new ConstantesGlobales();
                try {
                        Double number = currentItem.getDouble("Costo");
                        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.CANADA);
                        String currency = format.format(number);
                        item.setText(currentItem.getString("Detalle"));
                        itemValor.setText(""+currency);
                } catch (JSONException e) {
                        e.printStackTrace();
                }
                return v;
        }
}

package com.softernium.passportABG;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;

public class ConstantesGlobales {

    final private String Api = "https://www.abgpassport.com/APIMovil/";
    final private String url = "https://www.abgpassport.com/API/";
    final private String urlImagenesAutomoviles = "https://www.abgpassport.com/Imagenes/Automoviles/";
    final private String urlImagenesUsuarios    = "https://www.abgpassport.com/API/";
    final private String urlDocumentos    = "https://www.abgpassport.com/documentos/";

    //final private String Api = "http://crm.coeficiente.mx/crm/APIMovil/";
    //final private String url = "http://crm.coeficiente.mx/crm/API/";
    /// Direccion de notificaciones
    final private String Not = "Notificaciones.php";
    /// Direccion de Login
    final private String Logi = "login.php";
    /// Direccion de Lista de vehiculos
    final private String Veh = "ListaVehiculos.php";
    /// Direccion de agendar cambio de vehiculo
    final private String Cam = "AgendarCambio.php";
    /// Direccion de chofer
    final private String Cho = "Chofer.php";
    /// Direccion de peticiones para la seccion de configuracion
    final private String EdoCu = "EstadoCuenta.php";
    /// Direccion de peticiones para reportes de incidencias o sinestros
    final private String Rep = "ReportesYValoracion.php";
    /// Direccion de peticiones para reportes de incidencias o sinestros
    final private String Ubi = "Ubicaciones.php";
    /// Direccion de peticiones para reportes de incidencias o sinestros
    final private String Chat = "Chat.php";
    /// Direccion de peticiones para reportes de incidencias o sinestros
    final private String Comm = "comandos.php";
    /// Direccion de peticiones para reportes de incidencias o sinestros
    final private String Contr = "Controladores/";
    /// Direccion de peticiones para reportes de incidencias o sinestros
    final private String metri = "metricasMensuales.php";
    /// Direccion de peticiones para reportes de incidencias o sinestros
    final private String usr = "usuario.php";
    /// Direccion de peticiones para reportes de incidencias o sinestros
    final private String doc = "Documentos.php";

    public ConstantesGlobales(){

    }
    public String getApi(){
        return Api;
    }
    public String getNot(){
        return Not;
    }
    public String getLogi(){
        return Logi;
    }
    public String getCam(){
        return Cam;
    }
    public String getVeh(){
        return Veh;
    }
    public String getCho(){
        return Cho;
    }
    public String getEdoCu(){
        return EdoCu;
    }
    public String getRep(){
        return Rep;
    }
    public String getUbi(){
        return Ubi;
    }
    public String getChat(){
        return Chat;
    }
    public String getComm(){
        return Comm;
    }
    public String getContr() {
        return Contr;
    }
    public String getMetri() {
        return metri;
    }
    public String getUsr() {
        return usr;
    }
    public String getUrl() {
        return url;
    }
    public String getDoc() {
        return doc;
    }
    public String getUrlImagenesAutomoviles(){return urlImagenesAutomoviles;}
    public String getUrlDocumentos(){return urlDocumentos;}

    public Bitmap stringtoImage(String base64Image){
        byte[] decodedString = Base64.decode(base64Image, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;
    }

    public Bitmap stringtoScaleImage(String base64Image, int height, int width){
        //int alto = 246, ancho = 400;
        byte[] decodedString = Base64.decode(base64Image, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        int h = decodedByte.getHeight();
        int w = decodedByte.getWidth();
        Log.i("dimensiones","altura: "+ h + ", Ancho: " + w);
        int hdiv = 5, wdiv = 5;
        if (h >= 4000 || w >= 4000){
            Log.i("dimensiones"," DIM: 4000 ");
            hdiv = 5;
            wdiv = 5;
        } else if (h >= 3000 || w >= 3000){
            Log.i("dimensiones"," DIM: 3000 ");
            hdiv = 4;
            wdiv = 4;
        } else if (h >= 2000 || w >= 2000){
            Log.i("dimensiones"," DIM: 2000 ");
            hdiv = 3;
            wdiv = 3;
        } else if (h >= 1000 || w >= 1000){
            Log.i("dimensiones"," DIM: 1000 ");
            hdiv = 2;
            wdiv = 2;
        } else if ((h < 1000 && h > 700) || (w < 1000 && w > 700)){
            Log.i("dimensiones"," DIM: 1000 - 700 ");
            hdiv = 1;
            wdiv = 1;
        } else {
            Log.i("dimensiones"," DIM: 699 ");
            hdiv = 1;
            wdiv = 1;
        }
        h = Math.round(h / hdiv);
        w = Math.round(w / wdiv);
        decodedByte = Bitmap.createScaledBitmap(decodedByte, w, h, false);
        return decodedByte;
    }

    public Bitmap stringtoScaleImageByPercent(String base64Image, int percent){
        //int alto = 246, ancho = 400;
        byte[] decodedString = Base64.decode(base64Image, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        int h = decodedByte.getHeight();
        int w = decodedByte.getWidth();
        h = Math.round((h / 100) * percent);
        w = Math.round((w / 100) * percent);
        //Log.i("dimensiones 2","altura: "+ h + ", Ancho: " + w);
        decodedByte = Bitmap.createScaledBitmap(decodedByte, w, h, false);
        return decodedByte;
    }

    public String imageToString(ImageView View){
        ImageView v = View;
        Bitmap bitmap = ((BitmapDrawable) v.getDrawable()).getBitmap();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 20, outputStream);
        String s = Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
        return s;
    }

    public String imageToStringQuality(ImageView View, int quality, Bitmap.CompressFormat format){
        ImageView v = View;
        Bitmap bitmap = ((BitmapDrawable) v.getDrawable()).getBitmap();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(format, quality, outputStream);
        String s = Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
        return s;
    }

}

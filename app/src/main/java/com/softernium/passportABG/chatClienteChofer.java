package com.softernium.passportABG;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;
import com.softernium.passportABG.utils.NukeSSLCerts;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;

public class chatClienteChofer extends AppCompatActivity {

    private ListView tabla;
    private JSONArray chats = new JSONArray();
    private ImageButton send, call;
    private TextInputEditText txt;

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private int time = 1000 * 6;
    private Boolean control = true;
    private Timer interval;
    private static final int REQUEST_PHONE_CALL = 1;
    private String cel = "";

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new NukeSSLCerts().nuke(this);

        setContentView(R.layout.activity_chat_cliente_chofer);
        getSupportActionBar().setTitle("Chat con chofer");
        pref = chatClienteChofer.this.getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        editor = pref.edit();
        control = true;
        tabla = findViewById(R.id.tablaChatClienteChofer);
        txt = findViewById(R.id.msgClienteChofer);
        send = findViewById(R.id.sendMsgClienteChofer);
        call = findViewById(R.id.callChofer);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(chatClienteChofer.this, "", Toast.LENGTH_SHORT).show();
                if(!txt.getText().toString().equals("")) {
                    JSONObject obj = new JSONObject();
                    try {
                        obj.put("isIncoming",false);
                        obj.put("Texto",txt.getText().toString());
                        obj.put("FechaCreacion","Ahora");
                        chats.put(obj);
                        cargarChat();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    sendMsgFunction(txt.getText().toString());
                    txt.setText("");
                }
            }
        });
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callChofer();
            }
        });

    }// fin del onCreate


    public void callChofer(){
        Toast.makeText(this, ""+cel, Toast.LENGTH_SHORT).show();
        if(cel.equals("0")){
            cel = "";
        }
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(chatClienteChofer.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(chatClienteChofer.this, new String[]{Manifest.permission.CALL_PHONE},REQUEST_PHONE_CALL);
            }
            else {
                if (!cel.equals("")){
                    String phoneNo = cel;
                    if (!TextUtils.isEmpty(phoneNo)) {
                        String dial = "tel:" + phoneNo;
                        startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(dial)));
                    }else {
                        Toast.makeText(chatClienteChofer.this, "No hay numero de contacto", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(this, "No hay contacto de chofer.", Toast.LENGTH_SHORT).show();
                }
            }
        }
        else {
            if (!cel.equals("")){
                String phoneNo = cel;
                if (!TextUtils.isEmpty(phoneNo)) {
                    String dial = "tel:" + phoneNo;
                    startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(dial)));
                }else {
                    Toast.makeText(chatClienteChofer.this, "No hay numero de contacto", Toast.LENGTH_SHORT).show();
                }
            }else{
                Toast.makeText(this, "No hay contacto de chofer.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PHONE_CALL: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    callChofer();
                }
                else {

                }
                return;
            }
        }
    }

    public void timeOut(int timeInterval){
        Log.i("Interval","Intervlo establecido");
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        if(control){
                            obtenerChats();
                        }
//                        if ("".equals(txt.getText().toString()) && control) {
//                        }else{
//                        }
                    }
                }, timeInterval);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Detener al intervalo para la peticion de los chats
        //interval.cancel();
        control = false;
    }

    @Override
    protected void onStop() {
        super.onStop();
        control = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        control = false;
    }

    @Override
    protected void onStart() {
        super.onStart();
        control = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        obtenerChoferChat();
        control = true;
    }

    private void obtenerChoferChat() {
        // Peticion Post
        final ConstantesGlobales direcciones = new ConstantesGlobales();
        RequestQueue peticion = Volley.newRequestQueue(chatClienteChofer.this);
        String url = direcciones.getApi() +""+ direcciones.getLogi();

        final String data = "{"+
                " \"nIDCliente\": \""+ pref.getInt("nIDCliente", 0) + "\", " +
                " \"option\": 2" +
                "}";
        final Map<String, String>[] datos = new Map[]{new HashMap<String, String>()};

        StringRequest stringRegquest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //timeOut(1000);
                try {
                    JSONObject json = new JSONObject( response );
                    Log.i("RESPONSE:",""+json);
                    editor.putInt("idChofer",json.getInt("nIDChofer"));
                    editor.apply();
                    //Toast.makeText(chatClienteChofer.this, ""+json, Toast.LENGTH_SHORT).show();
                    cel = json.getString("Celular");
                    obtenerChats();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Error de respuesta","Error");
                error.printStackTrace();
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        peticion.add(stringRegquest);
    }

    private void sendMsgFunction(String msg) {
        // Peticion Post
        final ConstantesGlobales direcciones = new ConstantesGlobales();
        RequestQueue peticion = Volley.newRequestQueue(chatClienteChofer.this);
        String url = direcciones.getApi() +""+ direcciones.getChat();

        final String data = "{"+
                " \"nIDCliente\": \""+ pref.getInt("nIDCliente", 0) + "\", " +
                " \"nIDUsuario\": 0, " +
                " \"nIDChofer\": \""+ pref.getInt("idChofer", 0) +  "\", " +
                " \"Remitente\": \"CLIENTE\", " +
                " \"Texto\": \""+ msg + "\", " +
                " \"Estatus\": \"NO LEIDO\", " +
                " \"option\": 2" +
                "}";
        final Map<String, String>[] datos = new Map[]{new HashMap<String, String>()};

        StringRequest stringRegquest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //timeOut(1000);
                try {
                    JSONArray json = new JSONArray( response );
                    Date date = new Date();
                    //Caso 3: obtenerhora y fecha y salida por pantalla con formato:
                    DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
                    Log.i("HoraFormateadaConFecha","Hora y fecha: "+hourdateFormat.format(date));
//                    JSONObject newMsg = new JSONObject();
//                    newMsg.put("Texto","AWEBO");
//                    chats.put(newMsg);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Error de respuesta","Error");
                error.printStackTrace();
            }
        }){

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRegquest.setRetryPolicy(policy);
        peticion.add(stringRegquest);
    }
    private void cargarChat() {
        celdasChatsAdapter adapterNew = new celdasChatsAdapter(chatClienteChofer.this, R.layout.celdas_chat_generales, chats);
        tabla.setAdapter(adapterNew);
        tabla.setSelection(chats.length());
    }

    public void obtenerChats(){
        // Peticion Post
        final ConstantesGlobales direcciones = new ConstantesGlobales();
        RequestQueue peticion = Volley.newRequestQueue(chatClienteChofer.this);
        String url = direcciones.getApi() +""+ direcciones.getChat();

        final String data = "{"+
                " \"nIDCliente\": \""+ pref.getInt("nIDCliente", 0) + "\", " +
                " \"nIDChofer\": \""+ pref.getInt("idChofer", 0) + "\", " +
                " \"nIDUsuario\": \"0\", " +
                " \"Remitente\":\"CLIENTE\", " +
                " \"option\": 1" +
                "}";
        Log.i("datadd",""+ data);
        final Map<String, String>[] datos = new Map[]{new HashMap<String, String>()};

        StringRequest stringRegquest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray json = new JSONArray( response );
                    if(chats.length() != json.length()){
                        chats = json;
                        cargarChat();
                        leerTodo();
                    }
                    if(control){
                        timeOut(4000);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Error de respuesta","Error");
                error.printStackTrace();
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        peticion.add(stringRegquest);
    }
    public void leerTodo(){
        ArrayList<String> ids = new ArrayList<>();
        for(int i = 0; i < chats.length(); i++){
            try {
                if("NO LEIDO".equals(chats.getJSONObject(i).getString("Estatus"))){
                    ids.add(chats.getJSONObject(i).getString("nIDChat"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } // Fin del for
        Log.i("IDS",ids.toString());
        if(ids.size() > 0){
            String parametros = "";
            for(int i = 0; i < ids.size(); i++){
                if (ids.size()-1 == i) {
                    parametros = parametros +""+ ids.get(i);
                }else{
                    parametros = parametros +""+ ids.get(i)+",";
                }
            }
            // Peticion Post
            final ConstantesGlobales direcciones = new ConstantesGlobales();
            RequestQueue peticion = Volley.newRequestQueue(chatClienteChofer.this);
            String url = direcciones.getApi() +""+ direcciones.getChat();

            final String data = "{"+
                    " \"ids\": \""+ parametros + "\", " +
                    " \"option\": 3" +
                    "}";
            final Map<String, String>[] datos = new Map[]{new HashMap<String, String>()};

            StringRequest stringRegquest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {

                        JSONArray json = new JSONArray( response );


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i("Error de respuesta","Error");
                    error.printStackTrace();
                }
            }){

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return data == null ? null : data.getBytes("utf-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                        return null;
                    }
                }
            };

            peticion.add(stringRegquest);
        } else {
        }
    }// Fin de leer todo



}// fin de la clase

package com.softernium.passportABG;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.softernium.passportABG.utils.NukeSSLCerts;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class configuracionFragmento extends Fragment {

    private ImageButton comandosBtn;
    private ImageButton chatBtn;
    private ImageButton ubicacionesBtn;
    private Button faqBtn;
    private Button logout;
    private Button estadoCuenta;

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragmento_configuraciones,container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new NukeSSLCerts().nuke(this.getContext());
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //comandosBtn = getActivity().findViewById(R.id.comandosConfiguracionBtn);
        //chatBtn = getActivity().findViewById(R.id.chatBtn);
        faqBtn = getActivity().findViewById(R.id.faqBtn);
        logout = getActivity().findViewById(R.id.logOut);
        //comandosBtn.setImageResource(R.drawable.ic_settings);
        estadoCuenta = getActivity().findViewById(R.id.estadoCuentaBtn);
        pref = configuracionFragmento.this.getActivity().getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        editor = pref.edit();
        faqBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(configuracionFragmento.this.getActivity(), Faq.class);
                startActivity(intent);
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editor.putBoolean("status", false);
                editor.apply();
                Intent intent = new Intent(configuracionFragmento.this.getActivity(), login.class);
                startActivity(intent);
            }
        });
//        chatBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(configuracionFragmento.this.getActivity(), chatClienteChofer.class);
//                startActivity(intent);
//            }
//        });
        estadoCuenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(configuracionFragmento.this.getActivity(), estodoCuentaDetalle.class);
                startActivity(intent);
            }
        });

        obtenerEstadoCuenta();

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void obtenerEstadoCuenta(){
        // Peticion Post
        final ConstantesGlobales direcciones = new ConstantesGlobales();
        RequestQueue peticion = Volley.newRequestQueue(configuracionFragmento.this.getActivity());
        String url = direcciones.getApi() +""+ direcciones.getEdoCu();

        final String data = "{"+
                " \"nIDCliente\":\"" + pref.getInt("nIDCliente", 0) +"\", " +
                " \"option\": 1" +
                "}";

        final Map<String, String>[] datos = new Map[]{new HashMap<String, String>()};

        StringRequest stringRegquest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONArray json = new JSONArray( response );
                    if (json.getJSONObject(0).getBoolean("status")){
                        //Log.i("EstadoCuenta",""+json);
                        Double total = 0.0;
                        for (int i = 0; i < json.length(); i++){
                            total = total + json.getJSONObject(i).getDouble("Costo");
                        }
                        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.CANADA);
                        String currency = format.format(total);
                        estadoCuenta.setText(""+currency);
                        if(total == 0){
                            estadoCuenta.setEnabled(false);
                        }else{
                            estadoCuenta.setEnabled(true);
                        }
                    }else{
                        estadoCuenta.setText("$ 0.00");
                        estadoCuenta.setEnabled(false);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Error de respuesta","Error");
                error.printStackTrace();
            }
        }){

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        peticion.add(stringRegquest);
    }

}// Fin de la clase


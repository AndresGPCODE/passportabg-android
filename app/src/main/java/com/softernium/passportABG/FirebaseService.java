package com.softernium.passportABG;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.firebase.messaging.FirebaseMessagingService;

import com.google.firebase.messaging.RemoteMessage;

import static androidx.constraintlayout.widget.Constraints.TAG;

//import static com.android.volley.VolleyLog.TAG;

public class FirebaseService extends FirebaseMessagingService {

    private static final String CHANNEL_ID = "NOTIFICACION_FIREBASE";
    private static final int NOTIFICATION_ID = 1;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // ...

        // TODO(developer): Handle FCM messages here.
        //Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            pref = FirebaseService.this.getSharedPreferences("preferencias", Context.MODE_PRIVATE);
            editor = pref.edit();

            //editor.putBoolean("",true);



            if (/* Check if data needs to be processed by long running job */ true) {
                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
                //scheduleJob();
            } else {
                // Handle message within 10 seconds
                //handleNow();
            }

        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            //Toast.makeText(this, ""+remoteMessage.getNotification().getBody(), Toast.LENGTH_LONG).show();
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.

    }

    @Override
    public void onDeletedMessages() {
        super.onDeletedMessages();

    }

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        Log.i("newToken", s);
        getSharedPreferences("FIREBASE", MODE_PRIVATE).edit().putString("fb", s).apply();
    }

    public static String getToken(Context context) {
        return context.getSharedPreferences("FIREBASE", MODE_PRIVATE).getString("fb", "empty");
    }

  /*  private void createNotificacion( String tittle, String body) {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            CharSequence name = "NOTIFICACION";
            NotificationChannel chanel = new NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManager manager = (NotificationManager) FirebaseService.this.context.getSystemService(Context.NOTIFICATION_SERVICE);
            manager.createNotificationChannel(chanel);
        }
        NotificationCompat.Builder thisNotifi = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.abg_sms)
                .setContentTitle(tittle)
                .setContentText(body)
                //.setColor(Color.rgb(200,16,46))
                .setLights(Color.WHITE, 1000, 1000)
                //.setDefaults(Notification.DEFAULT_SOUND)
                .setVibrate(new long[]{1000,1000,1000,1000,1000,1000,1000,1000})
                .setPriority(NotificationCompat.PRIORITY_HIGH);
        NotificationManagerCompat notificacionManager = NotificationManagerCompat.from(context);
        notificacionManager.notify(NOTIFICATION_ID, thisNotifi.build());
    }*/

}

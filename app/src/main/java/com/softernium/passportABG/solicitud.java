package com.softernium.passportABG;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.icu.text.SimpleDateFormat;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;
import com.softernium.passportABG.utils.NukeSSLCerts;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import dmax.dialog.SpotsDialog;

public class solicitud extends AppCompatActivity {

    private TextView fechaTxt;
    private TextView horaTxt;
    private TextView ubicacionTxt;
    private TextInputEditText peticiones;

    int PERMISSION_ID = 44;

    private ImageButton fechaBtn;
    private ImageButton horaBtn;
    private ImageButton ubicacionBtn;
    private Button guardar;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private String AutoId, fehcaEnviar, horaFormater = "00:00:00", fehcaFormater = "DD/MM/YYYY";
    private Boolean control = false,fechaCorrecta = false, horaCorrecta = false;
    AlertDialog nDialog;
    private String Lat, Lon;

    private int mYear = 0, mMonth = 0, mDay = 0, mHour = 0, mMinute = 0;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new NukeSSLCerts().nuke(this);
        setContentView(R.layout.activity_solicitud);
        getSupportActionBar().setTitle("Datos de la solicitud");
        fechaBtn = findViewById(R.id.fechaSolicitudBtn);
        horaBtn = findViewById(R.id.horaSolicitudBtn);
        ubicacionBtn = findViewById(R.id.ubicacionSolicitudBtn);
        fechaTxt = findViewById(R.id.fechaSolicitudText);
        horaTxt = findViewById(R.id.horaSolicitudText);
        ubicacionTxt = findViewById(R.id.ubicacionSolicitudText);
        peticiones = findViewById(R.id.peticionesText);
        guardar = findViewById(R.id.guardarPeticionBtn);
        guardar.setTextColor(Color.parseColor("#ffffff"));
        pref = getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        editor = pref.edit();

        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validarDisponibilidadVehiculo();
            }
        });


        fechaBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            // Get Current Date
            final Calendar c = Calendar.getInstance();
            Date actual = c.getTime();
            c.setTime(actual);
            c.add(Calendar.DATE, 2);
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
            //Toast.makeText(solicitud.this, ""+mMonth, Toast.LENGTH_SHORT).show();
            DatePickerDialog datePickerDialog = new DatePickerDialog(solicitud.this,
                new DatePickerDialog.OnDateSetListener() {

                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        String mm;
                        String dd;
                        if((monthOfYear + 1) < 10){
                            mm = "0" + (monthOfYear + 1);
                        } else {
                            mm = "" + (monthOfYear + 1);
                        }
                        if(dayOfMonth < 10){
                            dd = "0" + dayOfMonth;
                        } else {
                            dd = "" + dayOfMonth;
                        }
                        fechaTxt.setText(dd + "-" + mm + "-" + year);
                        fechaTxt.setTextColor(Color.parseColor("#000000"));
                        fehcaFormater = year + "-" + mm + "-" + dd;
                        //alertDialog("hora",horaFormater,false);
                        if(!validarFecha()){
                            if(!fechaCorrecta){
                                fehcaFormater = "DD/MM/YYYY";
                            }
                        }
                    }
                }, mYear, mMonth, mDay);
            datePickerDialog.show();
            datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
            }
        });

        horaBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            // Get Current Time
            final Calendar c = Calendar.getInstance();
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);
            // Launch Time Picker Dialog
            //Toast.makeText(solicitud.this, "Hora: "+mHour+" Minuto: "+mMinute, Toast.LENGTH_SHORT).show();
            TimePickerDialog timePickerDialog = new TimePickerDialog(solicitud.this,
                new TimePickerDialog.OnTimeSetListener() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        String min,hora;
                        if(minute < 10){
                            min = "0"+minute;
                        } else{
                            min = ""+minute;
                        }
                        if(hourOfDay < 10){
                            hora = "0"+hourOfDay;
                        } else {
                            hora = ""+hourOfDay;
                        }
                        horaFormater = hora+":"+min+":00";
                        //Log.i("HoraFormatter",""+hourFormatter12(hourOfDay, minute));
                        if(hourOfDay > 11){
                            horaTxt.setText((hourOfDay - 12) + ":" + min + ":00 P.M.");
                        } else {
                            horaTxt.setText(hourOfDay + ":" + min +":00 A.M.");
                        }
                        horaTxt.setText(hourFormatter12(hourOfDay, minute));
                        horaTxt.setTextColor(Color.parseColor("#000000"));
                        //alertDialog("fecha",fehcaFormater,false);
                        //Toast.makeText(solicitud.this, ""+fehcaFormater, Toast.LENGTH_SHORT).show();

                        // Validar la hora seleccionada dentro de horas laborales
                        if(hourOfDay < 9 || hourOfDay > 19 || (hourOfDay == 19 && minute > 0)){
                            horaTxt.setTextColor(Color.parseColor("#c8102e"));
                            alertDialog("Fuera de horario de servicio","La hora de entrega debe respetar el horario de trabajo de 9:00 A.M. a 7:00 P.M.",false);
                            horaFormater = "00:00:00";
                        } else if(!validarHora()){
                            if(!horaCorrecta){
                                horaFormater = "00:00:00";
                            }
                        }

                    }
                }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });
        ubicacionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (checkPermissions()) {
                    if (isLocationEnabled()) {
                        //Toast.makeText(this.getActivity(), "Permisos Concedidos", Toast.LENGTH_SHORT).show();
                        irMapaSolicitud();
                    } else {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);
                    }
                } else {
                    requestPermissions();
                    //getLastLocation();
                }


            }
        });
    }

    public String hourFormatter12(int hora, int min){
        String fecha = "", minuto = "";
        if(min < 10){
            minuto = "0"+min;
        } else {
            minuto = ""+min;
        }
        if(hora < 12){
            if(hora < 10){
                fecha = "0"+hora+":"+minuto+":00 A.M.";
            }else{
                fecha = ""+hora+":"+minuto+":00 A.M.";
            }
        } else if(hora == 12){
            fecha = ""+hora+":"+minuto+":00 P.M.";
        } else if(hora > 12){
            hora = hora - 12;
            if(hora < 10){
                fecha = "0"+hora+":"+minuto+":00 P.M.";
            }else{
                fecha = ""+hora+":"+minuto+":00 P.M.";
            }
        }
        return fecha;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public Boolean validarTiempoMargen(){
        fechaCorrecta = false;
        horaCorrecta = false;
        if(fehcaFormater == "" || fehcaFormater == null){
            alertDialog("Fecha Invalida","La fecha de entrega debe ser al menos de 48 horas posteriores a la fecha actual.",false);
            fechaTxt.setTextColor(Color.parseColor("#c8102e"));
            return false;
        }
        if(fehcaFormater.trim() == "DD/MM/YYYY"){
            alertDialog("Fecha Invalida","La fecha de entrega debe ser al menos de 48 horas posteriores a la fecha actual.",false);
            fechaTxt.setTextColor(Color.parseColor("#c8102e"));
            return false;
        }

        String[] arrFecha = fehcaFormater.split("-");
        int añoSeleccionado = Integer.parseInt(arrFecha[0]);
        int mesSeleccionado = Integer.parseInt(arrFecha[1]);
        int diaSeleccionado = Integer.parseInt(arrFecha[2]);

        // VALIDAR DIA DE LA SEMANA

        if(this.esFinDeSemana(diaSeleccionado + "/" + mesSeleccionado + "/" + añoSeleccionado)){
            alertDialog("Dia no valido","La fecha de entrega solo puede sen en dias laborales (Lunes a Viernes).",false);
            return false;
        }

        fechaCorrecta = true;

        if(horaFormater == "" || horaFormater == null){
            alertDialog("Hora Invalida","La hora de entrega debe ser al menos de 48 horas posteriores a la fecha actual.",false);
            horaTxt.setTextColor(Color.parseColor("#c8102e"));
            return false;
        }
        if(horaFormater.trim() == "00:00:00"){
            alertDialog("Hora Invalida","La hora de entrega debe ser al menos de 48 horas posteriores a la fecha actual.",false);
            horaTxt.setTextColor(Color.parseColor("#c8102e"));
            return false;
        }
        String[] arrTiempo = horaFormater.split(":");
        int horaSeleccionada = Integer.parseInt(arrTiempo[0]);
        int minutoSeleccionado = Integer.parseInt(arrTiempo[1]);
        if(horaSeleccionada < mHour){
            if(diaSeleccionado <= mDay){
                alertDialog("Hora Invalida","La hora de entrega debe ser al menos de 48 horas posteriores a la fecha actual.",false);
                horaTxt.setTextColor(Color.parseColor("#c8102e"));
                return false;
            }
        }
        fechaTxt.setTextColor(Color.parseColor("#000000"));
        horaTxt.setTextColor(Color.parseColor("#000000"));
        horaCorrecta = true;
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public Boolean validarFecha(){
        fechaCorrecta = false;
        if(fehcaFormater == "" || fehcaFormater == null){
            alertDialog("Fecha Invalida","La fecha de entrega debe ser al menos de 48 horas posteriores a la fecha actual.",false);
            fechaTxt.setTextColor(Color.parseColor("#c8102e"));
            return false;
        }
        if(fehcaFormater.trim() == "DD/MM/YYYY"){
            alertDialog("Fecha Invalida","La fecha de entrega debe ser al menos de 48 horas posteriores a la fecha actual.",false);
            fechaTxt.setTextColor(Color.parseColor("#c8102e"));
            return false;
        }

        String[] arrFecha = fehcaFormater.split("-");
        int añoSeleccionado = Integer.parseInt(arrFecha[0]);
        int mesSeleccionado = Integer.parseInt(arrFecha[1]);
        int diaSeleccionado = Integer.parseInt(arrFecha[2]);

//        if(añoSeleccionado < mYear){
//            alertDialog("Fecha Invalida","La fecha de entrega debe ser al menos de 48 horas posteriores a la fecha actual.",false);
//            fechaTxt.setTextColor(Color.parseColor("#c8102e"));
//            return false;
//        }
//        if(mesSeleccionado < (mMonth + 1)){
//            alertDialog("Fecha Invalida","La fecha de entrega debe ser al menos de 48 horas posteriores a la fecha actual.",false);
//            fechaTxt.setTextColor(Color.parseColor("#c8102e"));
//            return false;
//        }
//        if(diaSeleccionado < mDay){
//            alertDialog("Fecha Invalida","La fecha de entrega debe ser al menos de 48 horas posteriores a la fecha actual.",false);
//            fechaTxt.setTextColor(Color.parseColor("#c8102e"));
//            return false;
//        }
        // VALIDAR DIA DE LA SEMANA

        if(this.esFinDeSemana(diaSeleccionado + "/" + mesSeleccionado + "/" + añoSeleccionado)){
            alertDialog("Dia no valido","La fecha de entrega solo puede sen en dias laborales (Lunes a Viernes).",false);
            fechaTxt.setTextColor(Color.parseColor("#c8102e"));
            return false;
        }

        fechaTxt.setTextColor(Color.parseColor("#000000"));
        fechaCorrecta = true;
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public Boolean esFinDeSemana(String fecha){
        Date date1 = null;
        try {
            //date1 = new SimpleDateFormat("dd/MM/yyyy").parse(diaSeleccionado + "/" + mesSeleccionado + "/" + añoSeleccionado);
            date1 = new SimpleDateFormat("dd/MM/yyyy").parse(fecha);
            Calendar c = Calendar.getInstance();
            c.setTime(date1);
            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
            System.out.println("XXXXXXXX - " + dayOfWeek);
            if (dayOfWeek == 1 || dayOfWeek == 7){
                return true;
            }
            return false;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public Boolean validarHora(){
        horaCorrecta = false;
        String[] arrFecha = fehcaFormater.split("-");
        int añoSeleccionado = Integer.parseInt(arrFecha[0]);
        int mesSeleccionado = Integer.parseInt(arrFecha[1]);
        int diaSeleccionado = Integer.parseInt(arrFecha[2]);

        if(horaFormater == "" || horaFormater == null || horaFormater.trim() == "00:00:00"){
            alertDialog("Hora Invalida","La hora de entrega debe ser al menos de 48 horas posteriores a la fecha actual.",false);
            horaTxt.setTextColor(Color.parseColor("#c8102e"));
            return false;
        }

        String[] arrTiempo = horaFormater.split(":");
        int horaSeleccionada = Integer.parseInt(arrTiempo[0]);
        int minutoSeleccionado = Integer.parseInt(arrTiempo[1]);

        if(horaSeleccionada < mHour){
            if(diaSeleccionado == mDay){
                alertDialog("Hora Invalida","La hora de entrega debe ser al menos de 48 horas posteriores a la fecha actual.",false);
                horaTxt.setTextColor(Color.parseColor("#c8102e"));
                return false;
            }
        }
//        else if(mesSeleccionado > mMonth && (mesSeleccionado - mMonth) == 1){
//            //if(diaSeleccionado <= mDay){}
//            if(horaSeleccionada < mHour){
//                alertDialog("Hora Invalida","La hora de entrega debe ser al menos de 48 horas posteriores a la fecha actual.",false);
//                horaTxt.setTextColor(Color.parseColor("#c8102e"));
//                return false;
//            }
//        }

        horaTxt.setTextColor(Color.parseColor("#000000"));
        horaCorrecta = true;
        return true;
    }

    public Boolean valiadrDatos(){
        // Validar
        if(fehcaFormater != "DD/MM/YYYY" && horaFormater != "00:00:00" && ubicacionTxt.getText() != "Sin ubicacion" && !ubicacionTxt.getText().equals("") && (Lat != "" && Lon != "") && (Lat != null && Lon != null) && (Lat != "0" && Lon != "0") && (Lat != "0.0" && Lon != "0.0")){
            return true;
        }
        return false;
    }
    @Override
    protected void onStart() {
        super.onStart();
    }
    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();
        Lat = intent.getStringExtra("Latitud");
        Lon = intent.getStringExtra("Longitud");
        if((Lat == "" && Lon == "")||(Lat == null && Lon == null)){
            ubicacionTxt.setText("Sin ubicacion");
            if(pref.getString("HoraPosibleCambio","00:00:00") == ""){
                editor.putString("HoraPosibleCambio","00:00:00");
                editor.apply();
            }
            if(pref.getString("FechaPosibleCambio","DD/MM/YYYY") == ""){
                editor.putString("FechaPosibleCambio","DD/MM/YYYY");
                editor.apply();
            }
            String fehcaLocal = pref.getString("FechaPosibleCambio","DD/MM/YYYY");
            String horaLocal = pref.getString("HoraPosibleCambio","00:00:00");
            String peticionesLocal = pref.getString("PeticionesPosibleCambio","");

            horaTxt.setText("00:00:00");
            fechaTxt.setText("DD/MM/YYYY");
            //fehcaFormater = pref.getString("FormatterFechaPosibleCambio","DD/MM/YYYY");
            //horaFormater = pref.getString("FormatterHoraPosibleCambio","00:00:00");
            fehcaFormater = "DD/MM/YYYY";
            horaFormater = "00:00:00";
            peticiones.setText("");
            guardar.setEnabled(false);

        } else {
            String fehcaLocal = pref.getString("FechaPosibleCambio","DD/MM/YYYY");
            String horaLocal = pref.getString("HoraPosibleCambio","00:00:00");
            String peticionesLocal = pref.getString("PeticionesPosibleCambio","");
            fehcaFormater = pref.getString("FormatterFechaPosibleCambio","DD/MM/YYYY");
            horaFormater = pref.getString("FormatterHoraPosibleCambio","00:00:00");
            horaTxt.setText(horaLocal);
            fechaTxt.setText(fehcaLocal);
            peticiones.setText(peticionesLocal);
            ubicacionTxt.setText(intent.getStringExtra("Direccion"));
            guardar.setEnabled(true);
            //Toast.makeText(this, ""+pref.getString("FormatterFechaPosibleCambio","DD/MM/YYYY")+" : "+pref.getString("FormatterHoraPosibleCambio","00:00:00"), Toast.LENGTH_LONG).show();
        }
    }
    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        editor.putString("FechaPosibleCambio","DD/MM/YYYY");
//        editor.putString("HoraPosibleCambio","00:00:00");
//        editor.putString("Notas","");
//        editor.apply();
    }


    public void guardarSolicitud(){

        if(control){
            Toast.makeText(this, "Solicitud enviada. espere", Toast.LENGTH_SHORT).show();
            return;
        }else{
            control = true;
        }

        //Toast.makeText(this, fehcaFormater+" "+horaFormater, Toast.LENGTH_SHORT).show();
        if (!valiadrDatos()){
            alertDialog("Datos invalidos","Algunos datos de la hora, fecha o ubicacion estan incorrectos.",false);
            return;
        }
        control = false;
        nDialog = new SpotsDialog.Builder().setContext(solicitud.this).setMessage("Guardando...").setTheme(R.style.Custom).build();
        nDialog.show();
        String[] CalleNumero = ubicacionTxt.getText().toString().split("#");
        // Peticion Post
        final ConstantesGlobales direcciones = new ConstantesGlobales();
        final RequestQueue peticion = Volley.newRequestQueue(solicitud.this);
        String url = direcciones.getApi() +""+ direcciones.getCam();
        final String data = "{"+
                " \"nIDCliente\": \""+ pref.getInt("nIDCliente", 0) + "\", " +
                " \"nIDAutomovil\": "+pref.getString("idVehiculoParaSolicitud","0")+", " +
                " \"Calle\": \""+CalleNumero[0].trim()+"\", " +
                " \"NoExterior\": \""+CalleNumero[1].trim()+"\", " +
                " \"Longitud\": \""+ Lon +"\", " +
                " \"Latitud\": \""+ Lat +"\", " +
                " \"FechaHora\": \""+fehcaFormater+" "+horaFormater+"\", " +
                " \"Notas\": \""+peticiones.getText()+"\", " +
                " \"Estatus\":1, " +
                " \"option\": 1" +
                "}";
        //Toast.makeText(this,data,Toast.LENGTH_LONG).show();
        final Map<String, String>[] datos = new Map[]{new HashMap<String, String>()};
        StringRequest stringRegquest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                nDialog.dismiss();
                try {
                    JSONArray json = new JSONArray( response );
                    if(json.getJSONObject(0).getBoolean("status")){
                        control = false;
                        editor.putString("FechaPosibleCambio","DD/MM/YYYY");
                        editor.putString("HoraPosibleCambio","00:00:00");
                        editor.putString("FormatterFechaPosibleCambio","DD/MM/YYYY");
                        editor.putString("FormatterHoraPosibleCambio","00:00:00");
                        editor.putString("PeticionesPosibleCambio","");
                        editor.putString("Notas",""+peticiones.getText());
                        editor.putString("FechaHora",fehcaFormater+" "+horaFormater);
                        editor.putString("fotoAuto", pref.getString("ImagenDetalleList","a,a"));
                        editor.putString("ubicacionCambio",ubicacionTxt.getText().toString());
                        editor.putString("Latitud",Lat);
                        editor.putString("Longitud",Lon);
                        editor.putBoolean("statusCambio",true);
                        editor.putInt("lastIdCambio",json.getJSONObject(0).getInt("lastID"));
                        editor.apply();
                        Intent intent = new Intent(solicitud.this, MainActivity.class);
                        startActivity(intent);
                    } else {
                        control = false;
                        alertDialog("Alerta!","Error al guardar la solicitud, "+json.getJSONObject(0).get("resp"),false);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                control = false;
                Log.i("Error de respuesta","Error");
                error.printStackTrace();
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRegquest.setRetryPolicy(policy);
       peticion.add(stringRegquest);
    }

    public Boolean validarDisponibilidadVehiculo(){
        //Toast.makeText(this, fehcaFormater+" "+horaFormater, Toast.LENGTH_SHORT).show();
        if (!valiadrDatos()){
            alertDialog("Datos invalidos","Algunos datos de la hora, fecha o ubicacion estan incorrectos.",false);
            return false;
        }
        nDialog = new SpotsDialog.Builder().setContext(solicitud.this).setMessage("Actualizando...").setTheme(R.style.Custom).build();
        nDialog.show();
        String[] CalleNumero = ubicacionTxt.getText().toString().split("#");
        // Peticion Post
        final ConstantesGlobales direcciones = new ConstantesGlobales();
        final RequestQueue peticion = Volley.newRequestQueue(solicitud.this);
        String url = direcciones.getApi() +""+ direcciones.getCam();
        final String data = "{"+
                " \"nIDAutomovil\": "+pref.getString("idVehiculoParaSolicitud","0")+", " +
                " \"option\": 4" +
                "}";
        control = false;
        final Map<String, String>[] datos = new Map[]{new HashMap<String, String>()};
        StringRequest stringRegquest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                nDialog.dismiss();
                try {
                    JSONArray json = new JSONArray( response );
                    if(json.getJSONObject(0).getBoolean("status")){
                        // Vehiculo sigue vigente
                        //Toast.makeText(solicitud.this, "vehiculo diponible", Toast.LENGTH_LONG).show();
                        guardarSolicitud();
                    } else {
                        alertDialog("Alerta!","El vehiculo ya no esta disponible.",false);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                nDialog.dismiss();
                control = false;
                Log.i("Error de respuesta","Error");
                error.printStackTrace();
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRegquest.setRetryPolicy(policy);
        peticion.add(stringRegquest);
        return false;
    }

    private void alertDialog(String titulo, String msg, boolean dismiss) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(titulo);
        builder.setMessage(msg);

        builder.setPositiveButton("Aceptar", null);
        if ( dismiss){
            builder.setNegativeButton("Cancelar", null);
        }
        AlertDialog dialog = builder.create();
        dialog.show();
    }




    private boolean checkPermissions(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            return true;
        }
        return false;
    }

    private void requestPermissions(){
        ActivityCompat.requestPermissions(
                this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_ID
        );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_ID) {
            if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                // Granted. Start getting the location information
                irMapaSolicitud();
            }
        }
    }
    private boolean isLocationEnabled(){
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
                LocationManager.NETWORK_PROVIDER
        );
    }

    public void irMapaSolicitud(){
        Intent intent = new Intent(solicitud.this, mapaSolicitud.class);
        intent.putExtra("Fecha",fechaTxt.getText().toString());
        intent.putExtra("Hora",horaTxt.getText().toString());
        intent.putExtra("Peticiones",peticiones.getText().toString());
        editor.putString("FechaPosibleCambio",fechaTxt.getText().toString());
        editor.putString("HoraPosibleCambio",horaTxt.getText().toString());
        editor.putString("FormatterFechaPosibleCambio",fehcaFormater);
        editor.putString("FormatterHoraPosibleCambio",horaFormater);
        editor.putString("PeticionesPosibleCambio",peticiones.getText().toString());
        editor.apply();
        startActivity(intent);
    }

}

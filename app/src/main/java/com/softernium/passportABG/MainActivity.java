package com.softernium.passportABG;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.badge.BadgeDrawable;
import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.softernium.passportABG.utils.NukeSSLCerts;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    private TextView mTextMessage, alertCount;
    private BottomNavigationView navView;
    Toolbar toolbar;
    private SharedPreferences pref;
    BadgeDrawable badge;
    private Boolean control = true;
    public boolean allowBackNav = false;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment selectedFragment = null;
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    selectedFragment = new InicioFragmento();
                    getSupportActionBar().setTitle("Inicio");
                    break;
                case R.id.navigation_dashboard:
                    selectedFragment = new vehiculosFragmento();
                    getSupportActionBar().setTitle("Lista de Vehículos");
                    break;
                case R.id.navigation_notifications:
                    selectedFragment = new notificacionesFragmento();
                    getSupportActionBar().setTitle("Notificaciones");
                    badge.setVisible(false,true);
                    break;
                case R.id.navigation_configuracion:
                    selectedFragment = new configuracionFragmento();

                    getSupportActionBar().setTitle("Ajustes");
                    break;
                case R.id.navigation_S_O_S:
                    selectedFragment = new sosFragmento();
                    getSupportActionBar().setTitle("S.O.S - Contacto de emergencia");
                    break;
            }

            getSupportFragmentManager().beginTransaction().replace(R.id.fragments_container, selectedFragment).commit();

            return true;
        }
    };

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new NukeSSLCerts().nuke(this);

        pref = this.getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        setContentView(R.layout.activity_main);
        Fragment fragmento = new InicioFragmento();
        getSupportFragmentManager().beginTransaction().replace(R.id.fragments_container, fragmento).commit();
        navView = findViewById(R.id.nav_view);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        toolbar = findViewById(R.id.toolbar);
        getSupportActionBar().show();
        getSupportActionBar().setTitle("Inicio");
        int menuItemId = navView.getMenu().getItem(2).getItemId();
        this.badge = navView.getOrCreateBadge(menuItemId);
        this.badge.setVisible(false,true);
        //setBadgeValueNotification("3");
    }

    @Override
    public void onBackPressed() {
        if (shouldAllowBack()) {
            super.onBackPressed();
            this.allowBackNav =  false;
            startActivity(new Intent(this, louncher.class));
            finish();
        } else {
            this.allowBackNav = true;
            Toast.makeText(this, "Presiona de nuevo para salir", Toast.LENGTH_SHORT).show();
            // No navegar
        }
    }

    public boolean shouldAllowBack(){
        return this.allowBackNav;
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.obtenerNotificaciones();
    }

    public void timeOut(int timeInterval){
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        if(control){
                            obtenerNotificaciones();
                        }
                    }
                }, timeInterval);
    }

    @Override
    protected void onPause() {
        super.onPause();
        control = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        control = false;
    }

    public void obtenerNotificaciones(){
        // Peticion Post
        final ConstantesGlobales direcciones = new ConstantesGlobales();
        RequestQueue peticion = Volley.newRequestQueue(this);
        String url = direcciones.getApi() +""+ direcciones.getNot();

        final String data = "{"+
                " \"kndUser\": \""+ pref.getInt("kndUser", 0) + "\", " +
                " \"usrId\":\"" + pref.getInt("nIDCliente", 0) +"\", " +
                " \"option\": 1" +
                "}";
        final Map<String, String>[] datos = new Map[]{new HashMap<String, String>()};
        StringRequest stringRegquest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray json = new JSONArray( response );
                    //alertDialog("",""+json,false);
                    Log.i("JSONNotificaciones",json.toString());
                    if (json.getJSONObject(0).getBoolean("status")){
                        //notificaciones = json;
                        //Hay notificaciones
                        int contador = 0;
                        for (int i = 0; i < json.length(); i++){
                            JSONObject obj = json.getJSONObject(i);
                            if(!obj.getString("Estatus").equals("LEIDO")){
                                contador ++ ;
                            }
                        }
                        if (contador != 0){
                            badge.setVisible(true,true);
                            badge.setNumber(contador);
                        } else {
                            badge.setVisible(false,true);
                        }
                    } else {
                        badge.setVisible(false,true);
                        //Sin notificaciones
                    }
                    if(control){
                        timeOut(15000);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Error de respuesta","Error");
                error.printStackTrace();
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        peticion.add(stringRegquest);
    }

    public void setBadgeValueNotification(String value){

//        //alertCount = (TextView) MenuItemCompat.getActionView(navView);
//        //alertCount = (TextView) findViewById(R.id.navigation_notifications);
//        alertCount = (TextView) navView.getMenu().findItem(R.id.navigation_notifications);
//        //Gravity property aligns the text
//        alertCount.setGravity(Gravity.CENTER_VERTICAL);
//        alertCount.setTypeface(null, Typeface.BOLD);
//        alertCount.setTextColor(getResources().getColor(R.color.colorAccent));
//        alertCount.setText("99+");

    }

    @SuppressLint("RestrictedApi")
    private void removeShiftMode(BottomNavigationView bottomNavigationView) {
        BottomNavigationView menuView = (BottomNavigationView) bottomNavigationView.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                //noinspection RestrictedApi
                item.setShifting(false);
                //noinspection RestrictedApi
                item.setChecked(item.getItemData().isChecked());
            }
        } catch (NoSuchFieldException e) {
        } catch (IllegalAccessException e) {
        }
    }

}

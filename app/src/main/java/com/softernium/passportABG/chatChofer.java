package com.softernium.passportABG;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;
import com.softernium.passportABG.utils.NukeSSLCerts;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;

public class chatChofer extends AppCompatActivity {

    private ListView tabla;
    private JSONArray chats = new JSONArray();
    private ImageButton send, call;
    private TextInputEditText txt;

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private int time = 1000 * 6;
    private Boolean control = true;
    private Timer interval;
    private static final int REQUEST_PHONE_CALL = 1;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new NukeSSLCerts().nuke(this);

        setContentView(R.layout.activity_chat_chofer);
        getSupportActionBar().setTitle("Chat con el cliente");

        tabla = findViewById(R.id.tablaChatChofer);
        txt = findViewById(R.id.msgChofer);
        send = findViewById(R.id.sendMsgChofer);
        call = findViewById(R.id.callCliente);
        pref = chatChofer.this.getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        editor = pref.edit();
        control = true;
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!txt.getText().toString().equals("")) {
                    JSONObject obj = new JSONObject();
                    try {
                        obj.put("isIncoming",false);
                        obj.put("Texto",txt.getText().toString());
                        obj.put("FechaCreacion","Ahora");
                        chats.put(obj);
                        cargarChat();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    sendMsgFunction(txt.getText().toString());
                    txt.setText("");
                }
            }
        });
        if(pref.getString("celularCliente","0").equals("0")){
            call.setEnabled(false);
        }
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callCliente();
                Toast.makeText(chatChofer.this, "Llamando cliente", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void callCliente(){
        String phoneNo = "" + pref.getString("celularCliente","0");
        if(phoneNo == "0"){
            Toast.makeText(this, "Cliente sin numero de telefono", Toast.LENGTH_SHORT).show();
            return;
        }
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(chatChofer.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(chatChofer.this, new String[]{Manifest.permission.CALL_PHONE},REQUEST_PHONE_CALL);
            }
            else {

                if (!TextUtils.isEmpty(phoneNo)) {
                    String dial = "tel:" + phoneNo;
                    startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(dial)));
                }else {
                    Toast.makeText(chatChofer.this, "No hay numero de contacto", Toast.LENGTH_SHORT).show();
                }
                //callAdmin();
            }
        }
        else {
            if (!TextUtils.isEmpty(phoneNo)) {
                String dial = "tel:" + phoneNo;
                startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(dial)));
            }else {
                Toast.makeText(chatChofer.this, "No hay numero de contacto", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PHONE_CALL: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    callCliente();
                }
                else {

                }
                return;
            }
        }
    }


    public void timeOut(int timeInterval){
        Log.i("Interval","Intervlo establecido");
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        if(control){
                            obtenerChats();
                        }
//                        if ("".equals(txt.getText().toString()) && control) {
//                        }else{
//                        }
                    }
                }, timeInterval);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Detener al intervalo para la peticion de los chats
        //interval.cancel();
        control = false;
    }

    @Override
    protected void onStop() {
        super.onStop();
        control = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        control = false;
    }

    @Override
    protected void onStart() {
        super.onStart();
        control = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        obtenerChats();
        control = true;
    }

    private void sendMsgFunction(String msg) {
        // Peticion Post
        final ConstantesGlobales direcciones = new ConstantesGlobales();
        RequestQueue peticion = Volley.newRequestQueue(chatChofer.this);
        String url = direcciones.getApi() +""+ direcciones.getChat();
        final String data = "{"+
                " \"nIDCliente\": \""+ pref.getString("nIDClienteEntrega", "") + "\", " +
                " \"nIDUsuario\": 0, " +
                " \"nIDChofer\": \""+ pref.getInt("nIDCliente", 0) +  "\", " +
                " \"Remitente\": \"CHOFER\", " +
                " \"Texto\": \""+ msg + "\", " +
                " \"Estatus\": \"NO LEIDO\", " +
                " \"option\": 2" +
                "}";
        final Map<String, String>[] datos = new Map[]{new HashMap<String, String>()};
        StringRequest stringRegquest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //timeOut(1000);
                try {
                    JSONArray json = new JSONArray( response );
                    Date date = new Date();
                    //Caso 3: obtenerhora y fecha y salida por pantalla con formato:
                    DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
                    Log.i("HoraFormateadaConFecha","Hora y fecha: "+hourdateFormat.format(date));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Error de respuesta","Error");
                error.printStackTrace();
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRegquest.setRetryPolicy(policy);
        peticion.add(stringRegquest);
    }

    private void cargarChat() {
        celdasChatsAdapter adapterNew = new celdasChatsAdapter(chatChofer.this, R.layout.celdas_chat_generales, chats);
        tabla.setAdapter(adapterNew);
        tabla.setSelection(chats.length());
    }

    public void obtenerChats(){
        // Peticion Post
        final ConstantesGlobales direcciones = new ConstantesGlobales();
        RequestQueue peticion = Volley.newRequestQueue(chatChofer.this);
        String url = direcciones.getApi() +""+ direcciones.getChat();

        final String data = "{"+
                " \"nIDCliente\": \""+ pref.getString("nIDClienteEntrega", "0") + "\", " +
                " \"nIDChofer\": \""+ pref.getInt("nIDCliente", 0) + "\", " +
                " \"nIDUsuario\": 0, " +
                " \"Remitente\":\"CHOFER\", " +
                " \"option\": 1" +
                "}";
        final Map<String, String>[] datos = new Map[]{new HashMap<String, String>()};

        StringRequest stringRegquest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONArray json = new JSONArray( response );
                    if(chats.length() != json.length()){
                        chats = json;
                        cargarChat();
                        leerTodo();
                    }
                    if(control){
                        timeOut(4000);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Error de respuesta","Error");
                error.printStackTrace();
            }
        }){

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        peticion.add(stringRegquest);

    }

    public void leerTodo(){
        ArrayList<String> ids = new ArrayList<>();
        for(int i = 0; i < chats.length(); i++){
            try {
                if("NO LEIDO".equals(chats.getJSONObject(i).getString("Estatus"))){
                    ids.add(chats.getJSONObject(i).getString("nIDChat"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } // Fin del for
        if(ids.size() > 0){
            String parametros = "";
            for(int i = 0; i < ids.size(); i++){
                if (ids.size()-1 == i) {
                    parametros = parametros +""+ ids.get(i);
                }else{
                    parametros = parametros +""+ ids.get(i)+",";
                }
            }
            // Peticion Post
            final ConstantesGlobales direcciones = new ConstantesGlobales();
            RequestQueue peticion = Volley.newRequestQueue(chatChofer.this);
            String url = direcciones.getApi() +""+ direcciones.getChat();

            final String data = "{"+
                    " \"ids\": \""+ parametros + "\", " +
                    " \"option\": 3" +
                    "}";
            final Map<String, String>[] datos = new Map[]{new HashMap<String, String>()};

            StringRequest stringRegquest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONArray json = new JSONArray( response );
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i("Error de respuesta","Error");
                    error.printStackTrace();
                }
            }){

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return data == null ? null : data.getBytes("utf-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                        return null;
                    }
                }
            };

            peticion.add(stringRegquest);
        } else {
        }
    }// Fin de leer todo

}// fin de la clase

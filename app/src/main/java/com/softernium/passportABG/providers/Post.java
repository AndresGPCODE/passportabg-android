package com.softernium.passportABG.providers;

public class Post {


    private String body;

    public String getJson() {
        return body;
    }

    public void setJson(String json) {
        this.body = json;
    }

    @Override
    public String toString() {
        return "" + getJson();
    }

}

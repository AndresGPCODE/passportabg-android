package com.softernium.passportABG;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

public class louncher extends AppCompatActivity{

    private static final String CHANNEL_ID = "NOTIFICACION";
    private static final int NOTIFICATION_ID = 5;

    Button loginbtn;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    public boolean allowBackNav = false;
//    public static final String INBOX = "content://sms/inbox";
//    public static final String SENT = "content://sms/sent";
//    public static final String DRAFT = "content://sms/draft";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //new NukeSSLCerts().nuke(this);
        setContentView(R.layout.activity_louncher);
        getSupportActionBar().hide();
        loginbtn = findViewById(R.id.Logbtn);
        pref = louncher.this.getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        editor = pref.edit();
        loginbtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Log.i("Tipo usuario: ","" + pref.getInt("kndUser", -1));
            if(pref.getBoolean("status" , false)){
                if(pref.getInt("kndUser", -1) == 0){
                    transitionToMain();
                } else if(pref.getInt("kndUser", -1) == 1){
                    transitionToMainChofer();
                } else {
                    transitionToHome();
                }
            }else{
                transitionToHome();
            }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void onBackPressed() {
        if (shouldAllowBack()) {
            //super.onBackPressed();
//            this.allowBackNav =  false;
//            startActivity(new Intent(this, louncher.class));
//            finish();
//            System.exit(0);
        } else {
            //this.allowBackNav = true;
            //Toast.makeText(this, "Presiona de nuevo para salir", Toast.LENGTH_SHORT).show();
            // No navegar
        }
    }

    public boolean shouldAllowBack(){
        return this.allowBackNav;
    }

    private void createNotificacion() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            CharSequence name = "Notificacion";
            NotificationChannel chanel = new NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManager manager = (NotificationManager) this.getSystemService(NOTIFICATION_SERVICE);
            manager.createNotificationChannel(chanel);
        }
        NotificationCompat.Builder thisNotifi = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.abg_sms)
                .setContentTitle("Notificacion de prueba")
                .setContentText("Esta notificacion es de pruebas para testear recepcion y gestion de SMS")
                .setColor(Color.BLUE)
                .setLights(Color.WHITE, 1000, 1000)
                .setDefaults(Notification.DEFAULT_SOUND)
                .setVibrate(new long[]{1000,1000,1000,1000,1000,1000,1000,1000})
                .setPriority(NotificationCompat.PRIORITY_HIGH);

        NotificationManagerCompat notificacionManager = NotificationManagerCompat.from(this.getApplicationContext());
        notificacionManager.notify(NOTIFICATION_ID, thisNotifi.build());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    final String myPackageName = getPackageName();
//                    if (Telephony.Sms.getDefaultSmsPackage(getApplicationContext()).equals(myPackageName)) {
//                        List<Sms> lst = getAllSms();
//                        Toast.makeText(this, ""+lst, Toast.LENGTH_LONG).show();
//                    }
                }
            }
        }
    }

    private void transitionToHome() {
        Intent intent = new Intent( louncher.this, login.class);
        startActivity(intent);
    }
    private void transitionToMain() {
        Intent intent = new Intent( louncher.this, MainActivity.class);
        startActivity(intent);
    }
    private void transitionToMainChofer() {
//        Intent intent = new Intent( louncher.this, MapsActivity.class);
        Intent intent = new Intent( louncher.this, mainChofer.class);
        startActivity(intent);
    }
    private void alertDialog(String titulo, String msg, boolean dismiss) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(titulo);
        builder.setMessage(msg);

        builder.setPositiveButton("Aceptar", null);
        if ( dismiss){
            builder.setNegativeButton("Cancelar", null);
        }
        AlertDialog dialog = builder.create();
        dialog.show();
    }


}



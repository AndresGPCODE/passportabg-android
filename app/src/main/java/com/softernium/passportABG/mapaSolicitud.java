package com.softernium.passportABG;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.softernium.passportABG.utils.NukeSSLCerts;

public class mapaSolicitud extends AppCompatActivity {

    private Button aceptar;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private String FechaIntent, HoraIntent, PeticionesIntent;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new NukeSSLCerts().nuke(this);
        getSupportActionBar().setTitle("Ubicacion de entrega");

        setContentView(R.layout.activity_mapa_solicitud);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_mapa, new mapaFragmento()).commit();
        //getSupportFragmentManager().beginTransaction().replace(R.id.fragment_mapa, new mapaSolicitudNuevo()).commit();
        pref = getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        editor = pref.edit();
        aceptar = findViewById(R.id.aceptarUbicacionBtn);
        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mapaSolicitud.this, solicitud.class);
                intent.putExtra("Latitud",pref.getFloat("Latitud", 0));
                intent.putExtra("Longitud",pref.getFloat("Longitud", 0));
                intent.putExtra("Fecha",FechaIntent);
                intent.putExtra("Hora",HoraIntent);
                intent.putExtra("Peticiones",PeticionesIntent);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();
        FechaIntent = intent.getStringExtra("Fecha") + "";
        HoraIntent = intent.getStringExtra("Hora") + "";
        PeticionesIntent = intent.getStringExtra("Peticiones") + "";
    }
}

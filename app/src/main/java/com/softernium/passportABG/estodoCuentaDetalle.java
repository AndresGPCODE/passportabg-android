package com.softernium.passportABG;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.softernium.passportABG.utils.NukeSSLCerts;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.text.NumberFormat;
import java.util.Locale;

public class estodoCuentaDetalle extends AppCompatActivity {

    private ListView tabla;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private JSONArray listaDetalles;
    private TextView totalLabel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new NukeSSLCerts().nuke(this);

        setContentView(R.layout.activity_estodo_cuenta_detalle);
        getSupportActionBar().setTitle("Estado de cuenta");
        //listaDetalles = findViewById(R.id.listaDetallesCuenta);
        tabla = findViewById(R.id.listaDetallesCuenta);
        pref = estodoCuentaDetalle.this.getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        editor = pref.edit();
        totalLabel = findViewById(R.id.estadoTotal);
        obtenerEstadoCuenta();
//        JSONObject obj = new JSONObject();
//        try {
//            obj.put("Detalle","Gasolina");
//            obj.put("Costo","150");
//            listaDetalles.put(obj);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
        //crearTabla();

    }


    private void crearTabla(){

        detallesCuenta adapterNew = new detallesCuenta(this ,R.layout.list_cuenta, listaDetalles);
        tabla.setAdapter(adapterNew);
        tabla.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int posision, long id) {
                try {
                    alertDialog(listaDetalles.getJSONObject(posision).getString("Detalle"),listaDetalles.getJSONObject(posision).getString("Descripcion"),false);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }
    public void obtenerEstadoCuenta(){
        // Peticion Post
        final ConstantesGlobales direcciones = new ConstantesGlobales();
        RequestQueue peticion = Volley.newRequestQueue(this);
        String url = direcciones.getApi() +""+ direcciones.getEdoCu();

        final String data = "{"+
                " \"nIDCliente\":\"" + pref.getInt("nIDCliente", 0) +"\", " +
                " \"option\": 1" +
                "}";
        StringRequest stringRegquest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray json = new JSONArray( response );
                    if (json.getJSONObject(0).getBoolean("status")){
                        //Log.i("EstadoCuenta",""+json);
                        listaDetalles = json;
                        Double total = 0.0;
                        for (int i = 0; i < json.length(); i++){
                            total = total + json.getJSONObject(i).getDouble("Costo");
                        }
                        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.CANADA);
                        String currency = format.format(total);
                        totalLabel.setText("Total:  "+currency);
                        crearTabla();
                    }else{
                        totalLabel.setText("$ 0.00");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Error de respuesta","Error");
                error.printStackTrace();
            }
        }){

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        peticion.add(stringRegquest);
    }





    private void alertDialog(String titulo, String msg, boolean dismiss) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(titulo);
        builder.setMessage(msg);

        builder.setPositiveButton("Aceptar", null);
        if ( dismiss){
            builder.setNegativeButton("Cancelar", null);
        }
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}


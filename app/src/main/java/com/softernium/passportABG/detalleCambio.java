package com.softernium.passportABG;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import dmax.dialog.SpotsDialog;

public class detalleCambio extends AppCompatActivity {

    private TextView fechaTxt;
    private TextView horaTxt;
    private TextView ubicacionTxt;
    private TextInputEditText peticiones;
    private ImageButton fechaBtn;
    private ImageButton horaBtn;
    private ImageButton ubicacionBtn;
    private ImageView vehiculo;
    private Button guardar;
    private Button cancelar;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private String AutoId, horaFormater = "00:00:00", fehcaFormater = "DD/MM/YYYY";
    private String Lat, Lon, LatAnt, LonAnt;
    private int mYear = 0, mMonth = 0, mDay = 0, mHour = 0, mMinute = 0;
    AlertDialog nDialog;
    private Boolean control = false,fechaCorrecta = true, horaCorrecta = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_cambio);
        getSupportActionBar().setTitle("Detalle próximo cambio.");
        fechaBtn = findViewById(R.id.fechaSolicitudBtnCambio);
        horaBtn = findViewById(R.id.horaSolicitudBtnCambio);
        ubicacionBtn = findViewById(R.id.ubicacionSolicitudBtnCambio);
        fechaTxt = findViewById(R.id.fechaSolicitudTextCambio);
        horaTxt = findViewById(R.id.horaSolicitudTextCambio);
        ubicacionTxt = findViewById(R.id.ubicacionSolicitudTextCambio);
        peticiones = findViewById(R.id.peticionesTextCambio);
        guardar = findViewById(R.id.guardarPeticionBtnCambio);
        cancelar = findViewById(R.id.cancelarPeticionBtnCambio);
        pref = getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        editor = pref.edit();
        vehiculo = findViewById(R.id.imageCambioVehiculo);
        ConstantesGlobales util = new ConstantesGlobales();

        String[] img = pref.getString("fotoAuto", "a,a").split(",");

        if(img[1] != "a"){
            vehiculo.setImageBitmap(util.stringtoImage(img[1]));
        }else{
            vehiculo.setImageResource(R.drawable.nif);
        }

        guardar.setTextColor(Color.parseColor("#ffffff"));
        cancelar.setTextColor(Color.parseColor("#ffffff"));

        ubicacionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(detalleCambio.this, cambioSolicitud.class);
                editor.putString("DetalleFechaPosibleCambio",""+fechaTxt.getText());
                editor.putString("DetalleHoraPosibleCambio",""+horaTxt.getText());
                editor.putString("DetalleFormatterFechaPosibleCambio",""+fehcaFormater);
                editor.putString("DetalleFormatterHoraPosibleCambio",""+horaFormater);
                editor.putString("DetallePeticionesPosibleCambio",""+peticiones.getText());
                editor.putBoolean("fechaCorrecta",fechaCorrecta);
                editor.putBoolean("horaCorrecta",horaCorrecta);
                editor.apply();
                startActivity(intent);
            }
        });

        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancelarSolicitud();
            }
        });
        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                guardarSolicitud();
            }
        });


        fechaBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            // Get Current Date
            final Calendar c = Calendar.getInstance();
            Date actual = c.getTime();
            c.setTime(actual);
            c.add(Calendar.DATE, 2);
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
            //Toast.makeText(solicitud.this, ""+mMonth, Toast.LENGTH_SHORT).show();
            DatePickerDialog datePickerDialog = new DatePickerDialog(detalleCambio.this,
                    new DatePickerDialog.OnDateSetListener() {

                        @RequiresApi(api = Build.VERSION_CODES.N)
                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            String mm;
                            String dd;

                            if((monthOfYear + 1) < 10){
                                mm = "0" + (monthOfYear + 1);
                            } else {
                                mm = "" + (monthOfYear + 1);
                            }

                            if(dayOfMonth < 10){
                                dd = "0" + dayOfMonth;
                            } else {
                                dd = "" + dayOfMonth;
                            }

                            fechaTxt.setText(dd + "-" + mm + "-" + year);
                            fechaTxt.setTextColor(Color.parseColor("#000000"));
                            fehcaFormater = year + "-" + mm + "-" + dd;
                            //alertDialog("hora",horaFormater,false);
                            //Toast.makeText(solicitud.this, ""+horaFormater, Toast.LENGTH_SHORT).show();
                            if(!validarFecha()){
                                if(!fechaCorrecta){
                                    fehcaFormater = "DD/MM/YYYY";
                                }
                            }
                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
            datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
            }
        });

        horaBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            // Get Current Time
            final Calendar c = Calendar.getInstance();
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);
            TimePickerDialog timePickerDialog = new TimePickerDialog(detalleCambio.this,
                new TimePickerDialog.OnTimeSetListener() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        String min,hora;
                        if(minute < 10){
                            min = "0"+minute;
                        } else {
                            min = ""+minute;
                        }
                        if(hourOfDay < 10){
                            hora = "0"+hourOfDay;
                        } else {
                            hora = ""+hourOfDay;
                        }
                        horaFormater = hora+":"+min+":00";
                        if(hourOfDay > 12){
                            horaTxt.setText((hourOfDay - 12) + ":" + min + ":00 P.M.");
                        } else {
                            horaTxt.setText(hourOfDay + ":" + min +":00 A.M.");
                        }
                        horaTxt.setTextColor(Color.parseColor("#000000"));
                        // Validar la hora seleccionada dentro de horas laborales
                        if(hourOfDay < 9 || hourOfDay > 19 || (hourOfDay == 19 && minute > 0)){
                            horaTxt.setTextColor(Color.parseColor("#c8102e"));
                            alertDialog("Fuera de horario de servicio","La hora de entrega debe respetar el horario de trabajo de 9:00 A.M. a 7:00 P.M.",false);
                            horaFormater = "00:00:00";
                        } else if(!validarHora()){
                            if(!horaCorrecta){
                                horaFormater = "00:00:00";
                            }
                        }
                    }
                }, mHour, mMinute, false);
            timePickerDialog.show();
            }
        });
    }

    public Boolean validarTiempoMargen(){
        fechaCorrecta = false;
        if(fehcaFormater == "" || fehcaFormater == null){
            alertDialog("Fecha Invalida","La fecha de entrega debe ser al menos de 24 horas posteriores a la fecha actual.",false);
            fechaTxt.setTextColor(Color.parseColor("#c8102e"));
            fechaCorrecta = false;
            return false;
        }
        if(fehcaFormater.trim() == "DD/MM/YYYY"){
            alertDialog("Fecha Invalida","La fecha de entrega debe ser al menos de 24 horas posteriores a la fecha actual.",false);
            fechaTxt.setTextColor(Color.parseColor("#c8102e"));
            fechaCorrecta = false;
            return false;
        }
        String[] arrFecha = fehcaFormater.split("-");
        int añoSeleccionado = Integer.parseInt(arrFecha[0]);
        int mesSeleccionado = Integer.parseInt(arrFecha[1]);
        int diaSeleccionado = Integer.parseInt(arrFecha[2]);
        if(añoSeleccionado < mYear){
            alertDialog("Fecha Invalida","La fecha de entrega debe ser al menos de 24 horas posteriores a la fecha actual.",false);
            fechaTxt.setTextColor(Color.parseColor("#c8102e"));
            fechaCorrecta = false;
            return false;
        }
        if(mesSeleccionado < (mMonth + 1)){
            alertDialog("Fecha Invalida","La fecha de entrega debe ser al menos de 24 horas posteriores a la fecha actual.",false);
            fechaTxt.setTextColor(Color.parseColor("#c8102e"));
            fechaCorrecta = false;
            return false;
        }
        if(diaSeleccionado < mDay){
            alertDialog("Fecha Invalida","La fecha de entrega debe ser al menos de 24 horas posteriores a la fecha actual.",false);
            fechaTxt.setTextColor(Color.parseColor("#c8102e"));
            fechaCorrecta = false;
            return false;
        }

        fechaCorrecta = true;
        horaCorrecta = false;

        if(horaFormater == "" || horaFormater == null){
            alertDialog("Hora Invalida","La hora de entrega debe ser al menos de 24 horas posteriores a la fecha actual.",false);
            horaTxt.setTextColor(Color.parseColor("#c8102e"));
            horaCorrecta = false;
            return false;
        }
        if(horaFormater.trim() == "00:00:00"){
            alertDialog("Hora Invalida","La hora de entrega debe ser al menos de 24 horas posteriores a la fecha actual.",false);
            horaTxt.setTextColor(Color.parseColor("#c8102e"));
            horaCorrecta = false;
            return false;
        }
        String[] arrTiempo = horaFormater.split(":");
        int horaSeleccionada = Integer.parseInt(arrTiempo[0]);
        int minutoSeleccionado = Integer.parseInt(arrTiempo[1]);
        if(horaSeleccionada < mHour){
            if(diaSeleccionado <= mDay){
                alertDialog("Hora Invalida","La hora de entrega debe ser al menos de 24 horas posteriores a la fecha actual.",false);
                horaTxt.setTextColor(Color.parseColor("#c8102e"));
                horaCorrecta = false;
                return false;
            }
        }
        fechaTxt.setTextColor(Color.parseColor("#000000"));
        horaTxt.setTextColor(Color.parseColor("#000000"));
        horaCorrecta = true;
        return true;
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    public Boolean validarFecha(){
        fechaCorrecta = false;
        if(fehcaFormater == "" || fehcaFormater == null){
            alertDialog("Fecha Invalida","La fecha de entrega debe ser al menos de 48 horas posteriores a la fecha actual.",false);
            fechaTxt.setTextColor(Color.parseColor("#c8102e"));
            return false;
        }
        if(fehcaFormater.trim() == "DD/MM/YYYY"){
            alertDialog("Fecha Invalida","La fecha de entrega debe ser al menos de 48 horas posteriores a la fecha actual.",false);
            fechaTxt.setTextColor(Color.parseColor("#c8102e"));
            return false;
        }

        String[] arrFecha = fehcaFormater.split("-");
        int añoSeleccionado = Integer.parseInt(arrFecha[0]);
        int mesSeleccionado = Integer.parseInt(arrFecha[1]);
        int diaSeleccionado = Integer.parseInt(arrFecha[2]);

//        if(añoSeleccionado < mYear){
//            alertDialog("Fecha Invalida","La fecha de entrega debe ser al menos de 48 horas posteriores a la fecha actual.",false);
//            fechaTxt.setTextColor(Color.parseColor("#c8102e"));
//            return false;
//        }
//        if(mesSeleccionado < (mMonth + 1)){
//            alertDialog("Fecha Invalida","La fecha de entrega debe ser al menos de 48 horas posteriores a la fecha actual.",false);
//            fechaTxt.setTextColor(Color.parseColor("#c8102e"));
//            return false;
//        }
//        if(diaSeleccionado < mDay){
//            alertDialog("Fecha Invalida","La fecha de entrega debe ser al menos de 48 horas posteriores a la fecha actual.",false);
//            fechaTxt.setTextColor(Color.parseColor("#c8102e"));
//            return false;
//        }
        // VALIDAR DIA DE LA SEMANA

        if(this.esFinDeSemana(diaSeleccionado + "/" + mesSeleccionado + "/" + añoSeleccionado)){
            alertDialog("Dia no valido","La fecha de entrega solo puede sen en dias laborales (Lunes a Viernes).",false);
            fechaTxt.setTextColor(Color.parseColor("#c8102e"));
            return false;
        }

        fechaTxt.setTextColor(Color.parseColor("#000000"));
        fechaCorrecta = true;
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public Boolean esFinDeSemana(String fecha){
        Date date1 = null;
        try {
            //date1 = new SimpleDateFormat("dd/MM/yyyy").parse(diaSeleccionado + "/" + mesSeleccionado + "/" + añoSeleccionado);
            date1 = new SimpleDateFormat("dd/MM/yyyy").parse(fecha);
            Calendar c = Calendar.getInstance();
            c.setTime(date1);
            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
            System.out.println("XXXXXXXX - " + dayOfWeek);
            if (dayOfWeek == 1 || dayOfWeek == 7){
                return true;
            }
            return false;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public Boolean validarHora(){
        horaCorrecta = false;
        String[] arrFecha = fehcaFormater.split("-");
        int añoSeleccionado = Integer.parseInt(arrFecha[0]);
        int mesSeleccionado = Integer.parseInt(arrFecha[1]);
        int diaSeleccionado = Integer.parseInt(arrFecha[2]);

        if(horaFormater == "" || horaFormater == null || horaFormater.trim() == "00:00:00"){
            alertDialog("Hora Invalida","La hora de entrega debe ser al menos de 48 horas posteriores a la fecha actual.",false);
            horaTxt.setTextColor(Color.parseColor("#c8102e"));
            return false;
        }

        String[] arrTiempo = horaFormater.split(":");
        int horaSeleccionada = Integer.parseInt(arrTiempo[0]);
        int minutoSeleccionado = Integer.parseInt(arrTiempo[1]);
        if(horaSeleccionada < mHour){
            if(diaSeleccionado == mDay){
                alertDialog("Hora Invalida","La hora de entrega debe ser al menos de 48 horas posteriores a la fecha actual.",false);
                horaTxt.setTextColor(Color.parseColor("#c8102e"));
                return false;
            }
        }

        horaTxt.setTextColor(Color.parseColor("#000000"));
        horaCorrecta = true;
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();
        Lat = intent.getStringExtra("Latitud");
        Lon = intent.getStringExtra("Longitud");
        //Toast.makeText(this, ""+Lat+" - "+Lon, Toast.LENGTH_SHORT).show();
//        horaTxt.setText(pref.getString("HoraPosibleCambio","Hora"));
//        fechaTxt.setText(pref.getString("FechaPosibleCambio","Fecha"));
        peticiones.setText(pref.getString("Notas",""));
        if(LatAnt == null && LonAnt == null){
            LatAnt = ""+pref.getString("Latitud","0.0");
            LonAnt = ""+pref.getString("Longitud","0.0");
        }


        if(pref.getString("DetalleFechaPosibleCambio","").equals("")){
            String[] fecha = pref.getString("FechaHora"," ").split(" ");
            fehcaFormater =  fecha[0];
            horaFormater = fecha[1];
            String[] fechaBr = fecha[0].split("-");
            String[] horaBr = fecha[1].split(":");
            fechaTxt.setText(fechaBr[2]+"-"+fechaBr[1]+"-"+fechaBr[0]);
            if(Integer.parseInt(horaBr[0]) > 12){
                int localint = Integer.parseInt(horaBr[0]) - 12;
                horaTxt.setText(""+localint+":"+horaBr[1]+":"+horaBr[2]+" P.M.");
            } else{
                int localint = Integer.parseInt(horaBr[0]);
                horaTxt.setText(""+localint+":"+horaBr[1]+":"+horaBr[2]+" A.M.");
            }
            peticiones.setText(pref.getString("Notas",""));
        }else{
            fechaTxt.setText(pref.getString("DetalleFechaPosibleCambio","DD/MM/YYYY"));
            horaTxt.setText(pref.getString("DetalleHoraPosibleCambio","00:00:00"));
            peticiones.setText(pref.getString("DetallePeticionesPosibleCambio",""));
            fehcaFormater =  ""+pref.getString("DetalleFormatterFechaPosibleCambio","DD/MM/YYYY");
            horaFormater = ""+pref.getString("DetalleFormatterHoraPosibleCambio","00:00:00");
            fechaCorrecta = pref.getBoolean("fechaCorrecta",false);
            horaCorrecta = pref.getBoolean("horaCorrecta",false);
            if(!fechaCorrecta){
                fehcaFormater = "DD/MM/YYYY";
                fechaTxt.setTextColor(Color.parseColor("#c8102e"));
            }
            if(!horaCorrecta){
                horaFormater = "00:00:00";
                horaTxt.setTextColor(Color.parseColor("#c8102e"));
            }
            //Toast.makeText(this, ""+horaFormater+""+fehcaFormater, Toast.LENGTH_SHORT).show();
        }

        if(Lat == "0" && Lon == "0" || Lat == null && Lon == null){
            Lat = LatAnt;
            Lon = LonAnt;
            ubicacionTxt.setText(pref.getString("ubicacionCambio","Sin ubicacion"));
            guardar.setEnabled(true);
        } else {
            ubicacionTxt.setText(intent.getStringExtra("Direccion"));
            guardar.setEnabled(true);
        }
    }

    public Boolean valiadrDatos(){
        // Validar
        //Toast.makeText(this, fehcaFormater+" - "+horaFormater+" - "+ubicacionTxt.getText()+"", Toast.LENGTH_SHORT).show();
        if(!fehcaFormater.equals("DD/MM/YYYY") && !horaFormater.equals("00:00:00") && ubicacionTxt.getText() != "Sin ubicacion"){
            return true;
        }
        return false;
    }



    public void guardarSolicitud(){
        //Toast.makeText(this, fehcaFormater+" "+horaFormater, Toast.LENGTH_SHORT).show();
        if (!valiadrDatos()){
            alertDialog("Datos invalidos","Algunos datos de la hora, fecha o ubicacion estan incorrectos.",false);
            return;
        }
        nDialog = new SpotsDialog.Builder().setContext(detalleCambio.this).setMessage("Actualizando...").setTheme(R.style.Custom).build();
        nDialog.show();
        String[] CalleNumero = ubicacionTxt.getText().toString().split("#");
        // Peticion Post
        final ConstantesGlobales direcciones = new ConstantesGlobales();
        final RequestQueue peticion = Volley.newRequestQueue(detalleCambio.this);
        String url = direcciones.getApi() +""+ direcciones.getCam();
        final String data = "{"+
                " \"nIDCliente\": \""+ pref.getInt("nIDCliente", 0) + "\", " +
                " \"nIDSolicitud\": "+pref.getInt("lastIdCambio",0)+", " +
                " \"Calle\": \""+CalleNumero[0].trim()+"\", " +
                " \"NoExterior\": \""+CalleNumero[1].trim()+"\", " +
                " \"Longitud\": \""+ Lon +"\", " +
                " \"Latitud\": \""+ Lat +"\", " +
                " \"FechaHora\": \""+ fehcaFormater+" "+horaFormater +"\", " +
                " \"Notas\": \""+peticiones.getText()+"\", " +
                " \"option\": 2" +
                "}";
        control = false;
        //alertDialog("",""+data,false);
        //Toast.makeText(this, ""+ fehcaFormater+" "+horaFormater, Toast.LENGTH_LONG).show();
        final Map<String, String>[] datos = new Map[]{new HashMap<String, String>()};
        StringRequest stringRegquest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                nDialog.dismiss();
                control = false;
                try {
                    JSONArray json = new JSONArray( response );
                    if(json.getJSONObject(0).getBoolean("status")){
                        editor.putString("FechaHora",""+fehcaFormater+" "+horaFormater);
                        editor.putString("HoraPosibleCambio",""+horaTxt.getText());
                        editor.putString("Notas",""+peticiones.getText());
                        editor.putString("ubicacionCambio",ubicacionTxt.getText().toString());
                        editor.putBoolean("CambioEstablecido",true);
                        editor.apply();
                        Intent intent = new Intent(detalleCambio.this, MainActivity.class);
                        startActivity(intent);
                    } else {
                        alertDialog("Alerta!","Error al actualizarsu cambio",false);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                nDialog.dismiss();
                control = false;
                Log.i("Error de respuesta","Error");
                error.printStackTrace();
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRegquest.setRetryPolicy(policy);
        peticion.add(stringRegquest);
    }
    public void cancelarSolicitud(){


        nDialog = new SpotsDialog.Builder().setContext(detalleCambio.this).setMessage("Cancelando...").setTheme(R.style.Custom).build();
        nDialog.show();

        String[] CalleNumero = ubicacionTxt.getText().toString().split("#");
        // Peticion Post
        final ConstantesGlobales direcciones = new ConstantesGlobales();
        final RequestQueue peticion = Volley.newRequestQueue(detalleCambio.this);
        String url = direcciones.getApi() +""+ direcciones.getCam();
        final String data = "{"+
                " \"lastIdSolicitud\": \""+ pref.getInt("lastIdCambio", 0) + "\", " +
                " \"option\": 3" +
                "}";
        //Toast.makeText(this,data,Toast.LENGTH_LONG).show();
        final Map<String, String>[] datos = new Map[]{new HashMap<String, String>()};
        StringRequest stringRegquest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                nDialog.dismiss();
                try {
                    //alertDialog("",response.toString(),false);
                    JSONArray json = new JSONArray( response );
                    if(json.getJSONObject(0).getBoolean("status")){
                        editor.putString("FechaHora",""+" ");
                        editor.putString("HoraPosibleCambio",""+"");
                        editor.putString("Notas",""+"");
                        editor.putString("ubicacionCambio","");
                        editor.putBoolean("statusCambio",false);
                        editor.putInt("lastIdCambio",0);
                        editor.apply();
                        Intent intent = new Intent(detalleCambio.this, MainActivity.class);
                        startActivity(intent);
                    } else {
                        alertDialog("Alerta!","Problemas al eliminar la solicitud la solicitud",false);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                nDialog.dismiss();
                Log.i("Error de respuesta","Error");
                error.printStackTrace();
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        int socketTimeout = 8000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRegquest.setRetryPolicy(policy);
        peticion.add(stringRegquest);
    }
    private void alertDialog(String titulo, String msg, boolean dismiss) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(titulo);
        builder.setMessage(msg);

        builder.setPositiveButton("Aceptar", null);
        if ( dismiss){
            builder.setNegativeButton("Cancelar", null);
        }
        AlertDialog dialog = builder.create();
        dialog.show();
    }

}// Fin clase

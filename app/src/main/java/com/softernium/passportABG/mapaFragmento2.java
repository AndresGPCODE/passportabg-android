package com.softernium.passportABG;


import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;

import java.io.IOException;
import java.util.List;
import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 */
public class mapaFragmento2 extends Fragment implements OnMapReadyCallback {

    private MapView mapView;

    private View rootView;
    private GoogleMap gMap;

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    int PERMISSION_ID = 44;
    FusedLocationProviderClient mFusedLocationClient;
    private Geocoder geocoder;
    private ImageButton buscar;
    String lat = "0";
    String lon = "0";
    private TextInputEditText inputDireccionText;
    private GoogleMap mMap;
    private List<Address> address;
    private Geocoder geo;

    private String FechaIntent, HoraIntent, PeticionesIntent;

    private String calle, numero;

    private Button aceptar;
    public mapaFragmento2() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //new NukeSSLCerts().nuke(this.getContext());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(mapaFragmento2.this.getActivity());
        rootView =  inflater.inflate(R.layout.fragment_mapa_fragmento2, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapView = (MapView) rootView.findViewById(R.id.mapCambio);
        inputDireccionText = getActivity().findViewById(R.id.inputDireccionTextCambio);
        buscar = getActivity().findViewById(R.id.buscarButtonCambio);
        aceptar = getActivity().findViewById(R.id.aceptarUbicacionBtnCambio);
        pref = this.getActivity().getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        editor = pref.edit();

        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("Latitud",""+ pref.getString("Latitud","0"));
                Log.i("Longitud",""+ pref.getString("Longitud","0"));
                Intent intent = new Intent(mapaFragmento2.this.getContext(), detalleCambio.class);
                intent.putExtra("Latitud",pref.getString("Latitud", "0"));
                intent.putExtra("Longitud",pref.getString("Longitud", "0"));
                intent.putExtra("Direccion",inputDireccionText.getText().toString());
                intent.putExtra("Fecha",FechaIntent);
                intent.putExtra("Hora",HoraIntent);
                intent.putExtra("Peticiones",PeticionesIntent);
                startActivity(intent);
            }
        });

        buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LatLng ubi = getLocationFromAddress(getContext(), inputDireccionText.getText().toString());
                if(ubi != null){
                    //Toast.makeText(getContext(),"Lon: "+ubi.longitude +" // Lat: "+ubi.latitude,Toast.LENGTH_SHORT).show();
                    searchPoint(ubi);
                } else {
                    Toast.makeText(getContext(),"Sin resultados",Toast.LENGTH_SHORT).show();
                }
            }
        });

        if(mapView != null){
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this);
        }

        if (checkPermissions()) {
            if (isLocationEnabled()) {
                Toast.makeText(this.getActivity(), "Permisos Concedidos", Toast.LENGTH_SHORT).show();
                getLastLocation();
            } else {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        } else {
            requestPermissions();
            //getLastLocation();
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {



        geocoder = new Geocoder(getContext(), Locale.getDefault());
        gMap = googleMap;
        gMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                // para cuando movemos la vista del mapa y este se desplaza
                LatLng moveIt = new LatLng(gMap.getCameraPosition().target.latitude,gMap.getCameraPosition().target.longitude);
                gMap.clear();
                gMap.addMarker(new MarkerOptions().position(moveIt).title("Ubicacion actual").draggable(true));

                try {
                    address = geocoder.getFromLocation(moveIt.latitude,moveIt.longitude, 1);
                    //String adr = address.get(0).getAddressLine(0);
                    //calle = address.get(0).getThoroughfare();
                    if (address.get(0).getThoroughfare() == null){
                        calle = "Sin calle";
                    } else {
                        calle = address.get(0).getThoroughfare();
                    }

                    if (address.get(0).getFeatureName() == null){
                        numero = "0";
                    } else {
                        numero = address.get(0).getFeatureName();
                    }
                    String direccion = calle + " # "+ numero;
                    inputDireccionText.setText(direccion);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                editor.putString("Latitud",  ""+gMap.getCameraPosition().target.latitude);
                editor.putString("Longitud",   ""+gMap.getCameraPosition().target.longitude);
                editor.apply();
            }
        });
        gMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {
            }
            @Override
            public void onMarkerDrag(Marker marker) {
            }
            @Override
            public void onMarkerDragEnd(Marker marker) {
                editor.putString("Latitud",  ""+marker.getPosition().latitude);
                editor.putString("Longitud",  ""+marker.getPosition().longitude);
                editor.apply();
                try {
                    address = geocoder.getFromLocation(marker.getPosition().latitude,marker.getPosition().longitude, 1);
                    //String adr = address.get(0).getAddressLine(0);
                    //inputDireccionText.setText(address.get(0).getLocality());
                    //Toast.makeText(mapaFragmento.this.getContext(), ""+address.get(0), Toast.LENGTH_SHORT).show();
                    //calle = address.get(0).getThoroughfare();
                    if (address.get(0).getThoroughfare() == null){
                        calle = "Sin calle";
                    } else {
                        calle = address.get(0).getThoroughfare();
                    }
                    if (address.get(0).getFeatureName() == null){
                        numero = "0";
                    } else {
                        numero = address.get(0).getFeatureName();
                    }
                    String direccion = calle + " # "+ numero;
                    inputDireccionText.setText(direccion);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private boolean checkPermissions(){
        if (ActivityCompat.checkSelfPermission(this.getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this.getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            return true;
        }
        return false;
    }
    private void requestPermissions(){
        ActivityCompat.requestPermissions(
                mapaFragmento2.this.getActivity(), new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_ID
        );
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_ID) {
            if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                // Granted. Start getting the location information
                getLastLocation();
            }
        }
    }
    private boolean isLocationEnabled(){
        LocationManager locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
                LocationManager.NETWORK_PROVIDER
        );
    }
    @SuppressLint("MissingPermission")
    private void getLastLocation(){
        mFusedLocationClient.getLastLocation().addOnCompleteListener(
            new OnCompleteListener<Location>() {
                @Override
                public void onComplete(@NonNull Task<Location> task) {
                    Location location = task.getResult();
                    if (location == null) {
                        requestNewLocationData();
                    } else {
                        lat = ""+location.getLatitude();
                        lon = ""+location.getLongitude();
                        LatLng actual = new LatLng(Double.parseDouble(lat), Double.parseDouble(lon));
                        gMap.addMarker(new MarkerOptions().position(actual).title("Ubicacion Actual").draggable(true));
                        gMap.setBuildingsEnabled(false);
                        CameraPosition camara = new CameraPosition.Builder()
                            .target(actual)
                            .zoom(15)
                            .build();
                        gMap.moveCamera(CameraUpdateFactory.newCameraPosition(camara));
                    }
                }
            }
        );

//        if (checkPermissions()) {
//            if (isLocationEnabled()) {
//                mFusedLocationClient.getLastLocation().addOnCompleteListener(
//                        new OnCompleteListener<Location>() {
//                            @Override
//                            public void onComplete(@NonNull Task<Location> task) {
//                                Location location = task.getResult();
//                                if (location == null) {
//                                    requestNewLocationData();
//                                } else {
//                                    lat = ""+location.getLatitude();
//                                    lon = ""+location.getLongitude();
//
//                                    LatLng actual = new LatLng(Double.parseDouble(lat), Double.parseDouble(lon));
//                                    gMap.addMarker(new MarkerOptions().position(actual).title("Ubicacion Actual").draggable(true));
//                                    gMap.setBuildingsEnabled(false);
//                                    CameraPosition camara = new CameraPosition.Builder()
//                                            .target(actual)
//                                            .zoom(15)
//                                            .build();
//                                    gMap.moveCamera(CameraUpdateFactory.newCameraPosition(camara));
////                                Toast.makeText(mapaFragmento.this.getActivity(), "Ubicacion actual: \n" +
////                                        "Lat:" + location.getLatitude() + "\n" +
////                                       "Lon:"+ location.getLongitude(), Toast.LENGTH_SHORT).show();
//                                }
//                            }
//                        }
//                );
//            } else {
////                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
////                startActivity(intent);
//            }
//        } else {
//
//        }
    }
    @SuppressLint("MissingPermission")
    private void requestNewLocationData(){
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(0);
        mLocationRequest.setFastestInterval(0);
        mLocationRequest.setNumUpdates(1);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(mapaFragmento2.this.getActivity());
        mFusedLocationClient.requestLocationUpdates(
                mLocationRequest, mLocationCallback,
                Looper.myLooper()
        );
    }
    private LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            Location mLastLocation = locationResult.getLastLocation();
        }
    };

    @Override
    public void onResume(){
        super.onResume();
//        if (checkPermissions()) {
//            getLastLocation();
//        }

        Intent intent = getActivity().getIntent();
        FechaIntent = intent.getStringExtra("Fecha");
        HoraIntent = intent.getStringExtra("Hora");
        PeticionesIntent = intent.getStringExtra("Peticiones");

    }

    @SuppressLint("MissingPermission")
    public void searchPoint(LatLng ubi){
        lat = ""+ubi.latitude;
        lon = ""+ubi.longitude;
        LatLng actual = new LatLng(Double.parseDouble(lat), Double.parseDouble(lon));
        gMap.addMarker(new MarkerOptions().position(actual).title("Ubicacion Actual").draggable(true));
        CameraPosition camara = new CameraPosition.Builder()
                .target(actual)
                .zoom(15)
                .build();
        gMap.moveCamera(CameraUpdateFactory.newCameraPosition(camara));
    }

    public LatLng getLocationFromAddress(Context context,String strAddress) {
        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;
        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            if(address.size() > 0){
                Address location = address.get(0);
                p1 = new LatLng(location.getLatitude(), location.getLongitude() );
            } else {
                return null;
            }
        } catch (IOException ex) {

            ex.printStackTrace();
        }
        return p1;
    }
}

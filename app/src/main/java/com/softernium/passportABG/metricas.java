package com.softernium.passportABG;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.softernium.passportABG.utils.NukeSSLCerts;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class metricas extends AppCompatActivity {

    private PieChart pieChart, kilometrosChart;
    int diasTranscurridos, kilometrosUsados;
    int totalDias = 30, kilometrosTotales = 3000;
    private TextView conteo;
    private LinearLayout linearDias;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    private Boolean esKilometrosExedidos = false;
    private Boolean esDiasExedidos = false;
    private int maxKilometros = 3000, maxDias = 30;




    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new NukeSSLCerts().nuke(this);
        pref = this.getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        editor = pref.edit();
        setContentView(R.layout.activity_metricas);
        getSupportActionBar().setTitle("Detalle de recorrido");
        pieChart = findViewById(R.id.pieChartDias);
        kilometrosChart = findViewById(R.id.pieChartKilometros);
        conteo = findViewById(R.id.conteoCambios);
        linearDias = findViewById(R.id.linearDias);
        diasTranscurridos = 0;
        kilometrosUsados = 0;
        conteo.setText("0");
        // Tamaño en píxeles
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        //Toast.makeText(this, ""+height, Toast.LENGTH_SHORT).show();

        // dpi - dp
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int heightPixels = metrics.heightPixels;
        int widthPixels = metrics.widthPixels;
        int densityDpi = metrics.densityDpi;
        float xdpi = metrics.xdpi;
        float ydpi = metrics.ydpi;

        cargarGradicaDias();
        cargarGraficaKilometros();

        int alturaOcupable;
        if(heightPixels > 900){
            alturaOcupable = heightPixels - 240;
        } else {
            alturaOcupable = heightPixels - 210;
        }
        int alturaCard = alturaOcupable / 2;

        CardView.LayoutParams params = (CardView.LayoutParams) pieChart.getLayoutParams();
        params.height = alturaCard;
        pieChart.setLayoutParams(params);

        CardView.LayoutParams paramsK = (CardView.LayoutParams) kilometrosChart.getLayoutParams();
        paramsK.height = alturaCard;
        kilometrosChart.setLayoutParams(paramsK);

    }

    @Override
    protected void onResume() {
        super.onResume();
        obtenerMetricas("" + pref.getInt("nIDCliente",0));
    }

    private void cargarGraficaKilometros() {
        Description description = new Description();
        description.setText("Kilometraje.");
        description.setTextSize(14);
        description.setPosition(160,30);
        kilometrosChart.setDescription(description);

        ArrayList<PieEntry> pieEntry =  new ArrayList<>();
        if (esKilometrosExedidos){
            int resto = kilometrosTotales - kilometrosUsados;
            pieEntry.add(new PieEntry(resto,"Km Exedidos"));
            pieEntry.add(new PieEntry(kilometrosUsados,"Km Maximos"));
            PieDataSet pieSetData = new PieDataSet(pieEntry, "");
            pieSetData.setColors(new int[]{ColorTemplate.rgb("#000000"), ColorTemplate.rgb("#c8102e"), ColorTemplate.rgb("#9E9E9E")});
            pieSetData.setValueTextSize(12);
            pieSetData.setValueTextColor(ColorTemplate.rgb("#FFFFFF"));
            PieData pieData = new PieData(pieSetData);
            kilometrosChart.setData(pieData);
//            kilometrosChart.setEntryLabelColor(ColorTemplate.rgb("#9E9E9E"));
            kilometrosChart.setEntryLabelTextSize(10);
        } else {
            pieEntry.add(new PieEntry(kilometrosUsados,"Recorridos"));
            int resto = kilometrosTotales - kilometrosUsados;
            pieEntry.add(new PieEntry(resto,"Restantes"));
            PieDataSet pieSetData = new PieDataSet(pieEntry, "");
            pieSetData.setColors(new int[]{ColorTemplate.rgb("#c8102e"), ColorTemplate.rgb("#9E9E9E"), ColorTemplate.rgb("#9E9E9E")});
            pieSetData.setValueTextSize(12);
            pieSetData.setValueTextColor(ColorTemplate.rgb("#FFFFFF"));

            PieData pieData = new PieData(pieSetData);
            kilometrosChart.setData(pieData);
            kilometrosChart.setEntryLabelTextSize(10);
        }

        kilometrosChart.notifyDataSetChanged();
        kilometrosChart.invalidate();
    }

    private void cargarGradicaDias() {
        Description description = new Description();
        description.setText("Días de uso.");
        description.setTextSize(14);
        description.setPosition(160,30);
        pieChart.setDescription(description);
        ArrayList<PieEntry> pieEntry =  new ArrayList<>();

        if (esDiasExedidos){
            int resto = totalDias - diasTranscurridos;
            pieEntry.add(new PieEntry(resto,"Dias Exedidos"));
            pieEntry.add(new PieEntry(diasTranscurridos,"Dias Maximos"));
            PieDataSet pieSetData = new PieDataSet(pieEntry, "");
            pieSetData.setColors(new int[]{ColorTemplate.rgb("#000000"), ColorTemplate.rgb("#c8102e"), ColorTemplate.rgb("#9E9E9E")});
            pieSetData.setValueTextSize(12);
            pieSetData.setValueTextColor(ColorTemplate.rgb("#FFFFFF"));
            PieData pieData = new PieData(pieSetData);
            pieData.setValueFormatter(new IValueFormatter() {
                @Override
                public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                    return String.valueOf((int) value);
                }
            });
            pieChart.setData(pieData);
            pieChart.notifyDataSetChanged();
            pieChart.invalidate();
        } else {
            pieEntry.add(new PieEntry(diasTranscurridos,"Transurridos"));
            int resto = totalDias - diasTranscurridos;
            pieEntry.add(new PieEntry(resto,"Restantes"));
            PieDataSet pieSetData = new PieDataSet(pieEntry, "");
            pieSetData.setColors(new int[]{ColorTemplate.rgb("#c8102e"), ColorTemplate.rgb("#9E9E9E"), ColorTemplate.rgb("#9E9E9E")});
            pieSetData.setValueTextSize(12);
            pieSetData.setValueTextColor(ColorTemplate.rgb("#FFFFFF"));
            PieData pieData = new PieData(pieSetData);
            pieData.setValueFormatter(new IValueFormatter() {
                @Override
                public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                    return String.valueOf((int) value);
                }
            });
            pieChart.setData(pieData);
            pieChart.notifyDataSetChanged();
            pieChart.invalidate();
        }

    }

    private void obtenerMetricas(String id) {
        final ConstantesGlobales direcciones = new ConstantesGlobales();
        RequestQueue peticion = Volley.newRequestQueue(this);

        String url = direcciones.getApi() +""+ direcciones.getMetri();
        final String data = "{"+
                " \"nIDCliente\": \""+ id +"\"" +
                "}";
        final Map<String, String>[] datos = new Map[]{new HashMap<String, String>()};
        StringRequest stringRegquest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray json = new JSONArray( response );
                    JSONObject data = json.getJSONObject(0);
                    JSONArray calculoDias = data.getJSONArray("CalcularDias");
                    JSONObject dataDias = calculoDias.getJSONObject(0);
                    if(data.getBoolean("status")){
                        // Respuesta del kilometraje

                        diasTranscurridos = dataDias.getInt("DiasTranscurridos");
                        maxDias = dataDias.getInt("MaxDiasRenta");
                        if(diasTranscurridos > maxDias){
                            totalDias = diasTranscurridos;
                            diasTranscurridos = maxDias;
                            esDiasExedidos = true;
                        }
                        cargarGradicaDias();
                        kilometrosUsados = data.getInt("Kilometraje");
                        if (kilometrosUsados > maxKilometros){
                            kilometrosTotales = kilometrosUsados;
                            kilometrosUsados = maxKilometros;
                            esKilometrosExedidos = true;
                        }
                        cargarGraficaKilometros();
                        conteo.setText(data.getString("Total"));
                    } else {
                        // Error de kilometraje
                        Log.i("KilometrajeNo: ","" + response);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Error de respuesta","Error");
                error.printStackTrace();
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRegquest.setRetryPolicy(policy);
        peticion.add(stringRegquest);
    } // Fin de la peticion de Kilometraje

    private void alertDialog(String titulo, String msg, boolean dismiss) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(titulo);
        builder.setMessage(msg);

        builder.setPositiveButton("Aceptar", null);
        if ( dismiss){
            builder.setNegativeButton("Cancelar", null);
        }
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}

package com.softernium.passportABG;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.softernium.passportABG.utils.NukeSSLCerts;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import dmax.dialog.SpotsDialog;

public class formularioRecepcion extends AppCompatActivity {

    private ImageButton addFrente;
    private ImageButton addAtras;
    private ImageButton addDerecha;
    private ImageButton addIzquierda;
    private ImageButton addSuperior;
    private ImageButton addInteriorTablero;
    private ImageButton addInteriorAtras;
    private ImageButton addInteriorAdelante;
    private ImageButton addCajuela;
    private ImageButton addHerramientas;
    private ImageButton addLlantaDerechaTrasera;
    private ImageButton addLlantaDerechaDelantera;
    private ImageButton addLlantaIzquierdaTrasera;
    private ImageButton addLlantaIzquierdaDelantera;
    private ImageView Frente;
    private ImageView Atras;
    private ImageView Derecha;
    private ImageView Izquierda;
    private ImageView Superior;
    private ImageView InteriorTablero;
    private ImageView InteriorAdelante;
    private ImageView InteriorAtras;
    private ImageView Cajuela;
    private ImageView Herramientas;
    private ImageView LlantaDerechaTrasera;
    private ImageView LlantaDerechaDelantera;
    private ImageView LlantaIzquierdaTrasera;
    private ImageView LlantaIzquierdaDelantera;

    private Button enviarEvidencias;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private int GALLERY_REQUEST_CODE = 1;
    private static final int STORAGE_PERMISSION_CODE = 1;
    private static final int PERMISSION_CODE = 1000;
    private static final int IMAGE_CAPTURE_CODE = 1000;
    private  int indexControl = 0;
    private String[] base64Images = new String[14];
    private ConstantesGlobales util = new ConstantesGlobales();
    int controlUbicacion = 1;
    Uri image_uri;
    AlertDialog nDialog;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new NukeSSLCerts().nuke(this);
        setContentView(R.layout.activity_formulario_recepcion);
        getSupportActionBar().setTitle("Evidencias del vehiculo recivido.");
        base64Images[0] = ""; // Frente
        base64Images[1] = ""; // Atras
        base64Images[2] = ""; // Izquierda
        base64Images[3] = ""; // Derecha
        base64Images[4] = ""; // Superior
        base64Images[5] = ""; // Tablero
        base64Images[6] = ""; // Interiro adelante
        base64Images[7] = ""; // Interior atras
        base64Images[8] = ""; // Cajuela
        base64Images[9] = ""; // Herramientas
        base64Images[10] = ""; // Llanta Trasera Derecha
        base64Images[11] = ""; // Llanta Delantera Derecha
        base64Images[12] = ""; // Llanta Trasera Izquierda
        base64Images[13] = ""; // Llanta Delantera Izquierda

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int screenH = displayMetrics.heightPixels;
        int screenW = displayMetrics.widthPixels;
        final int sW = (screenW / 100) * 90;
        final int sH = (screenH / 100) * 60;

        pref = formularioRecepcion.this.getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        editor = pref.edit();

        enviarEvidencias = findViewById(R.id.enviarEvidenciasRecivido);
        enviarEvidencias.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enviarEvidenciasEntrega();
            }
        });

        addFrente                       = findViewById(R.id.imageBtnFrenteRecivido);
        addAtras                        = findViewById(R.id.imageBtnAtrasRecivido);
        addDerecha                      = findViewById(R.id.imageBtnDerechaRecivido);
        addIzquierda                    = findViewById(R.id.imageBtnIzquierdaRecivido);
        addSuperior                     = findViewById(R.id.imageBtnSuperiorRecivido);
        addInteriorAtras                = findViewById(R.id.imageBtnInteriorAtrasRecivido);
        addInteriorAdelante             = findViewById(R.id.imageBtnInteriorAdelanteRecivido);
        addInteriorTablero              = findViewById(R.id.imageBtnTableroRecivido);
        addCajuela                      = findViewById(R.id.imageBtnCajuelaRecivido);
        addHerramientas                 = findViewById(R.id.imageBtnHerramientasRecivido);
        addLlantaDerechaTrasera         = findViewById(R.id.imageBtnLlantaAtrasDerechaRecivido);
        addLlantaDerechaDelantera       = findViewById(R.id.imageBtnLlantaAdelanteDerechaRecivido);
        addLlantaIzquierdaDelantera     = findViewById(R.id.imageBtnLlantaAdelanteIzquierdaRecivido);
        addLlantaIzquierdaTrasera       = findViewById(R.id.imageBtnLlantaAtrasIzquierdaRecivido);

        addFrente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                indexControl = 0;
                selectFromWherePickUp();
            }
        });
        addAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                indexControl = 1;
                selectFromWherePickUp();
            }
        });
        addIzquierda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                indexControl = 2;
                selectFromWherePickUp();
            }
        });
        addDerecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                indexControl = 3;
                selectFromWherePickUp();
            }
        });
        addSuperior.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                indexControl = 4;
                selectFromWherePickUp();
            }
        });
        addInteriorTablero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                indexControl = 5;
                selectFromWherePickUp();
            }
        });
        addInteriorAdelante.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                indexControl = 6;
                selectFromWherePickUp();
            }
        });
        addInteriorAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                indexControl = 7;
                selectFromWherePickUp();
            }
        });
        addCajuela.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                indexControl = 8;
                selectFromWherePickUp();
            }
        });
        addHerramientas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                indexControl = 9;
                selectFromWherePickUp();
            }
        });
        addLlantaDerechaTrasera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                indexControl = 10;
                selectFromWherePickUp();
            }
        });
        addLlantaDerechaDelantera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                indexControl = 11;
                selectFromWherePickUp();
            }
        });
        addLlantaIzquierdaTrasera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                indexControl = 12;
                selectFromWherePickUp();
            }
        });
        addLlantaIzquierdaDelantera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                indexControl = 13;
                selectFromWherePickUp();
            }
        });

        Frente = findViewById(R.id.imageFrenteRecivido);
        Frente.setClickable(true);
        Atras = findViewById(R.id.imageAtrasRecivido);
        Atras.setClickable(true);
        Derecha = findViewById(R.id.imageDerechaRecivido);
        Derecha.setClickable(true);
        Izquierda = findViewById(R.id.imageIzquierdaRecivido);
        Izquierda.setClickable(true);
        Superior = findViewById(R.id.imageSuperiorRecivido);
        Superior.setClickable(true);
        InteriorTablero = findViewById(R.id.imageTableroRecivido);
        InteriorTablero.setClickable(true);
        InteriorAdelante = findViewById(R.id.imageInteriorAdelanteRecivido);
        InteriorAdelante.setClickable(true);
        InteriorAtras = findViewById(R.id.imageInteriorAtrasRecivido);
        InteriorAtras.setClickable(true);
        Cajuela = findViewById(R.id.imageCajuelaRecivido);
        Cajuela.setClickable(true);
        Herramientas = findViewById(R.id.imageHerramientasRecivido);
        Herramientas.setClickable(true);
        LlantaDerechaTrasera = findViewById(R.id.imageLlantaAtrasDerechaRecivido);
        LlantaDerechaTrasera.setClickable(true);
        LlantaDerechaDelantera = findViewById(R.id.imageLlantaAdelanteDerechaRecivido);
        LlantaDerechaDelantera.setClickable(true);
        LlantaIzquierdaTrasera = findViewById(R.id.imageLlantaAtrasIzquierdaRecivido);
        LlantaIzquierdaTrasera.setClickable(true);
        LlantaIzquierdaDelantera = findViewById(R.id.imageLlantaAdelanteIzquierdaRecivido);
        LlantaIzquierdaDelantera.setClickable(true);

        Frente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageView image = new ImageView(getApplicationContext());
                String msg = "";
                if (base64Images[0] == ""){
                    msg = "Sin foto";
                } else {
                    image.setImageBitmap(util.stringtoScaleImageByPercent(base64Images[0],20));
                    mostrarImagenEmergente(image, sW, sH);
                }

            }
        });
        Atras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageView image = new ImageView(getApplicationContext());
                String msg = "";
                if (base64Images[1] == ""){
                    msg = "Sin foto";
                } else {
                    image.setImageBitmap(util.stringtoScaleImageByPercent(base64Images[1],20));
                    mostrarImagenEmergente(image, sW, sH);
                }

            }
        });
        Izquierda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageView image = new ImageView(getApplicationContext());
                String msg = "";
                if (base64Images[2] == ""){
                    msg = "Sin foto";
                } else {
                    image.setImageBitmap(util.stringtoScaleImageByPercent(base64Images[2],20));
                    mostrarImagenEmergente(image, sW, sH);
                }

            }
        });
        Derecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageView image = new ImageView(getApplicationContext());
                String msg = "";
                if (base64Images[3] == ""){
                    msg = "Sin foto";
                } else {
                    image.setImageBitmap(util.stringtoScaleImageByPercent(base64Images[3],20));
                    mostrarImagenEmergente(image, sW, sH);
                }

            }
        });
        Superior.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageView image = new ImageView(getApplicationContext());
                String msg = "";
                if (base64Images[4] == ""){
                    msg = "Sin foto";
                } else {
                    image.setImageBitmap(util.stringtoScaleImageByPercent(base64Images[4],20));
                    mostrarImagenEmergente(image, sW, sH);
                }

            }
        });
        InteriorTablero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageView image = new ImageView(getApplicationContext());
                String msg = "";
                if (base64Images[5] == ""){
                    msg = "Sin foto";
                } else {
                    image.setImageBitmap(util.stringtoScaleImageByPercent(base64Images[5],20));
                    mostrarImagenEmergente(image, sW, sH);
                }

            }
        });
        InteriorAdelante.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageView image = new ImageView(getApplicationContext());
                String msg = "";
                if (base64Images[6] == ""){
                    msg = "Sin foto";
                } else {
                    image.setImageBitmap(util.stringtoScaleImageByPercent(base64Images[6],20));
                    mostrarImagenEmergente(image, sW, sH);
                }

            }
        });
        InteriorAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageView image = new ImageView(getApplicationContext());
                String msg = "";
                if (base64Images[7] == ""){
                    msg = "Sin foto";
                } else {
                    image.setImageBitmap(util.stringtoScaleImageByPercent(base64Images[7],20));
                    mostrarImagenEmergente(image, sW, sH);
                }

            }
        });
        Cajuela.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageView image = new ImageView(getApplicationContext());
                String msg = "";
                if (base64Images[8] == ""){
                    msg = "Sin foto";
                } else {
                    image.setImageBitmap(util.stringtoScaleImageByPercent(base64Images[8],20));
                    mostrarImagenEmergente(image, sW, sH);
                }

            }
        });
        Herramientas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageView image = new ImageView(getApplicationContext());
                String msg = "";
                if (base64Images[9] == ""){
                    msg = "Sin foto";
                } else {
                    image.setImageBitmap(util.stringtoScaleImageByPercent(base64Images[9],20));
                    mostrarImagenEmergente(image, sW, sH);
                }

            }
        });
        LlantaDerechaTrasera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageView image = new ImageView(getApplicationContext());
                String msg = "";
                if (base64Images[10] == ""){
                    msg = "Sin foto";
                } else {
                    image.setImageBitmap(util.stringtoScaleImageByPercent(base64Images[10],20));
                    mostrarImagenEmergente(image, sW, sH);
                }

            }
        });
        LlantaDerechaDelantera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageView image = new ImageView(getApplicationContext());
                String msg = "";
                if (base64Images[11] == ""){
                    msg = "Sin foto";
                } else {
                    image.setImageBitmap(util.stringtoScaleImageByPercent(base64Images[11],20));
                    mostrarImagenEmergente(image, sW, sH);
                }

            }
        });
        LlantaIzquierdaTrasera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageView image = new ImageView(getApplicationContext());
                String msg = "";
                if (base64Images[12] == ""){
                    msg = "Sin foto";
                } else {
                    image.setImageBitmap(util.stringtoScaleImageByPercent(base64Images[12],20));
                    mostrarImagenEmergente(image, sW, sH);
                }

            }
        });
        LlantaIzquierdaDelantera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageView image = new ImageView(getApplicationContext());
                String msg = "";
                if (base64Images[13] == ""){
                    msg = "Sin foto";
                } else {
                    image.setImageBitmap(util.stringtoScaleImageByPercent(base64Images[13],20));
                    mostrarImagenEmergente(image, sW, sH);
                }

            }
        });

    }

    private void cargarImagen(ImageView iView){
        if(image_uri != null){
            iView.setImageURI(image_uri);
            base64Images[indexControl] = util.imageToStringQuality(iView, 20, Bitmap.CompressFormat.JPEG);
            iView.setImageBitmap(util.stringtoScaleImage(base64Images[indexControl], 0, 0));
        }
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Result code is RESULT_OK only if the user selects an Image
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if(controlUbicacion == 0){
                //En caso de tomar la fotorgafia desde la galeria del telefono, Actualmente deshabilitada por descision del cliente
                switch (requestCode) {
                    case 1:
                        //data.getData return the content URI for the selected Image
                        Uri selectedImage = data.getData();
                        String[] filePathColumn = {MediaStore.Images.Media.DATA};
                        // Get the cursor
                        Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                        // Move to first row
                        cursor.moveToFirst();
                        //Get the column index of MediaStore.Images.Media.DATA
                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        //Gets the String value in the column
                        String imgDecodableString = cursor.getString(columnIndex);
                        // Set the Image in ImageView after decoding the String
                        switch (indexControl) {
                            case 0:
                                Frente.setImageBitmap(BitmapFactory.decodeFile(imgDecodableString));
                                base64Images[indexControl] = util.imageToString(Frente);
                                Frente.setImageBitmap(util.stringtoScaleImage(base64Images[indexControl], 0, 0));
                                //base64Images[indexControl] = util.imageToString(Frente);
                                break;
                            case 1:
                                Atras.setImageBitmap(BitmapFactory.decodeFile(imgDecodableString));
                                base64Images[indexControl] = util.imageToString(Atras);
                                Atras.setImageBitmap(util.stringtoScaleImage(base64Images[indexControl], 0, 0));
                                //base64Images[indexControl] = util.imageToString(Atras);
                                break;
                            case 2:
                                Izquierda.setImageBitmap(BitmapFactory.decodeFile(imgDecodableString));
                                base64Images[indexControl] = util.imageToString(Izquierda);
                                Izquierda.setImageBitmap(util.stringtoScaleImage(base64Images[indexControl], 0, 0));
                                //base64Images[indexControl] = util.imageToString(Izquierda);
                                break;
                            case 3:
                                Derecha.setImageBitmap(BitmapFactory.decodeFile(imgDecodableString));
                                base64Images[indexControl] = util.imageToString(Derecha);
                                Derecha.setImageBitmap(util.stringtoScaleImage(base64Images[indexControl], 0, 0));
                                //base64Images[indexControl] = util.imageToString(Derecha);
                                break;
                            case 4:
                                Superior.setImageBitmap(BitmapFactory.decodeFile(imgDecodableString));
                                base64Images[indexControl] = util.imageToString(Superior);
                                Superior.setImageBitmap(util.stringtoScaleImage(base64Images[indexControl], 0, 0));
                                //base64Images[indexControl] = util.imageToString(Superior);
                                break;
                            case 5:
                                InteriorTablero.setImageBitmap(BitmapFactory.decodeFile(imgDecodableString));
                                base64Images[indexControl] = util.imageToString(InteriorTablero);
                                InteriorTablero.setImageBitmap(util.stringtoScaleImage(base64Images[indexControl], 0, 0));
                                //base64Images[indexControl] = util.imageToString(InteriorTablero);
                                break;
                            case 6:
                                InteriorAdelante.setImageBitmap(BitmapFactory.decodeFile(imgDecodableString));
                                base64Images[indexControl] = util.imageToString(InteriorAdelante);
                                InteriorAdelante.setImageBitmap(util.stringtoScaleImage(base64Images[indexControl], 0, 0));
                                //base64Images[indexControl] = util.imageToString(InteriorAdelante);
                                break;
                            case 7:
                                InteriorAtras.setImageBitmap(BitmapFactory.decodeFile(imgDecodableString));
                                base64Images[indexControl] = util.imageToString(InteriorAtras);
                                InteriorAtras.setImageBitmap(util.stringtoScaleImage(base64Images[indexControl], 0, 0));
                                //base64Images[indexControl] = util.imageToString(InteriorAtras);
                                break;
                            case 8:
                                Cajuela.setImageBitmap(BitmapFactory.decodeFile(imgDecodableString));
                                base64Images[indexControl] = util.imageToString(Cajuela);
                                Cajuela.setImageBitmap(util.stringtoScaleImage(base64Images[indexControl], 0, 0));
                                //base64Images[indexControl] = util.imageToString(InteriorAdelante);
                                break;
                            case 9:
                                Herramientas.setImageBitmap(BitmapFactory.decodeFile(imgDecodableString));
                                base64Images[indexControl] = util.imageToString(Herramientas);
                                Herramientas.setImageBitmap(util.stringtoScaleImage(base64Images[indexControl], 0, 0));
                                //base64Images[indexControl] = util.imageToString(InteriorAtras);
                                break;
                        }
                        cursor.close();
                        break;
                    default:
                        Toast.makeText(getApplicationContext(), "Archivo dañado!.", Toast.LENGTH_LONG).show();
                        break;
                }
            }else{
                final int QUALITY = 15;
                switch (indexControl) {
                    // En caso de tomar la foto con la camara.
                    case 0:
                        cargarImagen(Frente);
                        break;
                    case 1:
                        cargarImagen(Atras);
                        break;
                    case 2:
                        cargarImagen(Izquierda);
                        break;
                    case 3:
                        cargarImagen(Derecha);
                        break;
                    case 4:
                        cargarImagen(Superior);
                        break;
                    case 5:
                        cargarImagen(InteriorTablero);
                        break;
                    case 6:
                        cargarImagen(InteriorAdelante);
                        break;
                    case 7:
                        cargarImagen(InteriorAtras);
                        break;
                    case 8:
                        cargarImagen(Cajuela);
                        break;
                    case 9:
                        cargarImagen(Herramientas);
                        break;
                    case 10:
                        cargarImagen(LlantaDerechaTrasera);
                        break;
                    case 11:
                        cargarImagen(LlantaDerechaDelantera);
                        break;
                    case 12:
                        cargarImagen(LlantaIzquierdaTrasera);
                        break;
                    case 13:
                        cargarImagen(LlantaIzquierdaDelantera);
                        break;

                }
            }//Fin del else
        }// Fin del result ok
    }// Fin on activity result

    @Override
    protected void onResume() {
        super.onResume();
        checkPermissions();
        permisosCamara();
    }

    public void permisosCamara(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if(checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED ||
                    checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED){
                String[] request = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
                requestPermissions(request, PERMISSION_CODE);
            } else {
                //pickFromCamara();
            }
        } else {
            //pickFromCamara();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == STORAGE_PERMISSION_CODE)  {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Permission GRANTED", Toast.LENGTH_SHORT).show();
                if(controlUbicacion == 0){
                    //pickFromGallery();
                } else {
                    //pickFromCamara();
                }
            } else {
                Toast.makeText(this, "Permission DENIED", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void checkPermissions(){
        if (ContextCompat.checkSelfPermission(formularioRecepcion.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            //Toast.makeText(formularioEntrega.this, "You have already granted this permission!",Toast.LENGTH_SHORT).show();
        } else requestStoragePermission();
    }

    private void requestStoragePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            new AlertDialog.Builder(this)
                    .setTitle("Permiso necesario")
                    .setMessage("Este permiso es requerido para acceder a los archivos miltimedia y a la camara del dispositivo.")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(formularioRecepcion.this,
                                    new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
                        }
                    })
                    .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .create().show();
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
        }
    }

    public void selectFromWherePickUp(){
        controlUbicacion = 1;
        pickFromCamara();
        //alertDialogImage("Ubicacion de archivo","Selecciona la ubicacion del archivo.");
    }

    private void pickFromCamara() {
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "Nueva Foto");
        values.put(MediaStore.Images.Media.DESCRIPTION, "Evidencias de recepcion");
        image_uri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        camera.putExtra(MediaStore.EXTRA_OUTPUT, image_uri);
        startActivityForResult(camera, IMAGE_CAPTURE_CODE);
    }

    private void mostrarImagenEmergente(ImageView image, int sW, int sH){
        AlertDialog.Builder builder =
                new AlertDialog.Builder(this).
                        setMessage("").
                        setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).setView(image);
        builder.create().getWindow().setLayout(sW,sH);
        builder.create().show();
    }

    public Boolean validarDatos(){
        // Validar fotografias
        if ("".equals(base64Images[0].toString())) {
            alertDialog("Campos faltantes","Ingrese la fotografia del Frente",false);
            return false;
        }
        if ("".equals(base64Images[1].toString())) {
            alertDialog("Campos faltantes","Ingrese la fotografia del Atras",false);
            return false;
        }
        if ("".equals(base64Images[2].toString())) {
            alertDialog("Campos faltantes","Ingrese la fotografia del Izquierda",false);
            return false;
        }
        if ("".equals(base64Images[3].toString())) {
            alertDialog("Campos faltantes","Ingrese la fotografia del Derecha",false);
            return false;
        }
        if ("".equals(base64Images[4].toString())) {
            alertDialog("Campos faltantes","Ingrese la fotografia del Superior",false);
            return false;
        }
        if ("".equals(base64Images[5].toString())) {
            alertDialog("Campos faltantes","Ingrese la fotografia del Tablero",false);
            return false;
        }
        if ("".equals(base64Images[6].toString())) {
            alertDialog("Campos faltantes","Ingrese la fotografia del Interior adelante",false);
            return false;
        }
        if ("".equals(base64Images[7].toString())) {
            alertDialog("Campos faltantes","Ingrese la fotografia del Interior atras",false);
            return false;
        }
        if ("".equals(base64Images[8].toString())) {
            alertDialog("Campos faltantes","Ingrese la fotografia del Cajuela",false);
            return false;
        }
        if ("".equals(base64Images[9].toString())) {
            alertDialog("Campos faltantes","Ingrese la fotografia del Herramientas",false);
            return false;
        }
        if ("".equals(base64Images[10].toString())) {
            alertDialog("Campos faltantes","Ingrese la fotografia del Llanta derecha trasera",false);
            return false;
        }
        if ("".equals(base64Images[11].toString())) {
            alertDialog("Campos faltantes","Ingrese la fotografia del Llanta derecha delantera",false);
            return false;
        }
        if ("".equals(base64Images[12].toString())) {
            alertDialog("Campos faltantes","Ingrese la fotografia del Llanta izquierda trasera",false);
            return false;
        }
        if ("".equals(base64Images[13].toString())) {
            alertDialog("Campos faltantes","Ingrese la fotografia del Llanta izquierda delantera",false);
            return false;
        }
        return true;
    }

    private void alertDialog(String titulo, String msg, boolean dismiss) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(titulo);
        builder.setMessage(msg);
        builder.setPositiveButton("Aceptar", null);
        if ( dismiss){
            builder.setNegativeButton("Cancelar", null);
        }
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void enviarEvidenciasEntrega() {
        if(validarDatos()){
            nDialog = new SpotsDialog.Builder().setContext(this).setMessage("Enviando evidencias.").setTheme(R.style.Custom).build();
            nDialog.show();
            final ConstantesGlobales direcciones = new ConstantesGlobales();
            RequestQueue peticion = Volley.newRequestQueue(this);
            String url = direcciones.getApi() +""+ direcciones.getCho();
            final String data = "{"+
                    " \"idChofer\": \""+pref.getInt("nIDCliente",0)+"\", " +
                    " \"idSolicitud\": \""+pref.getInt("nIDSolicitud",0)+"\", " +
                    " \"nombreCliente\": \""+pref.getString("nombreCliente","Cliente")+"\", " +
                    " \"nIDVehiculo\": \""+pref.getString("idVehiculoEntrega","0")+"\", " +

                    " \"fotoFrente\": \""+base64Images[0]+"\", " +
                    " \"fotoAtras\": \""+base64Images[1]+"\", " +
                    " \"fotoIzquierda\": \""+base64Images[2]+"\", " +
                    " \"fotoDerecha\": \""+base64Images[3]+"\", " +
                    " \"fotoSuperior\": \""+base64Images[4]+"\", " +
                    " \"fotoTablero\": \""+base64Images[5]+"\", " +
                    " \"fotoInteriorAdelante\": \""+base64Images[6]+"\", " +
                    " \"fotoInteriorAtras\": \""+base64Images[7]+"\", " +
                    " \"fotoCajuela\": \""+base64Images[8]+"\", " +
                    " \"fotoHerramientas\": \""+base64Images[9]+"\", " +
                    " \"fotoLlantaTraseraDerecha\": \""+base64Images[10]+"\", " +
                    " \"fotoLlantaDelanteraDerecha\": \""+base64Images[11]+"\", " +
                    " \"fotoLlantaTraseraIzquierda\": \""+base64Images[12]+"\", " +
                    " \"fotoLlantaDelanteraIzquierda\": \""+base64Images[13]+"\", " +
                    " \"option\": \"6\" " +
                    "}";
            StringRequest stringRegquest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        nDialog.dismiss();
                        JSONObject json = new JSONObject( response );
                        if(json.getBoolean("status")){

                            Intent intent = new Intent(getApplicationContext(), mainChofer.class);
                            startActivity(intent);

                        } else {
                            Toast.makeText(getApplicationContext(), "No se pudo completar la operacion", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        nDialog.dismiss();
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    nDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Error de conexion.", Toast.LENGTH_SHORT).show();
                    Log.i("Error de respuesta","Error:"+error.toString());
                    error.printStackTrace();
                }
            }){
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=UTF-8";
                }
                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        //alertDialog("","data: "+data.getBytes("UTF-8"),false);
                        return data == null ? null : data.getBytes("UTF-8");

                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                        return null;
                    }
                }
            };
            int socketTimeout = 30000;//30 seconds - change to what you want
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            stringRegquest.setRetryPolicy(policy);
            peticion.add(stringRegquest);
        }
    }


}
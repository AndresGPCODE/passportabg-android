package com.softernium.passportABG;


import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.softernium.passportABG.utils.NukeSSLCerts;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import dmax.dialog.SpotsDialog;


/**
 * A simple {@link Fragment} subclass.
 */
public class choferMapa extends Fragment  {
    private View rootView;
    private GoogleMap gMap;
    private MapView mapView;
    private Button entregarBtn;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    AlertDialog nDialog;
    private ImageButton chatBtnChofer;
    private boolean control = true;

    public choferMapa() {
        // Required empty public constructor
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new NukeSSLCerts().nuke(this.getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_chofer_mapa, container, false);
        return  rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        entregarBtn = getView().findViewById(R.id.btnEntregaChofer);
        chatBtnChofer = getActivity().findViewById(R.id.chatBtnChofer);
        pref = this.getActivity().getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        editor = pref.edit();
        Integer idSolicitud = pref.getInt("nIDSolicitud",0);
        String Estatus = pref.getString("Estatus","NA");
        if(idSolicitud != 0 && Estatus.equals("EN RUTA")){
            entregarBtn.setEnabled(true);
            entregarBtn.setVisibility(View.VISIBLE);
            entregarBtn.setText("Solicitar recepción");
            chatBtnChofer.setEnabled(true);
            chatBtnChofer.setVisibility(View.VISIBLE);
            chatBtnChofer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(choferMapa.this.getActivity(), chatChofer.class);
                    startActivity(intent);
                }
            });
        } else {
            entregarBtn.setEnabled(false);
            entregarBtn.setText("Inhabilitado");
            entregarBtn.setVisibility(View.GONE);
            chatBtnChofer.setEnabled(false);
            chatBtnChofer.setVisibility(View.GONE);
            chatBtnChofer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(choferMapa.this.getActivity(), "No tienes cliente asignado para entrega", Toast.LENGTH_SHORT).show();
                }
            });
        }

        if(Estatus.equals("POR ACEPTAR") || Estatus.equals("ACEPTADO")){
            nDialog = new SpotsDialog.Builder().setContext(choferMapa.this.getContext()).setMessage("Esperando validacion.").setTheme(R.style.Custom).build();
            nDialog.show();
            validarRecepcion();
        }


        entregarBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nDialog = new SpotsDialog.Builder().setContext(choferMapa.this.getContext()).setMessage("Esperando validacion.").setTheme(R.style.Custom).build();
                nDialog.show();
                solicitarRecepcion();
            }
        });
        //Esta linea reemplaza el mapa del fragmento de mapra_fragmento_chofer poniendolo aqui
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_mapa, new mapaFragmentoChoferNuevo()).commit();
        //getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_mapa, new mapaSolicitudNuevo()).commit();
    }

    @Override
    public void onStop() {
        super.onStop();
        control = false;
    }

    @Override
    public void onPause() {
        super.onPause();
        control = false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        control = false;
    }


    private void solicitarRecepcion() {
        control = true;
        final ConstantesGlobales direcciones = new ConstantesGlobales();
        RequestQueue peticion = Volley.newRequestQueue(choferMapa.this.getContext());
        String url = direcciones.getApi() +""+ direcciones.getCho();
        final String data = "{"+
            " \"idChofer\": \""+pref.getInt("nIDCliente",0)+"\", " +
            " \"nIDSolicitud\": \""+pref.getInt("nIDSolicitud",0)+"\", " +
            " \"nIDCliente\": \""+pref.getString("nIDClienteEntrega","0")+"\", " +
            " \"option\": 3" +
            "}";
        final Map<String, String>[] datos = new Map[]{new HashMap<String, String>()};
        StringRequest stringRegquest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject( response );
                    if(json.getBoolean("status")){
                        entregarBtn.setEnabled(false);
                        Toast.makeText(choferMapa.this.getContext(), "Recepcion solicitada.", Toast.LENGTH_LONG).show();
                        timeOut(4000);
                    } else {
                        entregarBtn.setEnabled(true);
                        Toast.makeText(choferMapa.this.getContext(),"No se pudo realizar la peticion.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Error de respuesta","Error");
                error.printStackTrace();
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRegquest.setRetryPolicy(policy);
        peticion.add(stringRegquest);
    }

    public void timeOut(int timeInterval){
        Log.i("Interval","Intervlo establecido");
        new android.os.Handler().postDelayed(
            new Runnable() {
                public void run() {
                    if(control){
                        validarRecepcion();
                    }
                }
            }, timeInterval);
    }

    private void validarRecepcion() {
        Toast.makeText(choferMapa.this.getContext(), "Validando.", Toast.LENGTH_LONG).show();
        final ConstantesGlobales direcciones = new ConstantesGlobales();
        RequestQueue peticion = Volley.newRequestQueue(choferMapa.this.getContext());
        String url = direcciones.getApi() +""+ direcciones.getCho();
        final String data = "{" +
                " \"idChofer\": \""+pref.getInt("nIDCliente",0)+"\", " +
                " \"nIDSolicitud\": \""+pref.getInt("nIDSolicitud",0)+"\", " +
                " \"option\": 4" +
                "}";
        final Map<String, String>[] datos = new Map[]{new HashMap<String, String>()};
        StringRequest stringRegquest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject( response );
                    if(!json.getBoolean("status")){
                        entregarBtn.setEnabled(false);
                        timeOut(4000);
                    } else {
                        nDialog.dismiss();
                        Intent intent = new Intent(choferMapa.this.getActivity(), formularioEntrega.class);
                        startActivity(intent);
                        entregarBtn.setEnabled(true);
                        entregarBtn.setText("Entregar");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Error de respuesta","Error");
                error.printStackTrace();
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRegquest.setRetryPolicy(policy);
        peticion.add(stringRegquest);
    }
    private void alertDialog(String titulo, String msg, boolean dismiss) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(titulo);
        builder.setMessage(msg);

        builder.setPositiveButton("Aceptar", null);
        if ( dismiss){
            builder.setNegativeButton("Cancelar", null);
        }
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}

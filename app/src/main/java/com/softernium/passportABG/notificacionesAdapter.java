package com.softernium.passportABG;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class notificacionesAdapter extends BaseAdapter {

    private Context context;
    private int layout;
    private JSONArray list;

    public notificacionesAdapter(Context context, int layout, JSONArray list){
        this.context = context;
        this.layout = layout;
        this.list = list;
    }

    @Override
    public int getCount() {
        return this.list.length();
    }

    @Override
    public Object getItem(int i) {
        try {
            return this.list.getJSONObject(i);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        LayoutInflater inflate = LayoutInflater.from(this.context);
        v = inflate.inflate(R.layout.notificaciones_adapter, null, false);

        JSONObject currentItem = (JSONObject) getItem(i);

        TextView notificacion = (TextView) v.findViewById(R.id.textNotificacion);
        LinearLayout back = v.findViewById(R.id.layoutNotificacion);

        try {

            //Toast.makeText(context, currentItem.getString("Estatus"), Toast.LENGTH_SHORT).show();

            if(currentItem.getString("Estatus").equals("LEIDO")){
                //back.setBackgroundColor(Color.parseColor("#c8102e"));
                back.setBackgroundResource(R.drawable.notificacion_leida);
                notificacion.setTextColor(Color.parseColor("#000000"));
            } else{
                back.setBackgroundResource(R.drawable.notificacion_noleida);
                notificacion.setTextColor(Color.parseColor("#FFFFFF"));
            }
            notificacion.setText(currentItem.getString("Encabezado"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return v;
    }
}

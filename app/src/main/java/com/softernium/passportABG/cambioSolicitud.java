package com.softernium.passportABG;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.softernium.passportABG.utils.NukeSSLCerts;

public class cambioSolicitud extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new NukeSSLCerts().nuke(this);

        setContentView(R.layout.activity_cambio_solicitud);
        getSupportActionBar().setTitle("Selecciona ubicacion");
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_mapa_cambio, new mapaFragmento2()).commit();
    }
}

package com.softernium.passportABG;
import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.softernium.passportABG.utils.NukeSSLCerts;

import org.json.JSONException;
import org.json.JSONObject;
import java.io.UnsupportedEncodingException;

public class ReceiveSms extends BroadcastReceiver {

    private static final String CHANNEL_ID = "NOTIFICACION";
    private static final int NOTIFICATION_ID = 4;
    private RequestQueue requestQueue;
    private Context context;
    public static final String SMS_EXTRA_NAME = "pdus";
    public static final String SMS_URI = "content://sms";
    public static final String ADDRESS = "address";
    public static final String PERSON = "person";
    public static final String DATE = "date";
    public static final String READ = "read";
    public static final String STATUS = "status";
    public static final String TYPE = "type";
    public static final String BODY = "body";
    public static final String SEEN = "seen";
    public static final int MESSAGE_TYPE_INBOX = 1;
    public static final int MESSAGE_TYPE_SENT = 2;
    public static final int MESSAGE_IS_NOT_READ = 0;
    public static final int MESSAGE_IS_READ = 1;
    public static final int MESSAGE_IS_NOT_SEEN = 0;
    public static final int MESSAGE_IS_SEEN = 1;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    ConstantesGlobales api = new ConstantesGlobales();

    @Override
    public void onReceive(Context context, Intent intent) {
        new NukeSSLCerts().nuke(context);

        pref = context.getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        editor = pref.edit();
        String str ="Base";
        this.context = context;
//        if( intent.getAction().equals( "android.provider.Telephony.SMS_RECIEVE" ) ) {
            if( intent.getAction().equals( "android.provider.Telephony.SMS_DELIVER" ) ) {
            Bundle bundle = intent.getExtras();
            SmsMessage[] msgs = null;
            String msg_from;
            if( bundle != null ) {
                try {
                    Object[] pdus = ( Object[] ) bundle.get( "pdus" );
                    msgs          = new SmsMessage[pdus.length];
                    for (int i = 0; i < msgs.length; i++ ) {
                        msgs[i] = SmsMessage.createFromPdu( (byte[])pdus[i] );
                        SmsMessage sms = SmsMessage.createFromPdu((byte[])pdus[i]);
                        ContentResolver contentResolver = context.getContentResolver();
                        msg_from       = msgs[i].getOriginatingAddress();
                        String msgBody = msgs[i].getMessageBody();
                        if ( msgBody.indexOf( "hdop" ) > -1 ) {
                            //Toast.makeText( context, "Recibiste un SMS con ubicación"+msgBody, Toast.LENGTH_LONG).show();
                            Log.i("msgBody",""+msgBody);
                            String datos = "{ \"nIDHistorial\": 0," +
                                    " \"nIDDispositivoAuto\": "+pref.getString("nIDDispositivoAuto","0")+"," +
                                    "\"nIDSolicitud\": 0," +
                                    " \"Link\": \"" + "Link" + "\"," +
                                    " \"Respuesta\": \"" + msgBody +"\"," +
                                    " \"Latitud\": 0," +
                                    " \"longitud\": 0," +
                                    " \"Velocidad\": 0," +
                                    " \"Hora\": \"0\"," +
                                    " \"Fecha\": \"0\" }";
                            str = msgBody;
                            //createNotificacion("SMS - "+msg_from,""+msgBody);
                            submit( datos );
                        } else {
                            createNotificacion("SMS - "+msg_from,""+msgBody);
                            //Solo guardar el sms si no es respuesta del gps
                            putSmsToDatabase(contentResolver, sms);
                            //Log.i("msgBody","Nada");
                            //Toast.makeText( context, "Recibiste un SMS sin ubicación", Toast.LENGTH_SHORT ).show();
                        }
                    }
                } catch ( Exception e ) {
                    e.printStackTrace();
                }
            }
        }

    }// Fin del onReceive()
    private void submit( String data ){
        final String savedata = data;
        String URL = api.getUrl() + api.getContr() +"Historial.php?accion=agregar";
        requestQueue = Volley.newRequestQueue( context.getApplicationContext() );
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject objres = new JSONObject(response);
                    //Toast.makeText( context.getApplicationContext(), objres.toString(), Toast.LENGTH_LONG ).show();
                } catch (JSONException e) {
                    //Toast.makeText( context.getApplicationContext(), "Ubicación enviada a BD", Toast.LENGTH_LONG ).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText( context.getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT ).show();
            }
        }) {
            @Override
            public String getBodyContentType() { return "application/json; charset=utf-8"; }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return savedata == null ? null : savedata.getBytes("utf-8" );
                } catch ( UnsupportedEncodingException uee){
                    return null;
                }
            }
        };
        requestQueue.add( stringRequest );
    }
    private void alertMsg(String titulo, String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(titulo);
        builder.setMessage(msg);
        builder.setPositiveButton("Aceptar", null);
        //builder.setNegativeButton("Contrato", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    private void putSmsToDatabase(ContentResolver contentResolver, SmsMessage sms ) {
        // Create SMS row
        //Log.i("SMS: ",""+sms);
        ContentValues values = new ContentValues();
        values.put( ADDRESS, sms.getOriginatingAddress() );
        values.put( DATE, sms.getTimestampMillis() );
        values.put( READ, MESSAGE_IS_NOT_READ );
        values.put( STATUS, sms.getStatus() );
        values.put( TYPE, MESSAGE_TYPE_INBOX );
        values.put( SEEN, MESSAGE_IS_NOT_SEEN );
        values.put( BODY, sms.getMessageBody().toString() );
        // Push row into the SMS table
        contentResolver.insert( Uri.parse( "content://sms/inbox" ), values );
    }
    private void createNotificacion( String tittle, String body) {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            CharSequence name = "NOTIFICACION";
            NotificationChannel chanel = new NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManager manager = (NotificationManager) this.context.getSystemService(Context.NOTIFICATION_SERVICE);
            manager.createNotificationChannel(chanel);
        }
        NotificationCompat.Builder thisNotifi = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.abg_sms)
                .setContentTitle(tittle)
                .setContentText(body)
                //.setColor(Color.rgb(200,16,46))
                .setLights(Color.WHITE, 1000, 1000)
                //.setDefaults(Notification.DEFAULT_SOUND)
                .setVibrate(new long[]{1000,1000,1000,1000,1000,1000,1000,1000})
                .setPriority(NotificationCompat.PRIORITY_HIGH);
        NotificationManagerCompat notificacionManager = NotificationManagerCompat.from(context);
        notificacionManager.notify(NOTIFICATION_ID, thisNotifi.build());
    }

}

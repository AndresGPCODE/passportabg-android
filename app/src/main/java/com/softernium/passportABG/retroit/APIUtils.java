package com.softernium.passportABG.retroit;

import com.softernium.passportABG.ConstantesGlobales;

public class APIUtils {

    private APIUtils() {}

    static ConstantesGlobales utils = new ConstantesGlobales();

    public static final String BASE_URL =  utils.getApi() +""+ utils.getCho();

    public static IGoogleApi postAPIService() {

        return RetrofitClient.getClient(BASE_URL).create(IGoogleApi.class);
    }
}

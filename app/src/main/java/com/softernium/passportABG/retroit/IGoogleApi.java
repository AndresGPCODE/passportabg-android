package com.softernium.passportABG.retroit;

import com.softernium.passportABG.providers.Post;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface IGoogleApi {

    @GET
    Call<String> getDirections(@Url String url);


    @POST("abg/abgsite/APIMovil/Chofer.php")
    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    Call<Post> savePost(@Body Post post);


}

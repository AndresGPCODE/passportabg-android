package com.softernium.passportABG;

import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.softernium.passportABG.utils.NukeSSLCerts;

public class rastreoVehiculo extends AppCompatActivity {

    // *************************************************
    // *************************************************
    // ************ VARIABLES y CONSTANTES *************
    // ****************** REFERENCIAS ******************
    // *************************************************
    // *************************************************

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new NukeSSLCerts().nuke(this);

        setContentView(R.layout.activity_rastreo_vehiculo);
        getSupportFragmentManager().beginTransaction().replace(R.id.mapa_rastreo, new mapaFragmentoRastreo()).commit();
        getSupportActionBar().setTitle("Vehiculo en ruta de entrega.");

    }

    @Override
    protected void onResume() {
        super.onResume();
    }




}

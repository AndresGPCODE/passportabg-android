package com.softernium.passportABG;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.softernium.passportABG.utils.NukeSSLCerts;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import dmax.dialog.SpotsDialog;

public class notificacionesFragmento extends Fragment {
    private ListView tabla;
    private Boolean control;

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    private JSONArray notificaciones;
    AlertDialog nDialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragmento_notificaciones,container,false);

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new NukeSSLCerts().nuke(this.getContext());

        // la vista se eta creando
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // la vista ha sido cargada
        tabla =  getView().findViewById(R.id.tablaNotificacionesChofer);
        control = true;
        pref = notificacionesFragmento.this.getActivity().getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        editor = pref.edit();
         nDialog = new SpotsDialog.Builder().setContext(notificacionesFragmento.this.getContext()).setMessage("Cargando...").setTheme(R.style.Custom).build();
        nDialog.show();
        obtenerNotificaciones();
    }

    private void crearTabla(){
        notificacionesAdapter adapterNew = new notificacionesAdapter(getActivity(), R.layout.notificaciones_adapter, notificaciones);
        tabla.setAdapter(adapterNew);
        tabla.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int posision, long id) {
                try {
                    if(!"-1".equals(notificaciones.getJSONObject(posision).getString("nIDNotificacion"))){
                        notificacionLeida(notificaciones.getJSONObject(posision).getString("nIDNotificacion"));
                    } else {
                        //Toast.makeText(notificacionesFragmento.this.getContext(), "No leer notificacion", Toast.LENGTH_SHORT).show();
                    }
                    notificaciones.getJSONObject(posision).put("Estatus","LEIDO");
                    if("9".equals(notificaciones.getJSONObject(posision).getString("nIDTipoNotificacion"))){
                        String[] idArr = notificaciones.getJSONObject(posision).getString("Body").split(",,");
                        alertDialogEntrega("Entrega de vehiculo","Acepta la entrega del vehiculo?",true, idArr[1]);
                    }else{
                        alertDialog(notificaciones.getJSONObject(posision).getString("Encabezado"),""+ notificaciones.getJSONObject(posision).getString("Body"), true);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                notificacionesAdapter adapterNew = new notificacionesAdapter(getActivity(), R.layout.notificaciones_adapter, notificaciones);
                tabla.setAdapter(adapterNew);
                tabla.setSelection(posision);
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        control = false;
    }

    private void alertDialog(String titulo, String msg, boolean dismiss) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(titulo);
        builder.setMessage(msg);
        builder.setPositiveButton("Aceptar", null);
        if ( dismiss){
            builder.setNegativeButton("Cancelar", null);
        }
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void alertDialogEntrega(String titulo, String msg, boolean dismiss, final String idSol) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(titulo);
        builder.setMessage(msg);
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                validarRecepcion(idSol);
            }
        });
        if ( dismiss){
            builder.setNegativeButton("Cancelar", null);
        }
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    private void validarRecepcion(String id) {
        final ConstantesGlobales direcciones = new ConstantesGlobales();
        RequestQueue peticion = Volley.newRequestQueue(notificacionesFragmento.this.getContext());

        String url = direcciones.getApi() +""+ direcciones.getCho();
        final String data = "{"+
                " \"idChofer\": \"0\", " +
                " \"nIDSolicitud\": \""+id+"\", " +
                " \"option\": 5" +
                "}";
        final Map<String, String>[] datos = new Map[]{new HashMap<String, String>()};
        StringRequest stringRegquest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject( response );
                    if(json.getBoolean("status")){
                        Intent intent = new Intent(notificacionesFragmento.this.getActivity(), encuestaValoracion.class);
                        startActivity(intent);
                        editor.putBoolean("statusCambio", false);
                        editor.apply();
                        //Toast.makeText(notificacionesFragmento.this.getContext(), "Recepcion validada", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(notificacionesFragmento.this.getContext(), "No puedes validar esta entrega.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Error de respuesta","Error");
                error.printStackTrace();
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRegquest.setRetryPolicy(policy);
        peticion.add(stringRegquest);


        //Intent intent = new Intent(formularioEntrega.this, mainChofer.class);
        //startActivity(intent);

    }

    public void timeOut(int timeInterval){
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        if(control){
                            obtenerNotificaciones();
                        }
                    }
                }, timeInterval);
    }

    public void obtenerNotificaciones(){
        // Peticion Post
        final ConstantesGlobales direcciones = new ConstantesGlobales();
        RequestQueue peticion = Volley.newRequestQueue(notificacionesFragmento.this.getActivity());
        String url = direcciones.getApi() +""+ direcciones.getNot();

        final String data = "{"+
                " \"kndUser\": \""+ pref.getInt("kndUser", 0) + "\", " +
                " \"usrId\":\"" + pref.getInt("nIDCliente", 0) +"\", " +
                " \"option\": 1," +
                " \"notificacionesTotalesLeidas\": 1" +
                "}";
        final Map<String, String>[] datos = new Map[]{new HashMap<String, String>()};
        StringRequest stringRegquest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    nDialog.dismiss();
                    Log.i("Data: ",response);

                    JSONArray json = new JSONArray( response );
                    //alertDialog("",""+json,false);

                    if (json.getJSONObject(0).getBoolean("status")){
                        notificaciones = json;
                        //Log.i("JSON",json.toString());
                        crearTabla();
                    } else {
                        JSONObject obj = new JSONObject();
                        obj.put("nIDNotificacion","-1");
                        obj.put("nIDCliente",pref.getInt("nIDCliente", 0));
                        obj.put("nIDTipoNotificacion","-1");
                        obj.put("Encabezado","Sin notificaciones");
                        obj.put("Body","No hay notificaciones vigentes");
                        obj.put("Permiso","SI");
                        obj.put("Estatus","LEIDO");
                        obj.put("TipoNotificacion","POR DEFECTO");
                        obj.put("Descripcion","Se muestra esta notificacion por falta de notificaciones");
                        obj.put("status",true);
                        obj.put("resp","Notificacion por defecto");

                        JSONArray ja = new JSONArray();
                        ja.put(obj);
                        notificaciones = ja;
                        crearTabla();
                        //Toast.makeText(notificacionesFragmento.this.getContext(), "No hay notificaciones.", Toast.LENGTH_LONG).show();
                    }
                    if(control){
                        timeOut(30000);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Error de respuesta","Error");
                error.printStackTrace();
            }
        }){

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        peticion.add(stringRegquest);
    }



    public void notificacionLeida(String id){
        // Peticion Post
        final ConstantesGlobales direcciones = new ConstantesGlobales();
        RequestQueue peticion = Volley.newRequestQueue(notificacionesFragmento.this.getActivity());
        String url = direcciones.getApi() +""+ direcciones.getNot();

        final String data = "{"+
                " \"kndUser\": \""+ pref.getInt("kndUser", 0) + "\", " +
                " \"usrId\":\"" + pref.getInt("nIDCliente", 0) +"\", " +
                " \"option\": 2," +
                " \"idNotificacion\":" + id +
                "}";

        final Map<String, String>[] datos = new Map[]{new HashMap<String, String>()};
        //Toast.makeText(notificacionesFragmento.this.getContext(), ""+data, Toast.LENGTH_SHORT).show();
        StringRequest stringRegquest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    nDialog.dismiss();
                    //Log.i("Respuesta",""+response);
                    JSONArray json = new JSONArray( response );


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Error de respuesta","Error");
                error.printStackTrace();
            }
        }){

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        peticion.add(stringRegquest);
    }
} // fin de la clase

package com.softernium.passportABG;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.JointType;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.SquareCap;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.softernium.passportABG.providers.GoogleApiProvider;
import com.softernium.passportABG.utils.DecodePoints;
import com.softernium.passportABG.utils.NukeSSLCerts;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class mapaFragmentoChofer extends Fragment implements OnMapReadyCallback {



    /***********************************************************************************
     ***********************************************************************************
     ***********************************************************************************
     ***********************************************************************************
     ***********************************************************************************
     ***********************************************************************************
     *                                                                                 *
     * ESTA CLASE QUEDO DESUTILIZADA Y SE SUSTITUYO POR mapaFreagmnetoChoferNuevo.java *
     *                                                                                 *
     ***********************************************************************************
     ***********************************************************************************
     ***********************************************************************************
     ***********************************************************************************
     ***********************************************************************************
     ***********************************************************************************
     **/

    private MapView mapView;
    private View rootView;
    private GoogleMap gMap;

    private List<LatLng> mPolylineList;
    private PolylineOptions mPolylineOptions;

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    int PERMISSION_ID = 44;
    FusedLocationProviderClient mFusedLocationClient;
    String lat = "0";
    String lon = "0";
    private MarkerOptions place1, place2;
    private Polyline currentPolyline;
    public Boolean control = true;

    private GoogleApiProvider mGoogleApiProvider;


    public mapaFragmentoChofer() {
        // Required empty public constructor
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new NukeSSLCerts().nuke(this.getContext());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(mapaFragmentoChofer.this.getActivity());
        rootView = inflater.inflate(R.layout.fragment_mapa_fragmento_chofer_nuevo, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapView = (MapView) rootView.findViewById(R.id.mapChofer);
        pref = this.getActivity().getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        editor = pref.edit();
        if (checkPermissions()) {
            if (isLocationEnabled()) {
                //Toast.makeText(this.getActivity(), "Permisos Concedidos", Toast.LENGTH_SHORT).show();
            } else {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        } else {
            requestPermissions();
            getLastLocation();
        }
        if (mapView != null) {
            mapView.onCreate(null);
            mapView.getMapAsync(this);
            mapView.onResume();
        }
        //checkPermissions();
        mGoogleApiProvider = new GoogleApiProvider(mapaFragmentoChofer.this.getContext());
        //Toast.makeText(getContext(), "densityDpi: "+densityDpi+"\nxdpi: "+xdpi+"\nydpi: "+ydpi+"\nwidthPixels:"+widthPixels,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResume() {
        super.onResume();
        control = true;
        if (checkPermissions()) {
            getLastLocation();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        control = false;
        //Toast.makeText(mapaFragmentoChofer.this.getContext(), ""+control, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStop() {
        super.onStop();
        control = false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        control = false;
    }

    public void timeOut(int timeInterval){
        Toast.makeText(this.getActivity(), "Obteneiendo ultima ubicacion, actualizando ubicacion en el mapa", Toast.LENGTH_SHORT).show();
        new android.os.Handler().postDelayed(
            new Runnable() {
                public void run() {
                    if(control){
                        getLastLocation();
                    }
                }
            }, timeInterval
        );
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;
        checkPermissions();
        // Add a marker in Sydney and move the camera
        getLastLocation();
        //gMap.clear();

    }


    private boolean checkPermissions(){
        if (ActivityCompat.checkSelfPermission(this.getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this.getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            return true;
        }
        return false;
    }
    private void requestPermissions(){
        ActivityCompat.requestPermissions(
                mapaFragmentoChofer.this.getActivity(), new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_ID
        );
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_ID) {
            if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                // Granted. Start getting the location information
                getLastLocation();
            }
        }
    }
    private boolean isLocationEnabled(){
        LocationManager locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
                LocationManager.NETWORK_PROVIDER
        );
    }
    @SuppressLint("MissingPermission")
    private void getLastLocation(){
        if (checkPermissions()) {
            if (isLocationEnabled()) {
                mFusedLocationClient.getLastLocation().addOnCompleteListener(
                    new OnCompleteListener<Location>() {
                        @Override
                        public void onComplete(@NonNull Task<Location> task) {
                        Location location = task.getResult();
                        if (location == null) {
                            requestNewLocationData();
                        } else {
                            gMap.setBuildingsEnabled(false);
                            gMap.setMyLocationEnabled(true);
                            gMap.getUiSettings().setMyLocationButtonEnabled(true);
                            lat = ""+location.getLatitude();
                            lon = ""+location.getLongitude();
                            gMap.clear();
                            LatLng actual = new LatLng(Double.parseDouble(lat), Double.parseDouble(lon));
                            // gMap.addMarker(new MarkerOptions().position(actual).title("Ubicacion Actual.").draggable(false));
                            if(!pref.getString("latitudEntrega","0.0").equals("0.0")  && !pref.getString("longitudEntrega","0.0").equals("0.0")){
                                LatLng destino = new LatLng(Double.parseDouble(pref.getString("latitudEntrega","0.0")), Double.parseDouble(pref.getString("longitudEntrega","0.0")));
                                //alertDialog("",""+destino,false);
                                gMap.addMarker(new MarkerOptions().position(destino).title("Destino.").draggable(false));
                                //Log.i("Ubicaciones","Lat: " + lat + ", Lon: " + lon + ", ELat: " + Double.parseDouble(pref.getString("latitudEntrega","0.0")) + ", ELon: " + Double.parseDouble(pref.getString("longitudEntrega","0.0")));
                                //LatLngBounds Zona = new LatLngBounds(actual, destino);
                                //gMap.moveCamera(CameraUpdateFactory.newLatLngBounds(Zona, 90));
                                drawRoute(actual, destino);
                                CameraPosition camara = new CameraPosition.Builder()
                                    .target(actual)
                                    .zoom(16)
                                    .build();
                                gMap.moveCamera(CameraUpdateFactory.newCameraPosition(camara));
                            } else {
                                CameraPosition camara = new CameraPosition.Builder()
                                    .target(actual)
                                    .zoom(12)
                                    .build();
                                gMap.moveCamera(CameraUpdateFactory.newCameraPosition(camara));
                            }
                            if(control){
                                timeOut(15000);
                            }
                        }
                        }
                    }
                );
            } else {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        } else {
            requestPermissions();
            getLastLocation();
        }
    }

    @SuppressLint("MissingPermission")
    private void requestNewLocationData(){
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(0);
        mLocationRequest.setFastestInterval(0);
        mLocationRequest.setNumUpdates(1);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(mapaFragmentoChofer.this.getActivity());
        mFusedLocationClient.requestLocationUpdates(
                mLocationRequest, mLocationCallback,
                Looper.myLooper()
        );
    }

    private LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            Location mLastLocation = locationResult.getLastLocation();
        }
    };

    private void drawRoute(LatLng mOriginLatLng, LatLng mDestinationLatLng) {
        mGoogleApiProvider.getDirections(mOriginLatLng, mDestinationLatLng).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {
                    //Toast.makeText(getContext(), ""+response.body(), Toast.LENGTH_SHORT).show();
                    //Log.i("response.body()",response.body());
                    //alertDialog("response.body()",response.body(),false);
                    JSONObject jsonObject = new JSONObject(response.body());
                    JSONArray jsonArray = jsonObject.getJSONArray("routes");
                    JSONObject route = jsonArray.getJSONObject(0);
                    JSONObject polylines = route.getJSONObject("overview_polyline");
                    String points = polylines.getString("points");
                    mPolylineList = DecodePoints.decodePoly(points);
                    mPolylineOptions = new PolylineOptions();
                    mPolylineOptions.color(Color.parseColor("#c8102e"));
                    mPolylineOptions.width(8f);
                    mPolylineOptions.startCap(new SquareCap());
                    mPolylineOptions.jointType(JointType.ROUND);
                    mPolylineOptions.addAll(mPolylineList);
                    gMap.addPolyline(mPolylineOptions);
                } catch(Exception e) {
                    Log.d("Error", "Error encontrado " + e.getMessage());
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }
    private void alertDialog(String titulo, String msg, boolean dismiss) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getContext());
        builder.setTitle(titulo);
        builder.setMessage(msg);

        builder.setPositiveButton("Aceptar", null);
        if ( dismiss){
            builder.setNegativeButton("Cancelar", null);
        }
        AlertDialog dialog = builder.create();
        dialog.show();
    }

}



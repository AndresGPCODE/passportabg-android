package com.softernium.passportABG;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class celdasChatsAdapter extends BaseAdapter {


    private Context context;
    private int layout;
    private JSONArray list;

    public celdasChatsAdapter (Context context, int layout, JSONArray list){

        this.context = context;
        this.layout = layout;
        this.list = list;

    }

    @Override
    public int getCount() {
        return this.list.length();
    }

    @Override
    public Object getItem(int i) {
    try {
        return this.list.getJSONObject(i);
    } catch (JSONException e) {
        e.printStackTrace();
        return null;
    }
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View v = view;

        LayoutInflater inflate = LayoutInflater.from(this.context);
        v = inflate.inflate(R.layout.celdas_chat_generales, null, false);
        JSONObject msg = (JSONObject) getItem(i);


        TextView mensaje = (TextView) v.findViewById(R.id.textoMensajeId);
        LinearLayout lay = v.findViewById(R.id.mensajeLayoutId);
        TextView fecha = (TextView) v.findViewById(R.id.fechaMensajeId);

        try {
            if (msg.getBoolean("isIncoming")){
                mensaje.setText(msg.getString("Texto"));
                fecha.setTextColor(Color.parseColor("#000000"));
                mensaje.setTextColor(Color.parseColor("#000000"));
                lay.setBackgroundResource(R.drawable.celda_entrada);
                fecha.setText(msg.getString("FechaCreacion"));
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                params.gravity = Gravity.LEFT;
                params.topMargin = 15;
                params.leftMargin = 15;
                lay.setLayoutParams(params);

            } else {
                mensaje.setText(msg.getString("Texto"));
                fecha.setText(msg.getString("FechaCreacion"));
                fecha.setTextColor(Color.parseColor("#FFFFFF"));
                mensaje.setTextColor(Color.parseColor("#FFFFFF"));
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                params.gravity = Gravity.RIGHT;
                params.topMargin = 15;
                params.rightMargin = 15;
                lay.setLayoutParams(params);
                lay.setBackgroundResource(R.drawable.celda_salida);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return v;
    }


}

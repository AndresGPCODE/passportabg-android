package com.softernium.passportABG;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.softernium.passportABG.utils.NukeSSLCerts;


/**
 * A simple {@link Fragment} subclass.
 */
public class choferMenu extends Fragment {

    private Button logout;
    private ImageButton chatBtnChofer;

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    public choferMenu() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new NukeSSLCerts().nuke(this.getContext());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_chofer_menu, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    logout = getActivity().findViewById(R.id.cerrarSessionChofer);
    chatBtnChofer = getActivity().findViewById(R.id.chatBtnChofer);

        pref = choferMenu.this.getActivity().getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        editor = pref.edit();

    logout.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            editor.putBoolean("status", false);
            editor.apply();
            Intent intent = new Intent(choferMenu.this.getActivity(), login.class);
            startActivity(intent);
        }
    });




    }
}// Fin de la clase

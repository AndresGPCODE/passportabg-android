package com.softernium.passportABG;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.softernium.passportABG.utils.NukeSSLCerts;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dmax.dialog.SpotsDialog;

public class vehiculosFragmento extends Fragment {
    private ListView tabla;

    private List<String> vehiculos = new ArrayList<String>();
    private JSONArray vehiculosList = new JSONArray();
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Boolean hayVehiculos = true;
    AlertDialog nDialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragmento_vehiculos,container,false);
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new NukeSSLCerts().nuke(this.getContext());

        // la vista se esta creando
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // la vista ha sido cargada
        tabla =  getView().findViewById(R.id.tablaNotificacionesChofer);
        pref = vehiculosFragmento.this.getActivity().getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        editor = pref.edit();
        obtenreVehiculosList();
    }

    private void crearTabla(){
        // MyAdapter es la clase del controlador
        MyAdapter adapterNew = new MyAdapter(getActivity(), R.layout.list_item_vehiculo, vehiculosList);
        tabla.setAdapter(adapterNew);
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, vehiculos);
//        tabla.setAdapter(adapter);
        tabla.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int posision, long id) {
                if (hayVehiculos){
                    Intent intent = new Intent(vehiculosFragmento.this.getActivity(), detalleVehiculoList.class);
                    try {
                        editor.putString("ImagenDetalleList", vehiculosList.getJSONObject(posision).getString("Imagen"));
                        editor.apply();
                        intent.putExtra("modeloTextPass", vehiculosList.getJSONObject(posision).getString("Modelo"));
                        intent.putExtra("idTextPass", vehiculosList.getJSONObject(posision).getString("nIDAutomovil"));
                        intent.putExtra("marcaTextPass", vehiculosList.getJSONObject(posision).getString("Marca"));
                        intent.putExtra("colorTextPass", vehiculosList.getJSONObject(posision).getString("Color1"));
                        intent.putExtra("noPuertasTextPass", vehiculosList.getJSONObject(posision).getString("NumeroPuertas"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    startActivity(intent);
                }
            }
        });
    }
    private void alertDialog(String titulo, String msg, boolean dismiss) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(titulo);
        builder.setMessage(msg);
        builder.setPositiveButton("Aceptar", null);
        if ( dismiss){
            builder.setNegativeButton("Cancelar", null);
        }
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    public void obtenreVehiculosList(){
        nDialog = new SpotsDialog.Builder().setContext(vehiculosFragmento.this.getContext()).setMessage("Buscando").setTheme(R.style.Custom).build();
        nDialog.show();
        final ConstantesGlobales direcciones = new ConstantesGlobales();
        RequestQueue peticion = Volley.newRequestQueue(vehiculosFragmento.this.getContext());
        String url = direcciones.getApi() +""+ direcciones.getVeh();
        final String data = "{"+
                " \"usrId\":" + pref.getInt("nIDCliente", 0) +
                "}";
        final Map<String, String>[] datos = new Map[]{new HashMap<String, String>()};
        StringRequest stringRegquest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    if (!response.equals("") && response != null) {
                        hayVehiculos = true;
                        JSONArray json = new JSONArray( response );
                        vehiculosList = json;
                        if (!vehiculosList.getJSONObject(0).getBoolean("status")){
                            hayVehiculos = false;
                        }
                    } else {
                        hayVehiculos = false;
                        JSONObject item = new JSONObject();
                        item.put("Modelo","");
                        item.put("Marca","Sin Vehiculos");
                        item.put("Linea_Submarca","");
                        item.put("Imagen","0,0");
                        vehiculosList.put(item);
                    }
                } catch (JSONException e) {
                    hayVehiculos = false;
                    e.printStackTrace();
                }
                crearTabla();
                nDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Error de respuesta","Error");
                error.printStackTrace();
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        peticion.add(stringRegquest);
    }
}// Fin de la clase


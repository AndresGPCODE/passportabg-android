package com.softernium.passportABG;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.softernium.passportABG.utils.NukeSSLCerts;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class InicioFragmento extends Fragment {

    Button verProximoCambio;
    Button detalleMetricas;
    private FloatingActionButton revisarRastreo;
    private ImageView autoActual;
    private TextView modeloactual;
    private TextView bienvenida, fechaCambio, tituloCambio;
    Toolbar toolbar;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private PieChart pieChart ;
    int diasTranscurridos;
    int totalDias = 30;
    Boolean cambioEsEnRuta = false;
    private Boolean esDiasExedidos = false;
    private int  maxDias = 30;
    ConstantesGlobales util = new ConstantesGlobales();


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragmento_inicio,container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new NukeSSLCerts().nuke(this.getContext());
        //this.getActivity().onBackPressed();
//        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
//            @Override
//            public void handleOnBackPressed() {
//                //setEnabled(false);
//                Log.i("OnBackPressedCallback","OnBackPressedCallback Event");
//            }
//        };
//        callback.setEnabled(false);
//        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);


    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pref = InicioFragmento.this.getActivity().getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        editor = pref.edit();
        pieChart = getView().findViewById(R.id.pieChartDias);
        verProximoCambio = getView().findViewById(R.id.verProximoCambioBtn);
        verProximoCambio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(cambioEsEnRuta){
                    irRastreoEntrega();
                } else {
                    goToDetlleViews();
                }

            }
        });
        fechaCambio = getView().findViewById(R.id.fechaCambio);
        detalleMetricas = getView().findViewById(R.id.detallemetricasbtn);
        detalleMetricas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToDetalleMetricas();
            }
        });
        autoActual = getView().findViewById(R.id.vehiculoActualImg);

        modeloactual = getView().findViewById(R.id.modeloVehiculoTitulo);
        modeloactual.setText(pref.getString("Modelo",""));
        bienvenida  = getView().findViewById(R.id.bienvenidaTitulo);
        bienvenida.setText("Buen día: "+pref.getString("nombreCliente","Grupo ABG Passport"));
        tituloCambio = getActivity().findViewById(R.id.vehiculoActualTitulo);
        diasTranscurridos = 0;
        cargarGradicaDias();
    }

    private void cargarGradicaDias() {
        Description description = new Description();
        description.setText("Días de uso.");
        description.setTextSize(14);
        description.setPosition(160,30);
        pieChart.setDescription(description);

        ArrayList<PieEntry> pieEntry =  new ArrayList<>();

        if (esDiasExedidos){
            int resto = totalDias - diasTranscurridos;
            pieEntry.add(new PieEntry(resto,"Dias Exedidos"));
            pieEntry.add(new PieEntry(diasTranscurridos,"Dias Maximos"));
            PieDataSet pieSetData = new PieDataSet(pieEntry, "");
            pieSetData.setColors(new int[]{ColorTemplate.rgb("#000000"), ColorTemplate.rgb("#c8102e"), ColorTemplate.rgb("#9E9E9E")});
            pieSetData.setValueTextSize(12);
            pieSetData.setValueTextColor(ColorTemplate.rgb("#FFFFFF"));
            PieData pieData = new PieData(pieSetData);
            pieData.setValueFormatter(new IValueFormatter() {
                @Override
                public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                    return String.valueOf((int) value);
                }
            });
            pieChart.setData(pieData);
            pieChart.notifyDataSetChanged();
            pieChart.invalidate();
        } else {
            pieEntry.add(new PieEntry(diasTranscurridos,"Transurridos"));
            int resto = totalDias - diasTranscurridos;
            pieEntry.add(new PieEntry(resto,"Restantes"));
            PieDataSet pieSetData = new PieDataSet(pieEntry, "");
            pieSetData.setColors(new int[]{ColorTemplate.rgb("#c8102e"), ColorTemplate.rgb("#9E9E9E"), ColorTemplate.rgb("#9E9E9E")});
            pieSetData.setValueTextSize(12);
            pieSetData.setValueTextColor(ColorTemplate.rgb("#FFFFFF"));
            PieData pieData = new PieData(pieSetData);
            pieData.setValueFormatter(new IValueFormatter() {
                @Override
                public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                    return String.valueOf((int) value);
                }
            });
            pieChart.setData(pieData);
            pieChart.notifyDataSetChanged();
            pieChart.invalidate();
        }
    }

    private void goToDetalleMetricas(){
        Intent intent = new Intent( InicioFragmento.this.getActivity(), metricas.class);
        startActivity(intent);
    }

    private void goToDetlleViews(){
        Intent intent = new Intent( InicioFragmento.this.getActivity(), detalleCambio.class);
        editor.putString("DetalleFechaPosibleCambio","");
        editor.putString("DetalleHoraPosibleCambio","");
        editor.putString("DetalleFormatterFechaPosibleCambio","");
        editor.putString("DetalleFormatterHoraPosibleCambio","");
        editor.putString("DetallePeticionesPosibleCambio","");
        editor.putBoolean("fechaCorrecta",false);
        editor.putBoolean("horaCorrecta",false);
        editor.apply();
        startActivity(intent);
    }

    @Override
    public void onResume(){
        super.onResume();
        cargarProximoCambio();
        obtenerMetricas("" + pref.getInt("nIDCLiente",0));
        revisarProximoCambio();
    }

    private void cargarProximoCambio(){
        if(pref.getBoolean("statusCambio",false)){
            tituloCambio.setText("Proximo Cambio:");
            verProximoCambio.setEnabled(true);
            String[] dateTime = pref.getString("FechaHora","a a").split(" ");
            String[] fecha = dateTime[0].split("-");
            String[] hora = dateTime[1].split(":");
            int horaCambio = Integer.parseInt(hora[0]);
            if(horaCambio > 12){
                horaCambio = horaCambio - 12;
                fechaCambio.setText(fecha[2]+"-"+fecha[1]+"-"+fecha[0]+""+" "+horaCambio+":"+ hora[1]+":00 P.M.");
            } else if(horaCambio == 12){
                fechaCambio.setText(fecha[2]+"-"+fecha[1]+"-"+fecha[0]+""+" "+horaCambio+":"+ hora[1]+":00 P.M.");
            } else {
                fechaCambio.setText(fecha[2]+"-"+fecha[1]+"-"+fecha[0]+""+" "+horaCambio+":"+ hora[1]+":00 A.M.");
            }

            if(!pref.getString("fotoAuto","").equals("")){
                String[] arr = pref.getString("fotoAuto","a,a").split(",");
                if(!arr[0].equals("a")){
                    autoActual.setImageBitmap(util.stringtoImage(arr[1]));
                }
            }

            verProximoCambio.setBackgroundColor(Color.parseColor("#c8102e"));
            //verProximoCambio.setBackgroundResource(R.color.darkGray);
            verProximoCambio.setTextColor(Color.parseColor("#FFFFFF"));
            verProximoCambio.setBackgroundResource(R.drawable.shape);
        } else {
            tituloCambio.setText("Oportunidad:");
            autoActual.setImageResource(R.drawable.r8);
            modeloactual.setText("Audi R8");
            verProximoCambio.setEnabled(false);
            fechaCambio.setText("");
            verProximoCambio.setBackgroundColor(Color.parseColor("#A7A7A7"));
            verProximoCambio.setTextColor(Color.parseColor("#A7A7A7"));
            verProximoCambio.setBackgroundResource(R.drawable.shape);
            tituloCambio.setText("Oportunidad:");
            modeloactual.setText("Audi R8");
            verProximoCambio.setEnabled(false);
            fechaCambio.setText("");
            verProximoCambio.setBackgroundColor(Color.parseColor("#A7A7A7"));
            verProximoCambio.setTextColor(Color.parseColor("#A7A7A7"));
            verProximoCambio.setBackgroundResource(R.drawable.shape);
        }
    }

    private void obtenerMetricas(String id) {
        final ConstantesGlobales direcciones = new ConstantesGlobales();
        RequestQueue peticion = Volley.newRequestQueue(InicioFragmento.this.getContext());

        String url = direcciones.getApi() +""+ direcciones.getMetri();
        final String data = "{"+
                " \"nIDCliente\": \""+ id +"\"" +
                "}";
        final Map<String, String>[] datos = new Map[]{new HashMap<String, String>()};
        StringRequest stringRegquest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONArray json = new JSONArray( response );
                    JSONObject data = json.getJSONObject(0);
                    JSONArray calculoDias = data.getJSONArray("CalcularDias");
                    JSONObject dataDias = calculoDias.getJSONObject(0);
                    if(data.getBoolean("status")){
                        // Respuesta del kilometraje
                        Log.i("MetricasMensuales",calculoDias.toString());

                        diasTranscurridos = dataDias.getInt("DiasTranscurridos");
                        maxDias = dataDias.getInt("MaxDiasRenta");
                        if(diasTranscurridos > maxDias){
                            totalDias = diasTranscurridos;
                            diasTranscurridos = maxDias;
                            esDiasExedidos = true;
                        }
                        cargarGradicaDias();
                    } else {
                        // Error de kilometraje
                        Log.i("KilometrajeNo: ","" + response);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Error de respuesta","Error");
                error.printStackTrace();
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRegquest.setRetryPolicy(policy);
        peticion.add(stringRegquest);
    } // Fin de la peticion de Kilometraje

    private void alertDialog(String titulo, String msg, boolean dismiss) {
        AlertDialog.Builder builder = new AlertDialog.Builder(InicioFragmento.this.getContext());
        builder.setTitle(titulo);
        builder.setMessage(msg);

        builder.setPositiveButton("Aceptar", null);
        if ( dismiss){
            builder.setNegativeButton("Cancelar", null);
        }
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void revisarProximoCambio(){
        final ConstantesGlobales direcciones = new ConstantesGlobales();
        RequestQueue peticion = Volley.newRequestQueue(this.getContext());
        String url = direcciones.getApi() +""+ direcciones.getUsr();
        final String data = "{"+
                " \"nIDCliente\": \""+ pref.getInt("nIDCliente", 0) + "\", " +
                " \"option\": \"1\"" +
                "}";
        StringRequest stringRegquest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    //Toast.makeText(getActivity(), response, Toast.LENGTH_LONG).show();
                    JSONObject json = new JSONObject( response );
                    Log.i("Respuesta: ", response);
                    if(json.getBoolean("status")){
                        editor.putBoolean("statusCambio", true);
                        editor.putString("FechaHora", json.getString("FechaHora"));
                        if(!json.isNull("fotoAuto")){
                            String fotoAuto = json.get("fotoAuto").toString();
                            editor.putString("fotoAuto",fotoAuto);
                        }else{
                            editor.putString("fotoAuto","");
                        }
                        editor.apply();
                        if(json.getString("Estatus").equals("EN RUTA")){
                            verProximoCambio.setText(" Seguir entrega ");
                            cambioEsEnRuta = true;
                        } else {
                            verProximoCambio.setText("Ver");
                            cambioEsEnRuta = false;
                        }

                    } else {
                        editor.putBoolean("statusCambio", false);
                        editor.apply();
                        // No hay solicitudes

                    }
                    cargarProximoCambio();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Error de respuesta","Error");
                error.printStackTrace();
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        peticion.add(stringRegquest);
    }

    private void irRastreoEntrega(){
        Intent intent = new Intent(this.getContext(), rastreoVehiculo.class);
        startActivity(intent);
    }

} // Fin de la clase

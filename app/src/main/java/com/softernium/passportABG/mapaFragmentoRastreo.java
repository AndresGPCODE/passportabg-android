package com.softernium.passportABG;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.softernium.passportABG.utils.NukeSSLCerts;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import static androidx.core.content.ContextCompat.checkSelfPermission;


/**
 * A simple {@link Fragment} subclass.
 * */

public class mapaFragmentoRastreo extends Fragment implements OnMapReadyCallback {

    private View rootView;
    private GoogleMap gMap;
    private MapView mapView;
    private FloatingActionButton chatChoferRastreo, ubicarVehiculoFBBtn;
    FusedLocationProviderClient mFusedLocationClient;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    String lat = "0";
    String lon = "0";
    final int SEND_SMS_PERMISSION_REQUEST_CODE = 1;
    int PERMISSION_ID = 44;
    private int id = 0;
    private RequestQueue requestQueue;
    private Boolean control = true;
    ConstantesGlobales api = new ConstantesGlobales();
    LatLng miUbicacion = null;
    LatLng choferUbicacion = null;



    public mapaFragmentoRastreo() {

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new NukeSSLCerts().nuke(this.getContext());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(mapaFragmentoRastreo.this.getActivity());
        rootView = inflater.inflate(R.layout.fragment_mapa_fragmento_rastreo, container, false);
        return rootView;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pref = this.getActivity().getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        editor = pref.edit();
        mapView = (MapView) rootView.findViewById(R.id.mapRastreo);
        chatChoferRastreo = rootView.findViewById(R.id.chatChoferRastreo);
        ubicarVehiculoFBBtn = rootView.findViewById(R.id.ubicarVehiculoFB);
        if(mapView != null){
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this);
        }
        if (checkPermissions()) {
            if (isLocationEnabled()) {
                //Toast.makeText(this.getActivity(), "Permisos Concedidos", Toast.LENGTH_SHORT).show();
            } else {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        } else {
            requestPermissions();
            getLastLocation();
        }
        chatChoferRastreo.setOnClickListener(
            new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    irChatClienteChofer();
                }
            }
        );
        ubicarVehiculoFBBtn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ubicarVehiculo();
                    }
                }
        );
        id = pref.getInt("nIDCliente",0);

        getLocation();

    }

    public Boolean checkPermissionsSMS(String permission){
        int check = checkSelfPermission(mapaFragmentoRastreo.this.getActivity(), permission);
        return (check == PackageManager.PERMISSION_GRANTED);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;
        getLastLocation();
    }

    private boolean checkPermissions(){
        if (ActivityCompat.checkSelfPermission(mapaFragmentoRastreo.this.getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(mapaFragmentoRastreo.this.getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            return true;
        }
        return false;
    }

    private void requestPermissions(){
        ActivityCompat.requestPermissions(
                mapaFragmentoRastreo.this.getActivity(), new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_ID
        );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_ID) {
            if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                // Granted. Start getting the location information
                getLastLocation();
            }
        }
    }

    private boolean isLocationEnabled(){
        LocationManager locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
                LocationManager.NETWORK_PROVIDER
        );
    }



    @SuppressLint("MissingPermission")
    private void requestNewLocationData(){
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(0);
        mLocationRequest.setFastestInterval(0);
        mLocationRequest.setNumUpdates(1);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(mapaFragmentoRastreo.this.getContext());
        mFusedLocationClient.requestLocationUpdates(
                mLocationRequest, mLocationCallback,
                Looper.myLooper()
        );
    }

    private LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            Location mLastLocation = locationResult.getLastLocation();
        }
    };

    private void alertMsg(String titulo, String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mapaFragmentoRastreo.this.getContext());
        builder.setTitle(titulo);
        builder.setMessage(msg);

        builder.setPositiveButton("Aceptar", null);
        //builder.setNegativeButton("Contrato", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void timeOut(int timeInterval){
        Log.i("Interval","Intervlo establecido");
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        if(control){
                            getLocation();
                        }
                    }
                }, timeInterval);
    }
    @Override
    public void onStop() {
        super.onStop();
        control = false;
    }
    @Override
    public void onPause() {
        super.onPause();
        control = false;
    }
    @Override
    public void onResume() {
        super.onResume();
        control = true;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        control = false;
    }

    public void getLocation(){
        // Peticion Post
        final ConstantesGlobales direcciones = new ConstantesGlobales();
        RequestQueue peticion = Volley.newRequestQueue(mapaFragmentoRastreo.this.getContext());
        String url = direcciones.getApi() +""+ direcciones.getUbi();
        final String data = "{"+
                " \"nIDCliente\": \""+ id + "\", " +
                " \"option\": 1" +
                "}";
        //Toast.makeText(mapaFragmentoRastreo.this.getContext(), data, Toast.LENGTH_SHORT).show();
        final Map<String, String>[] datos = new Map[]{new HashMap<String, String>()};
        StringRequest stringRegquest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray json = new JSONArray( response );
                    if(json.getJSONObject(0).getBoolean("status")){
                        gMap.clear();
                        LatLng vehiculo = new LatLng(Double.parseDouble(json.getJSONObject(0).getString("Latitud")), Double.parseDouble(json.getJSONObject(0).getString("Longitud")));
                        //gMap.addMarker(new MarkerOptions().position(vehiculo).title("Chofer en ruta - "+ json.getJSONObject(0).getString("Direccion")).draggable(false));
                        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.abg_pin);
                        gMap.addMarker(new MarkerOptions().position(vehiculo).title("Chofer en ruta").icon(icon).snippet("Vehiculo").draggable(false));

                        miUbicacion = new LatLng(Double.parseDouble(json.getJSONObject(0).getString("LatitudEntrega")),
                                Double.parseDouble(json.getJSONObject(0).getString("LongitudEntrega")));

                        choferUbicacion = vehiculo;

                        if(miUbicacion != null){
                            gMap.addMarker(new MarkerOptions().position(miUbicacion).title("Punto de entrega").draggable(true));
                            LatLngBounds.Builder builder = new LatLngBounds.Builder();
                            builder.include(miUbicacion).include(vehiculo);
                            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(builder.build(), 100);
                            gMap.moveCamera(cameraUpdate);
                        } else {
                            CameraPosition camara = new CameraPosition.Builder()
                                    .target(vehiculo)
                                    .zoom(14)
                                    .build();
                            gMap.moveCamera(CameraUpdateFactory.newCameraPosition(camara));
                        }
                        timeOut(15000);
                    } else {
                        timeOut(3000);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Error de respuesta","Error"+error);
                error.printStackTrace();
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        int socketTimeout = 10000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRegquest.setRetryPolicy(policy);
        peticion.add(stringRegquest);
    }

    @SuppressLint("MissingPermission")
    private void getLastLocation(){
        if (checkPermissions()) {
            if (isLocationEnabled()) {
                mFusedLocationClient.getLastLocation().addOnCompleteListener(
                        new OnCompleteListener<Location>() {
                            @Override
                            public void onComplete(@NonNull Task<Location> task) {
                                Location location = task.getResult();
                                if (location == null) {
                                    requestNewLocationData();
                                } else {
                                    lat = ""+location.getLatitude();
                                    lon = ""+location.getLongitude();
                                    LatLng actual = new LatLng(Double.parseDouble(lat), Double.parseDouble(lon));
                                    miUbicacion = actual;
                                    gMap.setBuildingsEnabled(false);
                                    gMap.setMyLocationEnabled(true);
                                    gMap.getUiSettings().setMyLocationButtonEnabled(true);
                                    CameraPosition camara = new CameraPosition.Builder()
                                            .target(actual)
                                            .zoom(14)
                                            .build();
                                    //gMap.moveCamera(CameraUpdateFactory.newCameraPosition(camara));
                                }
                            }
                        }
                );
            } else {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        } else {
            requestPermissions();
            getLastLocation();
        }
    }

    private void irChatClienteChofer(){
        Intent intent = new Intent(this.getContext(), chatClienteChofer.class);
        startActivity(intent);
    }

    private void ubicarVehiculo(){

        if(choferUbicacion != null){
            CameraPosition camara = new CameraPosition.Builder()
                    .target(choferUbicacion)
                    .zoom(16)
                    .build();
            gMap.moveCamera(CameraUpdateFactory.newCameraPosition(camara));
        }

    }

} // fin de la clase

package com.softernium.passportABG;

public interface TaskLoadedCallback {
    void onTaskDone(Object... values);
}

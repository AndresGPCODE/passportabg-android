package com.softernium.passportABG;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;

public class Faq extends AppCompatActivity {

    private ImageButton chatAdministradorBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);
        getSupportActionBar().setTitle("F.A.Q.");

        chatAdministradorBtn = findViewById(R.id.chatAdministradorBtn);
        chatAdministradorBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Faq.this, chatAdministrador.class);
                startActivity(intent);
            }
        });
    }
}

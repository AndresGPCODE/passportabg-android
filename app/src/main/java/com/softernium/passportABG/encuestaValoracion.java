package com.softernium.passportABG;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.softernium.passportABG.utils.NukeSSLCerts;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class encuestaValoracion extends AppCompatActivity {

    private RatingBar chofer;
    private RatingBar tiempo;
    private RatingBar limpieza;
    private RatingBar buenEstado;
    private RatingBar tanqueLleno;
    private Button enviarValoracion;
    private TextView observaciones;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new NukeSSLCerts().nuke(this);

        setContentView(R.layout.activity_encuesta_valoracion);
        getSupportActionBar().setTitle("Encuesta de servicio");
        pref = encuestaValoracion.this.getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        editor = pref.edit();
        chofer = findViewById(R.id.ratingBarChofer);
        tiempo = findViewById(R.id.ratingBarTiempo);
        limpieza = findViewById(R.id.ratingBarLimpieza);
        buenEstado = findViewById(R.id.ratingBarBuenEstado);
        tanqueLleno = findViewById(R.id.ratingBarTanqueLleno);
        enviarValoracion = findViewById(R.id.enviarValoracion);
        observaciones = findViewById(R.id.observacionesAdicionales);
        enviarValoracion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validarCampos()){
                    enviarValoracionPeticion();
                } else {
                    // Enviar
                }
            }
        });

    }

    private boolean validarCampos() {
        if(observaciones.getText().toString() != ""){
            return true;
        }else{
            return false;
        }
    }

    private void enviarValoracionPeticion(){
        // Peticion Post
        final ConstantesGlobales direcciones = new ConstantesGlobales();
        RequestQueue peticion = Volley.newRequestQueue(encuestaValoracion.this);
        String url = direcciones.getApi() +""+ direcciones.getRep();



        final String data = "{"+
                " \"idCliente\": \""+ pref.getInt("nIDCliente", 0) + "\", " +
                " \"Limpieza\": \""+limpieza.getRating()+"\", " +
                " \"Puntualidad\": \""+tiempo.getRating()+"\", " +
                " \"Amabilidad\": \""+chofer.getRating()+"\", " +
                " \"Comentario\": \""+observaciones.getText().toString()+"\", " +
                " \"EstadoDelVehiculo\": \""+buenEstado.getRating()+"\", " +
                " \"TanqueLleno\": \""+tanqueLleno.getRating()+"\", " +
                " \"option\": 3" +
                "}";

        final Map<String, String>[] datos = new Map[]{new HashMap<String, String>()};

        StringRequest stringRegquest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //timeOut(1000);
                try {

                    JSONObject json = new JSONObject( response );
                    //alertDialog("",""+json,false);
                    if(json.getBoolean("status")){
                        Intent intent = new Intent(encuestaValoracion.this,MainActivity.class);
                        startActivity(intent);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Error de respuesta","Error");
                error.printStackTrace();
            }
        }){

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        peticion.add(stringRegquest);
    }

    private void alertDialog(String titulo, String msg, boolean dismiss) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(titulo);
        builder.setMessage(msg);
        builder.setPositiveButton("Aceptar", null);
        if ( dismiss){
            builder.setNegativeButton("Cancelar", null);
        }
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}

package com.softernium.passportABG;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.messaging.FirebaseMessaging;
import com.softernium.passportABG.utils.NukeSSLCerts;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import dmax.dialog.SpotsDialog;

public class login extends AppCompatActivity {
    CheckBox terminosChck;
    Button aceptar;
    TextInputEditText user;
    TextInputEditText pass;
    private ImageView logo;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    int cont = 0;
    String token;
    AlertDialog nDialog;
    public boolean allowBackNav = false;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new NukeSSLCerts().nuke(this);
        /* FirebaseOptions options = new FirebaseOptions.Builder()
                .setApplicationId("1:327532003335:android:f85a25c2d8816e3aff2aa2") // Required for Analytics.
                .setProjectId("abgproject-2382f") // Required for Firebase Installations.
                .setApiKey("AIzaSyATqMWD3XoSIoGjw9W89MbpQfWpw3j27Ck") // Required for Auth.
                .build();
        FirebaseApp.initializeApp(this, options, "ABGProject");*/

        setContentView(R.layout.activity_login);
        getSupportActionBar().show();
        getSupportActionBar().setTitle("Login");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        terminosChck = findViewById(R.id.aceptarTerminosCheckBox);
        aceptar = findViewById(R.id.aceptarbtn);
        user = findViewById(R.id.userText);
        pass = findViewById(R.id.passText);
        logo = findViewById(R.id.imgLogoLogin);
        pref = login.this.getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        editor = pref.edit();
        nDialog = new SpotsDialog.Builder().setContext(login.this).setMessage("Iniciando sesión...").setTheme(R.style.Custom).build();

        FirebaseMessaging.getInstance().setAutoInitEnabled(true);
//        token = FirebaseInstanceId.getInstance().getToken();
//        alertDialog("Tpken 1","Intento token 1: "+ token, false);
        //if(true){
        token = FirebaseService.getToken(this);
        //alertDialog("Tpken","Intento token: "+ token, false);
        //}

        if ("empty".equals(token)) {
            token = "TOKEN_NOT_ALLOWED_1";
        } else if ("".equals(token)) {
            token = "TOKEN_NOT_ALLOWED_2";
        }
        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginRequest();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (shouldAllowBack()) {
            super.onBackPressed();
            this.allowBackNav =  false;
            startActivity(new Intent(this, louncher.class));
            finish();
            System.exit(0);
        } else {
            this.allowBackNav = true;
            Toast.makeText(this, "Presiona de nuevo para salir", Toast.LENGTH_SHORT).show();
            // No navegar
        }
    }

    public boolean shouldAllowBack(){
        return this.allowBackNav;
    }

    private void loginRequest() {
        if (terminosChck.isChecked()){
            if (pass.getText().toString() != "" && user.getText().toString() != "" ){
                // Peticion Post
                nDialog.show();
                final String finalToken = token;
                final ConstantesGlobales direcciones = new ConstantesGlobales();
                RequestQueue peticion = Volley.newRequestQueue(login.this);
                String url = direcciones.getApi() +""+ direcciones.getLogi();
                //alertDialog("URL post",""+url, false);
                final String data = "{"+
                        " \"option\": 1," +
                        " \"email\": \""+user.getText().toString()+"\"," +
                        " \"password\": \""+pass.getText().toString() +"\"," +
                        " \"token\": \""+finalToken+"\"," +
                        " \"Dispositivo\": \"ANDROID\"" +
                        "}";
                //alertDialog("Post Data",""+data, false);
                final Map<String, String>[] datos = new Map[]{new HashMap<String, String>()};
                StringRequest stringRegquest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            //alertDialog("onResponse","Respuesta del servidor: "+response, false);
                            nDialog.dismiss();
                            JSONObject jsonArray = new JSONObject( response );
                            Boolean status = Boolean.parseBoolean(jsonArray.get("status").toString());
                            editor.putBoolean("status",status);
                            if(status){
                                String foto;
                                if(!jsonArray.isNull("Foto")){
                                    foto = jsonArray.get("Foto").toString();
                                } else {
                                    foto = "";
                                }
                                editor.putString("foto",foto);
                                if(!jsonArray.isNull("nIDUsuario")){
                                    Integer nIDUsuario = Integer.parseInt(jsonArray.getString("nIDUsuario"));
                                    editor.putInt("nIDUsuario",nIDUsuario);
                                } else {
                                    editor.putInt("nIDUsuario",0);
                                }
                                Integer kndUser = Integer.parseInt(jsonArray.get("kndUser").toString());
                                editor.putInt("kndUser",kndUser);
                                String Nombre = jsonArray.get("Nombre").toString();
                                editor.putString("nombreCliente",Nombre);
                                Integer nIDCliente = Integer.parseInt(jsonArray.get("nIDCliente").toString());
                                editor.putInt("nIDCliente",nIDCliente);
                                Boolean statusCambio = Boolean.parseBoolean(jsonArray.get("statusCambio").toString());
                                editor.putBoolean("statusCambio",statusCambio);
                                if(statusCambio){
                                    editor.putString("Latitud",jsonArray.getString("Latitud"));
                                    editor.putString("Longitud",jsonArray.getString("Longitud"));
                                }
                                if(!jsonArray.isNull("fotoAuto")){
                                    String fotoAuto = jsonArray.get("fotoAuto").toString();
                                    editor.putString("fotoAuto",fotoAuto);
                                }else{
                                    editor.putString("fotoAuto","");
                                }
                                String ca, no;
                                if(!jsonArray.isNull("Calle")){
                                    ca = jsonArray.get("Calle").toString();
                                }else{
                                    ca = "";
                                }
                                if(!jsonArray.isNull("NoExterior")){
                                    no = "# "+jsonArray.get("NoExterior").toString();
                                }else{
                                    no = "";
                                }
                                editor.putString("ubicacionCambio", ca + no);
                                if(!jsonArray.isNull("FechaHora")){
                                    String FechaHora = jsonArray.get("FechaHora").toString();
                                    editor.putString("FechaHora", FechaHora);
                                }else{
                                    editor.putString("FechaHora", "");
                                }
                                if(!jsonArray.isNull("Notas")){
                                    String Notas = jsonArray.get("Notas").toString();
                                    editor.putString("Notas", Notas);
                                }else{
                                    editor.putString("Notas", "");
                                }
                                if(!jsonArray.isNull("nIDSolicitud")){
                                    int Notas = jsonArray.getInt("nIDSolicitud");
                                    editor.putInt("lastIdCambio", Notas);
                                }else{
                                    editor.putInt("lastIdCambio", 0);
                                }
                                if(!jsonArray.isNull("contNombre")){
                                    String contNombre = jsonArray.get("contNombre").toString();
                                    editor.putString("contNombre", contNombre);
                                }else{
                                    editor.putString("contNombre", "");
                                }
                                if(!jsonArray.isNull("contnIDContactoEmergencia")){
                                    Integer contnIDContactoEmergencia = Integer.parseInt(jsonArray.get("contnIDContactoEmergencia").toString());
                                    editor.putInt("contnIDContactoEmergencia",contnIDContactoEmergencia);
                                }else{
                                    editor.putInt("contnIDContactoEmergencia",0);
                                }
                                if(!jsonArray.isNull("contTelefono")){
                                    String contTelefono = jsonArray.get("contTelefono").toString();
                                    editor.putString("contTelefono", contTelefono);
                                }else{
                                    editor.putString("contTelefono", "");
                                }
                                if(!jsonArray.isNull("contCalle")){
                                    String contCalle = jsonArray.get("contCalle").toString();
                                    editor.putString("contCalle", contCalle);
                                }else{
                                    editor.putString("contCalle", "");
                                }
                                if(!jsonArray.isNull("contNoExterior")){
                                    String contNoExterior = jsonArray.get("contNoExterior").toString();
                                    editor.putString("contNoExterior",contNoExterior);
                                } else {
                                    String contNoExterior = "";
                                    editor.putString("contNoExterior",contNoExterior);
                                }
                                //Fin de las preferencias de usuario
                                editor.apply();
                                if(kndUser == 0){
                                    // cliente
                                    goToCliente();
                                } else if(kndUser == 1){
                                    //chofer
                                    goToChofer();
                                }
                            } else {
                                nDialog.dismiss();
                                alertDialog("Usuario o contraseña incorrectos","Verifica que el usuario y la contraseña esten correctos.", false);
                            }
                        } catch (JSONException e) {
                            nDialog.dismiss();
                            Toast.makeText(login.this, "Informacion corrupta", Toast.LENGTH_SHORT).show();
                            //alertDialog("JSON","Informacion corrupta", false);
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (cont < 3){
                            // llamar funcion de login
                            loginRequest();
                            cont ++;
                        } else {
                            nDialog.dismiss();
                            Toast.makeText(login.this, "Error de conexion, reintente por favor.", Toast.LENGTH_LONG).show();
                            //alertDialog("onResponse","Respuesta del servidor: "+error.toString(), false);
                            error.printStackTrace();
                        }

                    }
                }){
                    @Override
                    public String getBodyContentType() {
                        return "application/json; charset=utf-8";
                    }
                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return data == null ? null : data.getBytes("utf-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                            return null;
                        }
                    }
                };
                int socketTimeout = 30000;//30 seconds - change to what you want
                RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                stringRegquest.setRetryPolicy(policy);
                peticion.add(stringRegquest);
            } else {
                alertDialog("Campos incompletos","Debes completar todos los campos Usuario y constreña", false);
            }
        } else {
            alertDialog("Terminos y condiciones","Debes aceptar los terminos y condiciones para poder hacer uso de la aplicacion ABG Passport.", false);
        }
    }

    private void alertDialog(String titulo, String msg, boolean dismiss) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(titulo);
        builder.setMessage(msg);

        builder.setPositiveButton("Aceptar", null);
        if ( dismiss){
            builder.setNegativeButton("Cancelar", null);
        }
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void goToCliente(){
        Intent intent = new Intent(login.this, MainActivity.class);
        startActivity(intent);
    }

    public void goToChofer(){
        Intent intent = new Intent(login.this, mainChofer.class);
        startActivity(intent);
    }

}



package com.softernium.passportABG;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.badge.BadgeDrawable;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.softernium.passportABG.utils.NukeSSLCerts;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class mainChofer extends AppCompatActivity {

    Toolbar toolbar;
    int PERMISSION_ID = 44;
    private SharedPreferences pref;
    BadgeDrawable badge;
    private Boolean control = true;
    public boolean allowBackNav = false;
    Fragment selectedFragment = null;
    public boolean permisosConcedidos = false;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home_chofer:
                    selectedFragment = new choferInicio();
                    getSupportActionBar().setTitle("Inicio");
                    break;
                case R.id.navigation_dashboard_chofer:
                    if (permisosConcedidos){
                        selectedFragment = new choferMapa();
                        getSupportActionBar().setTitle("Entrega");
                    } else {
                        consultarPermisosUbicacion();
                    }
                    break;
                case R.id.navigation_notifications_chofer:
                    selectedFragment = new choferNotificaciones();
                    getSupportActionBar().setTitle("Notificaciones");
                    badge.setVisible(false,true);
                    break;
                case R.id.navigation_configuracion_chofer:
                    selectedFragment = new choferMenu();
                    getSupportActionBar().setTitle("Menú");
                    break;
            }
            getSupportFragmentManager().beginTransaction().replace(R.id.fragments_container_chofer, selectedFragment).commit();
            return true;
        }
    };

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new NukeSSLCerts().nuke(this);

        pref = this.getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        setContentView(R.layout.activity_main_chofer);
        Fragment fragmento = new choferInicio();
        getSupportFragmentManager().beginTransaction().replace(R.id.fragments_container_chofer, fragmento).commit();
        BottomNavigationView navView = findViewById(R.id.nav_view_chofer);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        selectedFragment = new choferInicio();
        toolbar = findViewById(R.id.toolbar);
        getSupportActionBar().show();
        getSupportActionBar().setTitle("Inicio");
        int menuItemId = navView.getMenu().getItem(2).getItemId();
        this.badge = navView.getOrCreateBadge(menuItemId);
        this.badge.setVisible(false,true);
        //badge.setNumber(2);
        if (checkPermissions()) {
            if (isLocationEnabled()) {
                permisosConcedidos = true;
            }
        }
    }


    @Override
    public void onBackPressed() {
        if (shouldAllowBack()) {
            super.onBackPressed();
            this.allowBackNav =  false;
            startActivity(new Intent(this, louncher.class));
            finish();
        } else {
            this.allowBackNav = true;
            Toast.makeText(this, "Presiona de nuevo para salir", Toast.LENGTH_SHORT).show();
            // No navegar
        }
    }

    public boolean shouldAllowBack(){
        return this.allowBackNav;
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.obtenerNotificaciones();
    }
    public void timeOut(int timeInterval){
        new android.os.Handler().postDelayed(
            new Runnable() {
                public void run() {
                    if(control){
                        obtenerNotificaciones();
                    }
                }
            }, timeInterval
        );
    }

    @Override
    protected void onPause() {
        super.onPause();
        control = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        control = false;
    }

    public void obtenerNotificaciones(){
        // Peticion Post
        final ConstantesGlobales direcciones = new ConstantesGlobales();
        RequestQueue peticion = Volley.newRequestQueue(this);
        String url = direcciones.getApi() +""+ direcciones.getNot();

        final String data = "{"+
                " \"kndUser\": \""+ pref.getInt("kndUser", 0) + "\", " +
                " \"usrId\":\"" + pref.getInt("nIDCliente", 0) +"\", " +
                " \"option\": 1" +
                "}";
        final Map<String, String>[] datos = new Map[]{new HashMap<String, String>()};
        StringRequest stringRegquest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray json = new JSONArray( response );
                    //alertDialog("",""+json,false);
                    Log.i("JSONNotificaciones",json.toString());
                    if (json.getJSONObject(0).getBoolean("status")){
                        //notificaciones = json;
                        //Hay notificaciones
                        int contador = 0;
                        for (int i = 0; i < json.length(); i++){
                            JSONObject obj = json.getJSONObject(i);
                            if(!obj.getString("Estatus").equals("LEIDO")){
                                contador ++ ;
                            }
                        }
                        if (contador != 0){
                            badge.setVisible(true,true);
                            badge.setNumber(contador);
                        } else {
                            badge.setVisible(false,true);
                        }
                    } else {
                        badge.setVisible(false,true);
                        //Sin notificaciones
                    }
                    if(control){
                        timeOut(15000);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Error de respuesta","Error");
                error.printStackTrace();
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        peticion.add(stringRegquest);
    }

    public void consultarPermisosUbicacion(){
        if (checkPermissions()) {
            if (isLocationEnabled()) {
                //Toast.makeText(this.getActivity(), "Permisos Concedidos", Toast.LENGTH_SHORT).show();
                permisosConcedidos = true;

            } else {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        } else {
            requestPermissions();
        }
    }


    private boolean checkPermissions(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            return true;
        }
        return false;
    }

    private void requestPermissions(){
        ActivityCompat.requestPermissions(
                this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_ID
        );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_ID) {
            if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                // Granted. Start getting the location information
                irMapa();
            }
        }
    }
    private boolean isLocationEnabled(){
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
                LocationManager.NETWORK_PROVIDER
        );
    }

    public void irMapa(){
        permisosConcedidos = true;
    }

}

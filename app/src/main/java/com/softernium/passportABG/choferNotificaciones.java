package com.softernium.passportABG;


import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.softernium.passportABG.utils.NukeSSLCerts;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import dmax.dialog.SpotsDialog;


/**
 * A simple {@link Fragment} subclass.
 */
public class choferNotificaciones extends Fragment {

    private ListView tabla;
    private Boolean control;

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    private JSONArray notificaciones;
    AlertDialog nDialog;

    public choferNotificaciones() {
        // Required empty public constructor
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new NukeSSLCerts().nuke(this.getContext());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_chofer_notificaciones, container, false);
    }

    @Override
    public void onPause() {
        super.onPause();
        control = false;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tabla = getActivity().findViewById(R.id.tablaNotificacionesChofer);
        control = true;
        pref = choferNotificaciones.this.getActivity().getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        editor = pref.edit();
        nDialog = new SpotsDialog.Builder().setContext(choferNotificaciones.this.getContext()).setMessage("Cargando...").setTheme(R.style.Custom).build();

        obtenerNotificaciones();
    }
    private void crearTabla(){
        notificacionesAdapter adapterNew = new notificacionesAdapter(getActivity(), R.layout.notificaciones_adapter, notificaciones);
        tabla.setAdapter(adapterNew);
        tabla.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int posision, long id) {
                try {
                    if(!"-1".equals(notificaciones.getJSONObject(posision).getString("nIDNotificacionesChofer"))){
                        notificacionLeida(notificaciones.getJSONObject(posision).getString("nIDNotificacionesChofer"));
                    } else {
                        //Toast.makeText(notificacionesFragmento.this.getContext(), "No leer notificacion", Toast.LENGTH_SHORT).show();
                    }
                    //Toast.makeText(choferNotificaciones.this.getContext(), ""+notificaciones.getJSONObject(posision), Toast.LENGTH_SHORT).show();

                    notificaciones.getJSONObject(posision).put("Estatus","LEIDO");
                    alertDialog(notificaciones.getJSONObject(posision).getString("Encabezado"),""+ notificaciones.getJSONObject(posision).getString("Body"), true);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                notificacionesAdapter adapterNew = new notificacionesAdapter(getActivity(), R.layout.notificaciones_adapter, notificaciones);
                tabla.setAdapter(adapterNew);
                tabla.setSelection(posision);
            }
        });

    }
    private void alertDialog(String titulo, String msg, boolean dismiss) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(titulo);
        builder.setMessage(msg);

        builder.setPositiveButton("Aceptar", null);
        if ( dismiss){
            builder.setNegativeButton("Cancelar", null);
        }
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    public void timeOut(int timeInterval){
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        if(control){
                            obtenerNotificaciones();
                        }
                    }
                }, timeInterval);
    }

    public void obtenerNotificaciones(){
        // Peticion Post
        nDialog.show();
        final ConstantesGlobales direcciones = new ConstantesGlobales();
        RequestQueue peticion = Volley.newRequestQueue(choferNotificaciones.this.getActivity());
        String url = direcciones.getApi() +""+ direcciones.getNot();

        final String data = "{"+
                " \"kndUser\": \""+ pref.getInt("kndUser", 0) + "\", " +
                " \"usrId\":\"" + pref.getInt("nIDCliente", 0) +"\", " +
                " \"option\": 1," +
                " \"notificacionesTotalesLeidas\": 1" +
                "}";
        final Map<String, String>[] datos = new Map[]{new HashMap<String, String>()};

        StringRequest stringRegquest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    nDialog.dismiss();
                    JSONArray json = new JSONArray( response );

                    if (json.getJSONObject(0).getBoolean("status")){
                        notificaciones = json;
                        //Log.i("JSON",json.toString());
                        crearTabla();
                    } else {
                        JSONObject obj = new JSONObject();
                        obj.put("nIDNotificacionesChofer","-1");
                        obj.put("nIDChofer",pref.getInt("nIDCliente", 0));
                        obj.put("nIDTipoNotificacion","-1");
                        obj.put("Encabezado","Sin notificaciones");
                        obj.put("Body","No hay notificaciones vigentes");
                        obj.put("Permiso","SI");
                        obj.put("Estatus","LEIDO");
                        obj.put("TipoNotificacion","POR DEFECTO");
                        obj.put("Descripcion","Se muestra esta notificacion por falta de notificaciones");
                        obj.put("status",true);
                        obj.put("resp","Notificacion por defecto");

                        JSONArray ja = new JSONArray();
                        ja.put(obj);
                        notificaciones = ja;
                        crearTabla();
                        //Toast.makeText(notificacionesFragmento.this.getContext(), "No hay notificaciones.", Toast.LENGTH_LONG).show();
                    }
                    if(control){
                        timeOut(30000);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                nDialog.dismiss();
                Log.i("Error de respuesta","Error");
                error.printStackTrace();
            }
        }){

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        peticion.add(stringRegquest);

    }


    public void notificacionLeida(String id){
        // Peticion Post
        final ConstantesGlobales direcciones = new ConstantesGlobales();
        RequestQueue peticion = Volley.newRequestQueue(choferNotificaciones.this.getActivity());
        String url = direcciones.getApi() +""+ direcciones.getNot();

        final String data = "{"+
                " \"kndUser\": \""+ pref.getInt("kndUser", 0) + "\", " +
                " \"usrId\":\"" + pref.getInt("nIDCliente", 0) +"\", " +
                " \"option\": 2," +
                " \"idNotificacion\":" + id +
                "}";

        final Map<String, String>[] datos = new Map[]{new HashMap<String, String>()};
        //Toast.makeText(notificacionesFragmento.this.getContext(), ""+data, Toast.LENGTH_SHORT).show();
        StringRequest stringRegquest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    nDialog.dismiss();
                    //Log.i("Respuesta",""+response);
                    JSONArray json = new JSONArray( response );


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Error de respuesta","Error");
                error.printStackTrace();
            }
        }){

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        peticion.add(stringRegquest);
    }


}

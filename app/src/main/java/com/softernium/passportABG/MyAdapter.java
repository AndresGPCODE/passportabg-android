package com.softernium.passportABG;

    import android.content.Context;
    import android.view.LayoutInflater;
    import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
    import android.widget.ImageView;
    import android.widget.TextView;

    import org.json.JSONArray;
    import org.json.JSONException;
    import org.json.JSONObject;

public class MyAdapter extends BaseAdapter {


    private Context context;
    private int layout;
    private JSONArray list;


    public MyAdapter(Context context, int layout, JSONArray list){
        this.context = context;
        this.layout = layout;
        this.list = list;

    }

    @Override
    public int getCount() {
        return this.list.length();
    }

    @Override
    public Object getItem(int i) {
        try {
            return this.list.getJSONObject(i);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        View v = convertView;
        LayoutInflater inflate = LayoutInflater.from(this.context);
        v = inflate.inflate(R.layout.list_item_vehiculo, null, false);

        JSONObject currentItem = (JSONObject) getItem(position);

        TextView  modeloText = (TextView) v.findViewById(R.id.modeloVehiculoList);
        TextView  submarcaText = (TextView) v.findViewById(R.id.submarcaVehiculoList);
        TextView  marcaText = (TextView) v.findViewById(R.id.marcaVehiculoList);
        TextView  stockText = (TextView) v.findViewById(R.id.stockVehiculoList);
        ImageView imageVehiculo = (ImageView) v.findViewById(R.id.imageVehiculoList);

        ConstantesGlobales util = new ConstantesGlobales();
        if (getCount() <= 0){
            modeloText.setText("Sin vehiculos");
        } else {
            try {
                modeloText.setText(currentItem.getString("Modelo"));
                marcaText.setText(currentItem.getString("Marca"));
                submarcaText.setText(currentItem.getString("Linea_Submarca"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                if (currentItem.getString("Imagen").equals("0,0")){
                    imageVehiculo.setImageResource(R.drawable.nif);
                    stockText.setText("");
                } else {
                    String[] img = currentItem.getString("Imagen").split(",");
                    imageVehiculo.setImageBitmap(util.stringtoScaleImage(img[1], 246, 400));
                    stockText.setText("Disponible");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        return v;

    }
}

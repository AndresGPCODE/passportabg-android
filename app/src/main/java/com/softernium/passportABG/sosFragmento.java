package com.softernium.passportABG;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.softernium.passportABG.utils.NukeSSLCerts;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import static androidx.core.content.ContextCompat.checkSelfPermission;

public class sosFragmento extends Fragment {
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private TextView nombre, numero;
    private Button averia, incidente, polizasSeguro, contrato, selectCommand;
    FusedLocationProviderClient mFusedLocationClient;
    String lat,lon;
    int PERMISSION_ID = 44;
    final int SEND_SMS_PERMISSION_REQUEST_CODE = 1;
    private RequestQueue requestQueue;
    String id = "0";
    ConstantesGlobales api = new ConstantesGlobales();
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(sosFragmento.this.getActivity());
        return inflater.inflate(R.layout.fragmento_sos,container,false);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new NukeSSLCerts().nuke(this.getContext());
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pref = sosFragmento.this.getActivity().getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        editor = pref.edit();
        nombre = getActivity().findViewById(R.id.nombreContacto);
        numero = getActivity().findViewById(R.id.numeroContacto);
        nombre.setText(pref.getString("contNombre","Sin contacto"));
        numero.setText(pref.getString("contTelefono","Sin numero"));
        averia = getActivity().findViewById(R.id.averiaAlert);
        incidente = getActivity().findViewById(R.id.incidenteAlert);
        polizasSeguro = getActivity().findViewById(R.id.goToPoliza);
        contrato = getActivity().findViewById(R.id.goToContrato);
//        selectCommand = getActivity().findViewById(R.id.commandSelect);
        getDispositivo();
        polizasSeguro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                obtenerPoliza();
            }
        });
        contrato.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                obtenerContrato();
            }
        });
        averia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(sosFragmento.this.getContext(), "averia en: "+lat+" / "+lon, Toast.LENGTH_SHORT).show();
                if(!id.equals("0")){
                    if(pref.getString("contTelefono","0") != "0"){
                        sendSMS(pref.getString("contTelefono","0"), "Averia");
                    }
                    //sendSMS("3335784503", "Averia");
                    enviarReporte("2");
                } else {
                    Toast.makeText(sosFragmento.this.getContext(), "No disponible hasta reservar.", Toast.LENGTH_LONG).show();
                }

            }
        });
        incidente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(sosFragmento.this.getContext(), "incidente en: "+lat+" / "+lon, Toast.LENGTH_SHORT).show();
                if(!id.equals("0")){
                    if(pref.getString("contTelefono","0") != "0"){
                        sendSMS(pref.getString("contTelefono","0"), "Incidente");
                    }
                    enviarReporte("1");
                } else {
                    Toast.makeText(sosFragmento.this.getContext(), "No disponible hasta reservar.", Toast.LENGTH_LONG).show();
                }

            }
        });
//        selectCommand.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                selectCommando();
//            }
//        });

        getLastLocation();
        obtenerContactoEmergencia();
    }
    public void goPoliza(String Archivo){
        Intent intent = new Intent(sosFragmento.this.getContext(), webPolizaContratos.class);
        intent.putExtra("TipoDoc","Póliza");
        intent.putExtra("url",Archivo);
        startActivity(intent);
    }
    public void goContrato(String Archivo){
        Intent intent = new Intent(sosFragmento.this.getContext(), webPolizaContratos.class);
        intent.putExtra("TipoDoc","Contrato");
        intent.putExtra("url",Archivo);
        startActivity(intent);
    }
    private void alertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(sosFragmento.this.getContext());
        builder.setTitle("Ver documento");
        builder.setMessage("");

        builder.setPositiveButton("Poliza", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                obtenerPoliza();
            }
        });
        builder.setNegativeButton("Contrato", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                obtenerContrato();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    private void alertMsg(String titulo, String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(sosFragmento.this.getContext());
        builder.setTitle(titulo);
        builder.setMessage(msg);

        builder.setPositiveButton("Aceptar", null);
        //builder.setNegativeButton("Contrato", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    public void selectCommando(){
        if (id == "0"){
            Toast.makeText(sosFragmento.this.getContext(), "Obteniendo comandos de control", Toast.LENGTH_LONG).show();
            getDispositivo();
            return;
        }
        String[] opt = {"Apagar", "Encender", "Reiniciar", "Borrar records"};
//        String[] opt = {"Apagar", "Encender", "Reiniciar", "Borrar records", "Rastrear vehiculo"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Opciones de motor.").setItems(opt, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            // The 'which' argument contains the index position of the selected item
            switch (which){
                case 0:
                    //Apagar
                    //sendSMSCortarCorriente(id);
                    saveInDB(id, "CORTAR CORRIENTE", "0");
                    Toast.makeText(sosFragmento.this.getContext(), "Apagando vehiculo.", Toast.LENGTH_SHORT).show();
                    break;
                case 1:
                    //Encender
                    //sendSMSReconectarCorriente(id);
                    saveInDB(id, "ENCENDER", "0");
                    Toast.makeText(sosFragmento.this.getContext(), "Encendiendo vehiculo.", Toast.LENGTH_SHORT).show();
                    break;
                case 2:
                    //Reiniciar
                    //sendSMSReset(id);
                    saveInDB(id, "REINICIAR", "0");
                    Toast.makeText(sosFragmento.this.getContext(), "Reiniciando vehiculo.", Toast.LENGTH_SHORT).show();
                    break;
                case 3:
                    //Delrecords
                    //sendSMSDelrecords(id);
                    saveInDB(id, "DELRECORDS", "0");
                    Toast.makeText(sosFragmento.this.getContext(), "Eliminando registro de cords del GPS.", Toast.LENGTH_SHORT).show();
                    break;
                case 4:
                    //Geolocalizacion del vehiculo por comando SMS, redireccion a la vista de rastreo
                    Intent intent = new Intent(sosFragmento.this.getContext(), rastreoVehiculo.class);
                    intent.putExtra("idDispositivo",""+id);
                    startActivity(intent);
                    break;
                default:
                    break;
            }
            }
        });
        builder.create();
        builder.show();
    }
//    private void permisosSMS() {
//         //Permiso para gestionar la recepcion de sms, bloqueado por temas de seguridad en google play
////         if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(getContext(), Manifest.permission.RECEIVE_SMS )
////                != PackageManager.PERMISSION_GRANTED ) {
////            requestPermissions( new String[]{ Manifest.permission.RECEIVE_SMS }, 1000 );
////         }
//        //Toast.makeText(this.getContext(), "checkPermissions(Manifest.permission.SEND_SMS)", Toast.LENGTH_SHORT).show();
//        if(!checkPermissions(Manifest.permission.SEND_SMS)){
//           ActivityCompat.requestPermissions(sosFragmento.this.getActivity(), new String[]{Manifest.permission.SEND_SMS}, SEND_SMS_PERMISSION_REQUEST_CODE);
//        } else {
//        }
//    }
    public Boolean checkPermissions(String permission){
        int check = checkSelfPermission(sosFragmento.this.getActivity(), permission);

        return (check == PackageManager.PERMISSION_GRANTED);
    }
    @Override
    public void onResume() {
        super.onResume();
        getLastLocation();
    }
    private boolean checkPermissions(){
        if (checkSelfPermission(this.getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                checkSelfPermission(this.getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            return true;
        }
        return false;
    }
    private boolean isLocationEnabled(){
        LocationManager locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }
    @SuppressLint("MissingPermission")
    private void getLastLocation(){
        if (checkPermissions()) {
            if (isLocationEnabled()) {
                mFusedLocationClient.getLastLocation().addOnCompleteListener(
                    new OnCompleteListener<Location>() {
                        @Override
                        public void onComplete(@NonNull Task<Location> task) {
                        Location location = task.getResult();
                            if (location == null) {
                                requestNewLocationData();
                            } else {
                                lat = ""+location.getLatitude();
                                lon = ""+location.getLongitude();
                                //Toast.makeText(sosFragmento.this.getContext(), lat+" / "+lon, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                );
            } else {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        } else {
            requestPermissions();
            getLastLocation();
        }
    }
    @SuppressLint("MissingPermission")
    private void requestNewLocationData(){
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(0);
        mLocationRequest.setFastestInterval(0);
        mLocationRequest.setNumUpdates(1);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(sosFragmento.this.getActivity());
        mFusedLocationClient.requestLocationUpdates(
                mLocationRequest, mLocationCallback,
                Looper.myLooper()
        );
    }
    private LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            Location mLastLocation = locationResult.getLastLocation();
        }
    };
    private void requestPermissions(){
        ActivityCompat.requestPermissions(
                sosFragmento.this.getActivity(), new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_ID
        );
    }

    public void enviarReporte(String opt){
        // Peticion Post
        final ConstantesGlobales direcciones = new ConstantesGlobales();
        RequestQueue peticion = Volley.newRequestQueue(sosFragmento.this.getContext());
        String url = direcciones.getApi() +""+ direcciones.getRep();

        final String data = "{"+
                " \"idCliente\": \""+ pref.getInt("nIDCliente", 0) + "\", " +
                " \"Latitud\": \""+lat+"\", " +
                " \"Longitud\":\""+lon+"\", " +
                " \"option\": \""+opt+"\"" +
                "}";

        StringRequest stringRegquest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject json = new JSONObject( response );
                    if(json.getBoolean("status")){
                        //Toast.makeText(sosFragmento.this.getContext(), "Reporte enviado", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(sosFragmento.this.getContext(), "Problemas de conexion...", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Error de respuesta","Error");
                error.printStackTrace();
            }
        }){

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        peticion.add(stringRegquest);

    }
    public void sendSMS(String numero, String tipo){
        String msg = "Hola "+pref.getString("contNombre",",un contacto") +", "+pref.getString("nombreCliente",",un contacto") + " esta reportando " +tipo+ " esta es su ubicacion: "+lat+","+lon+" contactalo. https://maps.google.com/?q="+lat+","+lon;
        saveInDB("0", msg, numero);
//        permisosSMS();
//        if(checkPermissions(Manifest.permission.SEND_SMS)){
//            SmsManager smsMana = SmsManager.getDefault();
//            smsMana.sendTextMessage(numero, null, "Hola "+pref.getString("contNombre",",un contacto") +" esta reportando " +tipo+ " esta es su ubicacion: "+lat+","+lon+" contactalo. https://maps.google.com/?q="+lat+","+lon, null, null);
//            String msg = "Hola "+pref.getString("contNombre",",un contacto") +" esta reportando " +tipo+ " esta es su ubicacion: "+lat+","+lon+" contactalo. https://maps.google.com/?q="+lat+","+lon;
//            saveInDB(id, msg, numero);
//            Toast.makeText(sosFragmento.this.getContext(), "Mensaje enviado", Toast.LENGTH_SHORT).show();
//        }else {
//            //sendSMS(numero,tipo);
//            Toast.makeText(sosFragmento.this.getContext(), "Mensaje no enviado, por falta de permisos sms.", Toast.LENGTH_SHORT).show();
//        }
    }
    public void getDispositivo(){
        // Peticion Post
        final ConstantesGlobales direcciones = new ConstantesGlobales();
        RequestQueue peticion = Volley.newRequestQueue(sosFragmento.this.getContext());
        String url = direcciones.getApi() +""+ direcciones.getComm();
        final String data = "{"+
                " \"idCliente\": \""+ pref.getInt("nIDCliente", 0) + "\", " +
                " \"option\": 1" +
                "}";
        final Map<String, String>[] datos = new Map[]{new HashMap<String, String>()};
        StringRequest stringRegquest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject( response );
                    String thisId = json.getString("nIDDispositivoAuto");
                    //alertMsg("id",""+json);
                    if(!thisId.equals("") && thisId != null && !thisId.equals("0")){
                        id = thisId;
                    }else{
                        id = "0";
                        Toast.makeText(sosFragmento.this.getContext(), "No hay dispositivo GPS vinculado", Toast.LENGTH_LONG).show();
                    }
                    //sendSMS(id);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Error de respuesta","Error");
                error.printStackTrace();
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        int socketTimeout = 10000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRegquest.setRetryPolicy(policy);
        peticion.add(stringRegquest);
    }
    public void sendSMSCortarCorriente(String id){

        requestQueue = Volley.newRequestQueue( sosFragmento.this.getContext() );
        final String ids = id;
        String urlcut = api.getUrl() + api.getContr() +"Comandos.php?nIDDispositivoAuto="+ids+"&Accion=CORTAR%20CORRIENTE";
        //Toast.makeText(sosFragmento.this.getContext(), ""+id, Toast.LENGTH_LONG).show();
        //Log.i("sendSMS",urlcut);
        StringRequest requestcut = new StringRequest( Request.Method.GET, urlcut, new Response.Listener<String>() {
            @Override
            public void onResponse( String response ) {
                try {
                    //Toast.makeText(sosFragmento.this.getContext(), response, Toast.LENGTH_LONG).show();
                    JSONArray jsonArray = new JSONArray( response );
                    String phone     = jsonArray.getJSONObject(0).getString("Celular");
                    String command   = jsonArray.getJSONObject(0).getString("Comando");
                    String password  = jsonArray.getJSONObject(0).getString("Password");
                    String separator = jsonArray.getJSONObject(0).getString("Separador");
                    String param1    = jsonArray.getJSONObject(0).getString("Parametro1");
                    //String message   =  " " + password + separator + command + separator + "0" ;
                    //String message   =  password + " " + command ;
                    String message;
                    if(password != "" && password != null && password != "null"){
                        message   =  password + " "  + command  ;
                    }else{
                        message   =  "" + command  ;
                    }
                    message   =  "" + command  ;
//                    if(checkPermissions(Manifest.permission.SEND_SMS)){
//                        SmsManager smsMana = SmsManager.getDefault();
//                        smsMana.sendTextMessage(phone, null, message, null, null);
//                        //alertMsg("Respuesta",message);
//                        Toast.makeText(sosFragmento.this.getContext(), "Mensaje Enviado Exitosamente", Toast.LENGTH_SHORT).show();
                        saveInDB(ids, "CORTAR CORRIENTE", "0");
//                    }else {
//                        Toast.makeText(sosFragmento.this.getContext(), "Mensaje no enviado", Toast.LENGTH_SHORT).show();
//                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(sosFragmento.this.getContext(), "Mensaje no enviado", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse( VolleyError error ) {
                error.printStackTrace();
            }
        });
        int socketTimeout = 10000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        requestcut.setRetryPolicy(policy);
        requestQueue.add( requestcut );
    }
    public void sendSMSReconectarCorriente(String id){

        requestQueue = Volley.newRequestQueue( sosFragmento.this.getContext() );
        final String ids = id;
        String urlcut = api.getUrl() + api.getContr() +"Comandos.php?nIDDispositivoAuto="+ids+"&Accion=ENCENDER";
        //Toast.makeText(sosFragmento.this.getContext(), ""+id, Toast.LENGTH_LONG).show();
        //Log.i("sendSMS",urlcut);
        StringRequest requestcut = new StringRequest( Request.Method.GET, urlcut, new Response.Listener<String>() {
            @Override
            public void onResponse( String response ) {
                try {
                    //Toast.makeText(sosFragmento.this.getContext(), response, Toast.LENGTH_LONG).show();
                    JSONArray jsonArray = new JSONArray( response );
                    //alertMsg("jsonArray",""+jsonArray);
                    String phone     = jsonArray.getJSONObject(0).getString("Celular");
                    String command   = jsonArray.getJSONObject(0).getString("Comando");
                    String password  = jsonArray.getJSONObject(0).getString("Password");
                    String separator = jsonArray.getJSONObject(0).getString("Separador");
                    String param1    = jsonArray.getJSONObject(0).getString("Parametro1");
                    //String message   =  " " + password + separator + command + separator + "0" ;
                    //String message   =  password + " " + command ;
                    String message;
                    if(password != "" && password != null && password != "null"){
                        message   =  password + " "  + command  ;
                    }else{
                        message   =  "" + command  ;
                    }
                    message   =  "" + command  ;
//                    if(checkPermissions(Manifest.permission.SEND_SMS)){
//                        SmsManager smsMana = SmsManager.getDefault();
//                        smsMana.sendTextMessage(phone, null, message, null, null);
//                        //alertMsg("Respuesta",message);
//                        Toast.makeText(sosFragmento.this.getContext(), "Mensaje Enviado Exitosamente", Toast.LENGTH_SHORT).show();
                        saveInDB(ids, "ENCENDER", "0");
//                    }else {
//                        Toast.makeText(sosFragmento.this.getContext(), "Mensaje no enviado", Toast.LENGTH_SHORT).show();
//                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(sosFragmento.this.getContext(), "Mensaje no enviado", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse( VolleyError error ) {
                error.printStackTrace();
            }
        });
        int socketTimeout = 10000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        requestcut.setRetryPolicy(policy);
        requestQueue.add( requestcut );
    }
    public void sendSMSReset(String id){

        requestQueue = Volley.newRequestQueue( sosFragmento.this.getContext() );
        final String ids = id;
        String urlcut = api.getUrl() + api.getContr() +"Comandos.php?nIDDispositivoAuto="+ids+"&Accion=REINICIAR";
        //Toast.makeText(sosFragmento.this.getContext(), ""+id, Toast.LENGTH_LONG).show();
        //Log.i("sendSMS",urlcut);
        StringRequest requestcut = new StringRequest( Request.Method.GET, urlcut, new Response.Listener<String>() {
            @Override
            public void onResponse( String response ) {
                try {
                    //Toast.makeText(sosFragmento.this.getContext(), response, Toast.LENGTH_LONG).show();
                    JSONArray jsonArray = new JSONArray( response );
                    //alertMsg("jsonArray",""+jsonArray);
                    String phone     = jsonArray.getJSONObject(0).getString("Celular");
                    String command   = jsonArray.getJSONObject(0).getString("Comando");
                    String password  = jsonArray.getJSONObject(0).getString("Password");
                    String separator = jsonArray.getJSONObject(0).getString("Separador");
                    String param1    = jsonArray.getJSONObject(0).getString("Parametro1");
                    //String message   =  " " + password + separator + command + separator + "0" ;
                    //String message   =  password + " " + command ;
                    String message;
                    if(password != "" && password != null && password != "null"){
                        message   =  password + " "  + command  ;
                    }else{
                        message   =  "" + command  ;
                    }
                    message   =  "" + command  ;
//                    if(checkPermissions(Manifest.permission.SEND_SMS)){
//                        SmsManager smsMana = SmsManager.getDefault();
//                        smsMana.sendTextMessage(phone, null, message, null, null);
//                        //alertMsg("Respuesta",message);
//                        Toast.makeText(sosFragmento.this.getContext(), "Mensaje Enviado Exitosamente", Toast.LENGTH_SHORT).show();
                        saveInDB(ids, "Reiniciar", "0");
//                    }else {
//                        Toast.makeText(sosFragmento.this.getContext(), "Mensaje no enviado", Toast.LENGTH_SHORT).show();
//                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(sosFragmento.this.getContext(), "Mensaje no enviado", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse( VolleyError error ) {
                error.printStackTrace();
            }
        });
        int socketTimeout = 10000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        requestcut.setRetryPolicy(policy);
        requestQueue.add( requestcut );
    }
    public void sendSMSDelrecords(String id){
        requestQueue = Volley.newRequestQueue( sosFragmento.this.getContext() );
        final String ids = id;
        String urlcut = api.getUrl() + api.getContr() +"Comandos.php?nIDDispositivoAuto="+ids+"&Accion=DELRECORDS";
        //Toast.makeText(sosFragmento.this.getContext(), ""+id, Toast.LENGTH_LONG).show();
        //Log.i("sendSMS",urlcut);
        StringRequest requestcut = new StringRequest( Request.Method.GET, urlcut, new Response.Listener<String>() {
            @Override
            public void onResponse( String response ) {
                try {
                    //Toast.makeText(sosFragmento.this.getContext(), response, Toast.LENGTH_LONG).show();
                    JSONArray jsonArray = new JSONArray( response );
                    //alertMsg("jsonArray",""+jsonArray);
                    String phone     = jsonArray.getJSONObject(0).getString("Celular");
                    String command   = jsonArray.getJSONObject(0).getString("Comando");
                    String password  = jsonArray.getJSONObject(0).getString("Password");
                    String separator = jsonArray.getJSONObject(0).getString("Separador");
                    String param1    = jsonArray.getJSONObject(0).getString("Parametro1");
                    //String message   =  " " + password + separator + command + separator + "0" ;
                    //String message   =  password + " " + command ;
                    String message;
                    if(password != "" && password != null && password != "null"){
                        message   =  password + " "  + command  ;
                    }else{
                        message   =  "" + command  ;
                    }
                    message   =  "" + command  ;
//                    if(checkPermissions(Manifest.permission.SEND_SMS)){
//                        SmsManager smsMana = SmsManager.getDefault();
//                        smsMana.sendTextMessage(phone, null, message, null, null);
//                        //alertMsg("Respuesta",message);
//                        Toast.makeText(sosFragmento.this.getContext(), "Mensaje Enviado Exitosamente", Toast.LENGTH_SHORT).show();
                        saveInDB(ids, "DELRECORDS", "0");
//                    }else {
//                        Toast.makeText(sosFragmento.this.getContext(), "Mensaje no enviado", Toast.LENGTH_SHORT).show();
//                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(sosFragmento.this.getContext(), "Mensaje no enviado", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse( VolleyError error ) {
                error.printStackTrace();
            }
        });
        int socketTimeout = 10000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        requestcut.setRetryPolicy(policy);
        requestQueue.add( requestcut );
    }
    public void sendSMSGeo(final String id){
        requestQueue = Volley.newRequestQueue( sosFragmento.this.getContext() );
        final String ids = id;
        String urlcut = api.getUrl() + api.getContr() +"Comandos.php?nIDDispositivoAuto="+ids+"&Accion=GEOLOCALIZACION";
        StringRequest requestcut = new StringRequest( Request.Method.GET, urlcut, new Response.Listener<String>() {
            @Override
            public void onResponse( String response ) {
                try {
                    JSONArray jsonArray = new JSONArray( response );
                    String phone     = jsonArray.getJSONObject(0).getString("Celular");
                    String command   = jsonArray.getJSONObject(0).getString("Comando");
                    String password  = jsonArray.getJSONObject(0).getString("Password");
                    String separator = jsonArray.getJSONObject(0).getString("Separador");
                    String param1    = jsonArray.getJSONObject(0).getString("Parametro1");

                    //String message   = password + separator + command  ;
                    String message;
                    if(password != ""){
                         message   =  password + " "  + command  ;
                    }else{
                         message   =  "" + command  ;
                    }

//                    if(checkPermissions(Manifest.permission.SEND_SMS)){
//                        SmsManager smsMana = SmsManager.getDefault();
//                        //smsMana.sendTextMessage(phone, null, " records ", null, null);
//                        smsMana.sendTextMessage(phone, null, message, null, null);
//                        //alertDialog("Respuesta",message,false);
//                        Toast.makeText(sosFragmento.this.getContext(), "Mensaje Enviado Exitosamente", Toast.LENGTH_SHORT).show();
//                        saveInDB(ids, "GEOLOCALIZACION");
//                    }else {
//                        Toast.makeText(sosFragmento.this.getContext(), "Mensaje no enviado", Toast.LENGTH_SHORT).show();
//                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(sosFragmento.this.getContext(), "Mensaje no enviado", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse( VolleyError error ) {
                error.printStackTrace();
            }
        });
        requestQueue.add( requestcut );
    }
    public void saveInDB(String id, String accion, String telefono){
        final String cut = "{ \"nIDAccion\": 0," +
                " \"nIDDispositivoAuto\": "+id+"," +
                " \"Accion\": \""+accion+"\"," +
                " \"Telefono\": \""+telefono+"\"," +
                " \"Estatus\": \"NO PROCESADA\" }";
        final String savedata = cut;
        String URL = api.getUrl() + api.getContr() +"Acciones.php";

        requestQueue = Volley.newRequestQueue( sosFragmento.this.getContext() );

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("Respuesta", "" + ("INSERTED".equals(response)));
                Log.i("Respuesta", "" + (response.equals("INSERTED")));
                Toast.makeText(sosFragmento.this.getContext(), "Enviado", Toast.LENGTH_LONG).show();

//                if("INSERTED".equals(response)){
//                    Toast.makeText(sosFragmento.this.getContext(), "Enviado", Toast.LENGTH_LONG).show();
//                } else {
//                    Toast.makeText(sosFragmento.this.getContext(), "No enviado", Toast.LENGTH_LONG).show();
//                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText( sosFragmento.this.getContext(), error.getMessage(), Toast.LENGTH_SHORT ).show();
            }
        }) {
            @Override
            public String getBodyContentType() { return "application/json; charset=utf-8"; }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return savedata == null ? null : savedata.getBytes("utf-8" );
                } catch ( UnsupportedEncodingException uee){
                    return null;
                }
            }
        };

        requestQueue.add( stringRequest );
    }

    public void obtenerContactoEmergencia(){
        // Peticion Post
        final ConstantesGlobales direcciones = new ConstantesGlobales();
        RequestQueue peticion = Volley.newRequestQueue(sosFragmento.this.getContext());
        String url = direcciones.getApi() +""+ direcciones.getUsr();
        final String data = "{"+
                " \"nIDCliente\": \""+ pref.getInt("nIDCliente", 0) + "\", " +
                " \"option\": \"2\"" +
                "}";
        StringRequest stringRegquest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    //Toast.makeText(getActivity(), response, Toast.LENGTH_LONG).show();
                    JSONObject json = new JSONObject( response );
                    if(json.getBoolean("statusContacto")){
                        nombre.setText(json.getString("contNombre"));
                        numero.setText(json.getString("contTelefono"));
                        editor.putString("contNombre",      json.getString("contNombre"));
                        editor.putString("contTelefono",    json.getString("contTelefono"));
                        editor.apply();
                    } else {
                        Toast.makeText(sosFragmento.this.getContext(), "Problemas de actualizacion de contacto", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Error de respuesta","Error");
                error.printStackTrace();
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        peticion.add(stringRegquest);
    }


    public void obtenerContrato(){
        // Peticion Post
        final ConstantesGlobales direcciones = new ConstantesGlobales();
        RequestQueue peticion = Volley.newRequestQueue(sosFragmento.this.getContext());
        String url = direcciones.getApi() +""+ direcciones.getDoc();
        final String data = "{"+
                " \"nIDCliente\": \""+ pref.getInt("nIDCliente", 0) + "\", " +
                " \"option\": \"2\"" +
                "}";
        StringRequest stringRegquest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    //Toast.makeText(getActivity(), response, Toast.LENGTH_LONG).show();
                    JSONArray json = new JSONArray( response );

                    if(json.getJSONObject(0).getBoolean("status")){
                        //Log.i("Documento",direcciones.getUrlDocumentos() + "PDF/" + json.getJSONObject(0).getString("Documento"));
                        String Documento = direcciones.getUrlDocumentos() + "PDF/" + json.getJSONObject(0).getString("Documento");
                        goContrato(Documento);

                    } else {
                        Toast.makeText(sosFragmento.this.getContext(), "No hay contrato registrado", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Error de respuesta","Error");
                error.printStackTrace();
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        peticion.add(stringRegquest);
    }

    public void obtenerPoliza(){
        // Peticion Post
        final ConstantesGlobales direcciones = new ConstantesGlobales();
        RequestQueue peticion = Volley.newRequestQueue(sosFragmento.this.getContext());
        String url = direcciones.getApi() +""+ direcciones.getDoc();
        final String data = "{"+
                " \"nIDCliente\": \""+ pref.getInt("nIDCliente", 0) + "\", " +
                " \"option\": \"1\"" +
                "}";
        StringRequest stringRegquest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    //Toast.makeText(getActivity(), response, Toast.LENGTH_LONG).show();
                    JSONArray json = new JSONArray( response );

                    if(json.getJSONObject(0).getBoolean("status")){
                        Log.i("Documento",direcciones.getUrlDocumentos() + "PDF/" + json.getJSONObject(0).getString("Documento"));
                        String Documento = direcciones.getUrlDocumentos() + "PDF/" + json.getJSONObject(0).getString("Documento");
                        goPoliza(Documento);

                    } else {
                        Toast.makeText(sosFragmento.this.getContext(), "No hay póliza registrado", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Error de respuesta","Error");
                error.printStackTrace();
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        peticion.add(stringRegquest);
    }



}// FIN DE LA CLASE

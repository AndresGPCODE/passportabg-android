package com.softernium.passportABG.utils;

import android.content.Context;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.SecureRandom;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class NukeSSLCerts {

    protected static final String TAG = "NukeSSLCerts";

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static void nuke(final Context context) {

        try {
            TrustManager[] trustAllCerts = new TrustManager[] {
                new X509TrustManager() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    public X509Certificate[] getAcceptedIssuers() {
                        X509Certificate[] myTrustedAnchors = new X509Certificate[1];
                        CertificateFactory cf = null;
                        try {
                            cf = CertificateFactory.getInstance("X.509");
                            InputStream caInput = new BufferedInputStream(context.getAssets().open("abgpassport.crt"));
                            Certificate ca = cf.generateCertificate(caInput);
                            X509Certificate cert = (X509Certificate)cf.generateCertificate(caInput);
                            myTrustedAnchors[0] = cert;
                        } catch (CertificateException | IOException e) {
                            e.printStackTrace();
                        }
                        Log.i("NUKE","getAcceptedIssuers: " + myTrustedAnchors.toString());
                        return myTrustedAnchors;
                    }
                    @Override
                    public void checkClientTrusted(X509Certificate[] certs, String authType) {
                        Log.i("NUKE","checkClientTrusted: " + authType);
                    }
                    @Override
                    public void checkServerTrusted(X509Certificate[] certs, String authType) throws CertificateException {
                        Log.i("NUKE","checkServerTrusted: " + authType);
                        try {
                            certs[0].checkValidity();
                        } catch (Exception e) {
                            throw new CertificateException("Certificate not valid or trusted.");
                        }
                    }
                }
            };
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String arg0, SSLSession arg1) {
                    Log.i("NUKE","VERIFY: " + arg0 + " + " + arg1.toString());
                    if(arg0.equals("abgpassport.com") || arg0.equals("www.abgpassport.com") || arg0.equals("clients4.google.com") || arg0.equals("csi.gstatic.com")){
                        return true;
                    } else {
                        return false;
                    }
                }
            });
        } catch (Exception e) {
        }
        /*try {
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            InputStream caInput = new BufferedInputStream(context.getAssets().open("abgpassport.crt"));
            Certificate ca = cf.generateCertificate(caInput);
            System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
            caInput.close();
            // Create a KeyStore containing our trusted CAs
            String keyStoreType = KeyStore.getDefaultType();
            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
            keyStore.load(null, null);
            keyStore.setCertificateEntry("ca", ca);
            // Create a TrustManager that trusts the CAs in our KeyStore
            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
            tmf.init(keyStore);
            // Create an SSLContext that uses our TrustManager
            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, tmf.getTrustManagers(), null);
            HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
            // Tell the URLConnection to use a SocketFactory from our SSLContext
//            URL url = new URL("https://www.abgpassport.com/CA/");
//            HttpsURLConnection urlConnection =
//                    (HttpsURLConnection)url.openConnection();
//            urlConnection.setSSLSocketFactory(sslContext.getSocketFactory());
//            try (InputStream in = urlConnection.getInputStream()) {
//                IOUtils.copyStream(in, System.out);
//            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }*/
    }
}

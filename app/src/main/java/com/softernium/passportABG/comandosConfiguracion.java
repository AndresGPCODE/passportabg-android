package com.softernium.passportABG;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.softernium.passportABG.utils.NukeSSLCerts;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class comandosConfiguracion extends AppCompatActivity {

    private Button apagarComando;
    final int SEND_SMS_PERMISSION_REQUEST_CODE = 1;
    private RequestQueue requestQueue;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    String id = "0";
    private Boolean estadoVehiculo = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new NukeSSLCerts().nuke(this);

        setContentView(R.layout.activity_comandos_configuracion);
        getSupportActionBar().setTitle("Configuracion");

        apagarComando = findViewById(R.id.apagarBtn);
        pref = comandosConfiguracion.this.getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        editor = pref.edit();
        apagarComando.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //sendSMSCorriente(id);
                estadoVehiculo = !estadoVehiculo;
                if(!estadoVehiculo){
                    apagarComando.setText("Encendar");
                    // apagamos el vehiculo
                } else {
                    apagarComando.setText("Apagar");
                    //encendemos el vehiculo
                }
            }
        });

//        if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission( Manifest.permission.RECEIVE_SMS )
//                != PackageManager.PERMISSION_GRANTED ) {
//            requestPermissions( new String[]{ Manifest.permission.RECEIVE_SMS }, 1000 );
//        }

//        if(checkPermissions(Manifest.permission.SEND_SMS)){
//
//        }else{
//            ActivityCompat.requestPermissions(comandosConfiguracion.this,
//                    new String[]{Manifest.permission.SEND_SMS}, SEND_SMS_PERMISSION_REQUEST_CODE);
//        }
        getDispositivo();
    }

    public Boolean checkPermissions(String permission){
        int check = ContextCompat.checkSelfPermission(comandosConfiguracion.this, permission);
       return (check == PackageManager.PERMISSION_GRANTED);
    }

    public void getDispositivo(){
        // Peticion Post
        final ConstantesGlobales direcciones = new ConstantesGlobales();
        RequestQueue peticion = Volley.newRequestQueue(comandosConfiguracion.this);
        String url = direcciones.getApi() +""+ direcciones.getComm();
        final String data = "{"+
                " \"idCliente\": \""+ pref.getInt("nIDCliente", 0) + "\", " +
                " \"option\": 1" +
                "}";
        final Map<String, String>[] datos = new Map[]{new HashMap<String, String>()};
        StringRequest stringRegquest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject( response );
                    String thisId = json.getString("nIDDispositivoAuto");
                    //alertDialog("id",""+thisId,false);
                    id = thisId;
                    //sendSMS(id);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Error de respuesta","Error");
                error.printStackTrace();
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        peticion.add(stringRegquest);
    }

    public void sendSMSCorriente(String id){

        requestQueue = Volley.newRequestQueue( this );
        final String ids = id;

        //String urlcut = "http://170.80.28.214/abg/abgsite/API/Controladores/Comandos.php?nIDDispositivoAuto=8&Accion=GEOLOCALIZACION";
        String urlcut = "http://170.80.28.214/abg/abgsite/API/Controladores/Comandos.php?nIDDispositivoAuto="+ids+"&Accion=CORTAR%20CORRIENTE";
        Toast.makeText(this, ""+id, Toast.LENGTH_SHORT).show();
        Log.i("sendSMS",urlcut);
            StringRequest requestcut = new StringRequest( Request.Method.GET, urlcut, new Response.Listener<String>() {
                @Override
                public void onResponse( String response ) {
                    try {
                        Toast.makeText(comandosConfiguracion.this, response, Toast.LENGTH_SHORT).show();
                        JSONArray jsonArray = new JSONArray( response );
                        String phone     = jsonArray.getJSONObject(0).getString("Celular");
                        String command   = jsonArray.getJSONObject(0).getString("Comando");
                        String password  = jsonArray.getJSONObject(0).getString("Password");
                        String separator = jsonArray.getJSONObject(0).getString("Separador");
                        String param1    = jsonArray.getJSONObject(0).getString("Parametro1");
                        String message   =  " " + password + separator + command + separator + "0" ;
//                        if(checkPermissions(Manifest.permission.SEND_SMS)){
//                            SmsManager smsMana = SmsManager.getDefault();
//                            //smsMana.sendTextMessage(phone, null, " SETIO 1,1", null, null);
//                            smsMana.sendTextMessage(phone, null, "Hola", null, null);
//                            //alertDialog("Respuesta",message,false);
//                            Toast.makeText(comandosConfiguracion.this, "Mensaje Enviado Exitosamente", Toast.LENGTH_SHORT).show();
                            saveInDB("CORTAR CORRIENTE");
//                        }else {
//                            Toast.makeText(comandosConfiguracion.this, "Mensaje no enviado", Toast.LENGTH_SHORT).show();
//                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(comandosConfiguracion.this, "Mensaje no enviado", Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse( VolleyError error ) {
                    error.printStackTrace();
                }
            });
        //requestQueue.add( requestcut );
    }

    public void sendSMSGeo(String id){

        requestQueue = Volley.newRequestQueue( this );
        final String ids = "8";

        String urlcut = "http://170.80.28.214/abg/abgsite/API/Controladores/Comandos.php?nIDDispositivoAuto="+ids+"&Accion=GEOLOCALIZACION";
        //String urlcut = "http://170.80.28.214/abg/abgsite/API/Controladores/Comandos.php?nIDDispositivoAuto=8&Accion=CORTAR%20CORRIENTE";

        Log.i("sendSMS",urlcut);
        StringRequest requestcut = new StringRequest( Request.Method.GET, urlcut, new Response.Listener<String>() {
            @Override
            public void onResponse( String response ) {
                try {
                    //Toast.makeText(comandosConfiguracion.this, response, Toast.LENGTH_SHORT).show();
                    JSONArray jsonArray = new JSONArray( response );
                    String phone     = jsonArray.getJSONObject(0).getString("Celular");
                    String command   = jsonArray.getJSONObject(0).getString("Comando");
                    String password  = jsonArray.getJSONObject(0).getString("Password");
                    String separator = jsonArray.getJSONObject(0).getString("Separador");
                    String param1    = jsonArray.getJSONObject(0).getString("Parametro1");
                    String message   =  " " + password + separator + command + separator + "0" ;
//                    if(checkPermissions(Manifest.permission.SEND_SMS)){
//                        SmsManager smsMana = SmsManager.getDefault();
//                        //smsMana.sendTextMessage(phone, null, " SETIO 1,1", null, null);
//                        //smsMana.sendTextMessage(phone, null, message, null, null);
//                        //alertDialog("Respuesta",message,false);
//                        Toast.makeText(comandosConfiguracion.this, "Mensaje Enviado Exitosamente", Toast.LENGTH_SHORT).show();
//                        saveInDB();
//                    }else {
//                        Toast.makeText(comandosConfiguracion.this, "Mensaje no enviado", Toast.LENGTH_SHORT).show();
//                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(comandosConfiguracion.this, "Mensaje no enviado", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse( VolleyError error ) {
                error.printStackTrace();
            }
        });
        requestQueue.add( requestcut );
    }

    public void saveInDB(String accion){
        final String cut = "{ \"nIDAccion\": 0," +
                " \"nIDDispositivoAuto\": "+this.id+"," +
                " \"Accion\": \""+accion+"\"," +
                " \"Estatus\": \"NO PROCESADA\" }";

        final String savedata = cut;

        String URL = "http://170.80.28.214/abg/abgsite/API/Controladores/Acciones.php";

        requestQueue = Volley.newRequestQueue( getApplicationContext() );

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject objres = new JSONObject(response);
                    Toast.makeText(getApplicationContext(), objres.toString(), Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(), "Acción enviada", Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText( getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT ).show();
            }
        }) {
            @Override
            public String getBodyContentType() { return "application/json; charset=utf-8"; }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return savedata == null ? null : savedata.getBytes("utf-8" );
                } catch ( UnsupportedEncodingException uee){
                    return null;
                }
            }
        };

        requestQueue.add( stringRequest );
    }

    private void alertDialog(String titulo, String msg, boolean dismiss) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(titulo);
        builder.setMessage(msg);

        builder.setPositiveButton("Aceptar", null);
        if ( dismiss){
            builder.setNegativeButton("Cancelar", null);
        }
        AlertDialog dialog = builder.create();
        dialog.show();
    }

}


